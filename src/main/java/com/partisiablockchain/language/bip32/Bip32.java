package com.partisiablockchain.language.bip32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.bouncycastle.util.Arrays.concatenate;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.util.NumericConversion;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.ECPoint;

/**
 * Used for generating and deriving keys according to the <a
 * href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki">BIP32 specification</a>.
 *
 * <p>This is used for creating wallets, as it makes it possible to derive multiple child keys from
 * a single mnemonic seed.
 */
public final class Bip32 {

  private static final ECDomainParameters EC_PARAMS = Curve.CURVE;
  private static final BigInteger CURVE_ORDER = EC_PARAMS.getN();
  private static final String HMAC_SHA_512 = "HmacSHA512";

  /** Keys with index 0x80000000 and above are hardened. */
  public static final int HARDENED_INDEXES_START = 0x80000000;

  /**
   * The key for the hmacSha512 hash used in master key generation, as specified <a
   * href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#user-content-Master_key_generation">here</a>.
   */
  private static final String MASTER_KEY_KEY = "Bitcoin seed";

  /** Private constructor to prevent instantiation. */
  private Bip32() {}

  /**
   * Generates a master key.
   *
   * <p>This is done by:
   *
   * <ul>
   *   <li>Taking a mnemonic seed S (as bytes)
   *   <li>Computing I = HMAC-SHA512("Bitcoin seed",S)
   *   <li>Splitting I into I_L and I_R (each 32 bytes)
   *   <li>The master key is the BigInteger representation of I_L with the corresponding chain code
   *       I_R
   * </ul>
   *
   * <p>This is according to <a
   * href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#user-content-Master_key_generation">the
   * BIP32 specification</a>
   *
   * @param seed Seed to generate master key from.
   * @return Master key.
   */
  public static ExtendedKey generateMasterKey(byte[] seed) {
    return generateMasterKey(seed, Bip32::hmacSha512);
  }

  static ExtendedKey generateMasterKey(byte[] seed, HashFunction hashFunction) {
    byte[] i = hashFunction.hash(MASTER_KEY_KEY.getBytes(StandardCharsets.UTF_8), seed);

    byte[] il = Arrays.copyOfRange(i, 0, 32);
    byte[] ir = Arrays.copyOfRange(i, 32, 64);

    BigInteger privateKey = new BigInteger(1, il);

    if (privateKey.equals(BigInteger.ZERO) || privateKey.compareTo(CURVE_ORDER) >= 0) {
      throw new RuntimeException("Invalid master key generated");
    }

    return new ExtendedKey(privateKey, publicKeyFromPrivate(privateKey), ir, 0, 0);
  }

  /**
   * Derives a child key from a given parent key and index.
   *
   * <p>There are 3 possible derivations, namely:
   *
   * <ul>
   *   <li>Private parent key -> private child key
   *   <li>Public parent key -> public child key
   *   <li>Private parent key -> public child key
   * </ul>
   *
   * <p>If the parent key has a private key, both the private and corresponding public child key is
   * derived, otherwise only the public child key is derived.
   *
   * @param parent The key to derive the child key from.
   * @param childIndex The index of the child key.
   * @return Child key derived from parent key.
   * @throws Exception Throws an exception if the derived child key is invalid.
   */
  public static ExtendedKey deriveChildKey(ExtendedKey parent, int childIndex) throws Exception {
    return deriveChildKey(parent, childIndex, Bip32::hmacSha512);
  }

  static ExtendedKey deriveChildKey(ExtendedKey parent, int childIndex, HashFunction hashFunction)
      throws Exception {
    boolean hardened = (childIndex & HARDENED_INDEXES_START) != 0;
    byte[] data;

    if (hardened) {
      if (!parent.hasPrivateKey()) {
        throw new Exception("Cannot derive hardened key from public key");
      }
      data = concatenate(new byte[] {0x00}, ser256(parent.privateKey()), ser32(childIndex));
    } else {
      byte[] pubKey = parent.publicKey().asBytes();
      data = concatenate(pubKey, ser32(childIndex));
    }

    byte[] i = hashFunction.hash(parent.chainCode(), data);
    byte[] il = Arrays.copyOfRange(i, 0, 32);
    byte[] ir = Arrays.copyOfRange(i, 32, 64);

    BigInteger ilInt = new BigInteger(1, il);

    if (ilInt.compareTo(CURVE_ORDER) >= 0) {
      throw new RuntimeException("Invalid key derived");
    }

    BigInteger childPrivateKey = null;
    BlockchainPublicKey childPublicKey;

    if (parent.hasPrivateKey()) {
      childPrivateKey = ilInt.add(parent.privateKey()).mod(CURVE_ORDER);

      if (childPrivateKey.equals(BigInteger.ZERO)) {
        throw new RuntimeException("Invalid child private key derived");
      }

      childPublicKey = publicKeyFromPrivate(childPrivateKey);
    } else {
      BlockchainPublicKey blockchainPublicKey = publicKeyFromPrivate(ilInt);
      ECPoint ilPoint = EC_PARAMS.getCurve().decodePoint(blockchainPublicKey.asBytes());
      ECPoint parentPublicKeyPoint = EC_PARAMS.getCurve().decodePoint(parent.publicKey().asBytes());
      ECPoint childPublicKeyPoint = ilPoint.add(parentPublicKeyPoint);

      if (childPublicKeyPoint.isInfinity()) {
        throw new RuntimeException("Invalid child public key derived");
      }

      childPublicKey =
          BlockchainPublicKey.fromEncodedEcPoint(childPublicKeyPoint.normalize().getEncoded(true));
    }

    return new ExtendedKey(childPrivateKey, childPublicKey, ir, parent.depth() + 1, childIndex);
  }

  /**
   * Computes the HMAC-SHA-512 MAC with the given key on the given data.
   *
   * @param key The key to use for the MAC.
   * @param data The data to get the MAC for.
   * @return The bytes of the MAC.
   */
  private static byte[] hmacSha512(byte[] key, byte[] data) {
    Mac hmac = ExceptionConverter.call(() -> Mac.getInstance(HMAC_SHA_512));
    SecretKeySpec keySpec = new SecretKeySpec(key, HMAC_SHA_512);
    ExceptionConverter.run(() -> hmac.init(keySpec));
    return hmac.doFinal(data);
  }

  /**
   * Serializes a 32-bit unsigned integer as a 4-byte sequence, most significant byte first.
   *
   * @param value The integer to serialize.
   * @return The serialized bytes.
   */
  private static byte[] ser32(int value) {
    return NumericConversion.intToBytes(value);
  }

  /**
   * Serializes the given BigInteger as a 32-byte sequence, most significant byte first.
   *
   * @param value The BigInteger to serialize.
   * @return The serialized bytes.
   */
  static byte[] ser256(BigInteger value) {
    byte[] result = new byte[32];
    NumericConversion.bigIntToBytesUnsigned(value, 32, result, 0);
    return result;
  }

  /**
   * Gets the public key from a given private key.
   *
   * @param privateKey The private key to find corresponding public key for.
   * @return The public key from the given private key.
   */
  static BlockchainPublicKey publicKeyFromPrivate(BigInteger privateKey) {
    return new KeyPair(privateKey).getPublic();
  }

  @FunctionalInterface
  interface HashFunction {

    /**
     * The hash function to use when generating and deriving keys.
     *
     * @param key The key for the hash function.
     * @param toHash The bytes to hash.
     * @return The hashed bytes.
     */
    byte[] hash(byte[] key, byte[] toHash);
  }
}
