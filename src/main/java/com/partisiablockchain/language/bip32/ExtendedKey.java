package com.partisiablockchain.language.bip32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.bip32.Bip32.ser256;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import java.math.BigInteger;
import org.bouncycastle.util.encoders.Hex;

/**
 * An extended key includes a private and/or public key as well as a chain code, as described in <a
 * href="https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#user-content-Extended_keys">the
 * BIP32 specification</a>. The chain code for two corresponding public and private keys are
 * identical.
 *
 * <p>An extended private key is (k,c) where k is the normal private key and c is the chaincode.
 *
 * <p>An extended public key is (K,c) where K = point(k) and c is the chaincode.
 */
public record ExtendedKey(
    BigInteger privateKey, BlockchainPublicKey publicKey, byte[] chainCode, int depth, int index) {

  /**
   * Get the private key in hex format.
   *
   * @return String of the private key in hex format.
   */
  public String getPrivateKeyHex() {
    return Hex.toHexString(ser256(privateKey));
  }

  String getChainCodeHex() {
    return Hex.toHexString(chainCode);
  }

  String getPublicKeyHex() {
    return Hex.toHexString(publicKey.asBytes());
  }

  boolean hasPrivateKey() {
    return privateKey != null;
  }
}
