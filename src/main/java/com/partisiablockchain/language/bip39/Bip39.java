package com.partisiablockchain.language.bip39;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Used for the creation and validation of mnemonic phrases as well as converting a mnemonic phrase
 * into a binary seed. This is done according to the <a
 * href="https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki">BIP39 specification</a>
 */
public final class Bip39 {

  private static final String ENGLISH_WORDLIST = "english.txt";
  private static final String PBKDF_2_WITH_HMAC_SHA_512 = "PBKDF2WithHmacSHA512";
  private static final String MNEMONIC_SALT_START = "mnemonic";

  /** Private constructor to prevent instantiation. */
  private Bip39() {}

  /**
   * Generates a mnemonic phrase from given entropy.
   *
   * @param entropy The entropy bytes.
   * @return String of a mnemonic phrase.
   */
  public static String generateMnemonic(byte[] entropy) {
    int entropyBitLength = entropy.length * 8;
    if (entropyBitLength % 32 != 0 || entropyBitLength < 128 || entropyBitLength > 256) {
      throw new RuntimeException(
          "Invalid entropy size, it should be a multiple of 32 and in the range [128;256]");
    }

    List<Integer> indexes = getWordIndexes(entropy);
    List<String> wordList = loadWordList();

    StringJoiner mnemonic = new StringJoiner(" ");
    for (int index : indexes) {
      mnemonic.add(wordList.get(index));
    }

    return mnemonic.toString().trim();
  }

  /**
   * Load the English word list from resources.
   *
   * <p>The word list is from <a
   * href="https://github.com/bitcoin/bips/blob/master/bip-0039/english.txt">here</a>.
   *
   * @return List of english words.
   */
  private static List<String> loadWordList() {
    String words =
        WithResource.apply(
            () -> Bip39.class.getClassLoader().getResourceAsStream(ENGLISH_WORDLIST),
            wordStream -> new String(wordStream.readAllBytes(), StandardCharsets.UTF_8));
    return words.lines().toList();
  }

  /**
   * Splits the entropy into 11-bit segments and converts each segment to indexes.
   *
   * @param entropy The entropy to convert to word indexes.
   * @return List of integers representing word indexes.
   */
  private static List<Integer> getWordIndexes(byte[] entropy) {
    byte[] entropyWithChecksum = Arrays.copyOfRange(entropy, 0, entropy.length + 1);
    int checksumLength = entropy.length * 8 / 32;
    entropyWithChecksum[entropy.length] = computeChecksum(entropy);

    int mnemonicPhraseLength = ((entropy.length * 8) + checksumLength) / 11;

    List<Integer> indexes = new ArrayList<>();
    for (int i = 0, wi = 0; wi < mnemonicPhraseLength; i += 11, wi++) {
      indexes.add(wi, getNext11Bits(entropyWithChecksum, i));
    }

    return indexes;
  }

  /**
   * Generates a seed from the mnemonic and passphrase using the PBKDF2 function with HMAC-SHA512.
   *
   * <p>The mnemonic and passphrase are normalized using NFKD before processing.
   *
   * @param mnemonic The mnemonic phrase.
   * @param passphrase The passphrase (empty string if none).
   * @return The generated seed as a byte array.
   */
  public static byte[] mnemonicToSeed(String mnemonic, String passphrase) {
    String normalizedMnemonic = Normalizer.normalize(mnemonic, Normalizer.Form.NFKD);
    String normalizedPassphrase = Normalizer.normalize(passphrase, Normalizer.Form.NFKD);

    String salt = MNEMONIC_SALT_START + normalizedPassphrase;
    byte[] saltBytes = salt.getBytes(StandardCharsets.UTF_8);

    PBEKeySpec pbeKeySpec = new PBEKeySpec(normalizedMnemonic.toCharArray(), saltBytes, 2048, 512);
    SecretKeyFactory secretKeyFactory =
        ExceptionConverter.call(() -> SecretKeyFactory.getInstance(PBKDF_2_WITH_HMAC_SHA_512));

    return ExceptionConverter.call(() -> secretKeyFactory.generateSecret(pbeKeySpec).getEncoded());
  }

  /**
   * Computes the checksum of the given entropy.
   *
   * <p>The checksum is computed by taking the first (entropy length / 32) bits of the SHA-256 hash
   * of the entropy, according to BIP39. Since (entropy length / 32) is at most 8, we just use the
   * first byte of the SHA-256 hash.
   *
   * @param entropy The entropy bytes.
   * @return The checksum byte.
   */
  private static byte computeChecksum(byte[] entropy) {
    return Hash.create(out -> out.write(entropy)).getBytes()[0];
  }

  /**
   * Validates mnemonic phrase.
   *
   * <p>Checks for
   *
   * <ul>
   *   <li>Correct word count (should be either 12, 15, 18, 21 or 24 words)
   *   <li>Valid words (they exist in the english word list)
   *   <li>The checksum is correct
   * </ul>
   *
   * @param mnemonic The mnemonic phrase to check for validity.
   * @throws RuntimeException If mnemonic phrase is not valid.
   */
  public static void validateMnemonic(String mnemonic) throws RuntimeException {
    if (mnemonic == null) {
      throw new NullPointerException("Mnemonic is null");
    }

    String trimmedMnemonic = mnemonic.trim();

    if (trimmedMnemonic.isEmpty()) {
      throw new RuntimeException("Invalid word count: 0");
    }

    String[] words = trimmedMnemonic.split("\\s+");
    int wordCount = words.length;

    // Word count has to be either 12, 15, 18, 21 or 24.
    if (wordCount % 3 != 0 || wordCount < 12 || wordCount > 24) {
      throw new RuntimeException("Invalid word count: " + wordCount);
    }

    List<Integer> indexes = convertWordsToIndexes(words);

    checkChecksum(indexes);
  }

  /**
   * Converts a list of words to indexes in the word list.
   *
   * <p>If a word in the list is invalid, an exception is thrown.
   *
   * @param words List of words to find indexes for.
   * @return List of indexes.
   */
  private static List<Integer> convertWordsToIndexes(String[] words) {
    List<String> wordList = loadWordList();
    List<Integer> indexes = new ArrayList<>();

    for (String word : words) {
      int index = wordList.indexOf(word);
      if (index == -1) {
        throw new RuntimeException("Invalid word found in mnemonic: " + word);
      }
      indexes.add(index);
    }

    return indexes;
  }

  /**
   * Checks that the checksum is correct.
   *
   * <p>Does so by
   *
   * <ul>
   *   <li>Converting indexes of the words back into 11-bit numbers
   *   <li>Getting the entropy and checksum from the bits
   *   <li>Checking if this checksum matches the computed checksum
   * </ul>
   *
   * @param indexes The indexes of the words in the wordlist.
   */
  private static void checkChecksum(List<Integer> indexes) {
    int totalBits = indexes.size() * 11;
    int totalBytes = getByteAmountFromBits(totalBits);
    byte[] entropyWithChecksum = new byte[totalBytes];
    writeIn11BitChunks(entropyWithChecksum, indexes);

    int checksumBits = totalBits / 33;
    int entropyBits = totalBits - checksumBits;
    int entropyBytes = getByteAmountFromBits(entropyBits);

    byte[] entropy = Arrays.copyOf(entropyWithChecksum, entropyBytes);
    int expectedChecksum = extractBits(entropyWithChecksum, entropyBits, checksumBits);

    byte computedChecksum = computeChecksum(entropy);
    int computedChecksumBits = (computedChecksum & 0xFF) >> (8 - checksumBits);

    if (expectedChecksum != computedChecksumBits) {
      throw new RuntimeException("Invalid checksum");
    }
  }

  /**
   * Calculates the amount of bytes needed to contain a certain amount of bits. This is calculated
   * as ⌈bits/8⌉.
   *
   * @param bits The amount of bits to fit into bytes.
   * @return The amount of bytes needed to fit the bits.
   */
  static int getByteAmountFromBits(int bits) {
    return (bits + 7) / 8;
  }

  /** Extracts bits from a byte array starting at a given bit offset. */
  private static int extractBits(byte[] data, int bitOffset, int numBits) {
    int result = 0;
    for (int i = 0; i < numBits; i++) {
      int totalBitIndex = bitOffset + i;
      int byteIndex = totalBitIndex / 8;
      int bitInByte = 7 - (totalBitIndex % 8);

      int bit = (data[byteIndex] >> bitInByte) & 1;
      result = (result << 1) | bit;
    }

    return result;
  }

  /**
   * Returns the next 11 bits from a byte array, starting at the given offset, as an integer.
   *
   * @param byteArray The byte array to read the bits from.
   * @param bitOffset The offset to start reading from.
   * @return The 11 bit value as an integer.
   */
  private static int getNext11Bits(byte[] byteArray, int bitOffset) {
    int result = 0;
    int bitsCollected = 0;

    while (bitsCollected < 11) {
      int byteIndex = bitOffset / 8;
      int bitIndex = bitOffset % 8;

      int currentBit = (byteArray[byteIndex] >> (7 - bitIndex)) & 1;
      result = (result << 1) | currentBit;
      bitOffset++;
      bitsCollected++;
    }

    return result;
  }

  /**
   * Writes a list of integer values into a byte array in 11-bit chunks.
   *
   * @param byteArray The byte array to write into. It must be large enough to hold all the bits.
   * @param values The list of integer values to write. Each value should fit within 11 bits.
   */
  private static void writeIn11BitChunks(byte[] byteArray, List<Integer> values) {
    int bitOffset = 0;
    for (Integer value : values) {
      write11Bits(byteArray, bitOffset, value);
      bitOffset += 11;
    }
  }

  /**
   * Writes 11 bits of a value into a byte array starting at a given bit offset. Bits are written
   * from the most significant bit to the least significant bit.
   *
   * @param array The byte array to write bits into.
   * @param bitOffset The starting bit offset in the array where the bits will be written.
   * @param value The value containing the bits to write.
   */
  private static void write11Bits(byte[] array, int bitOffset, int value) {
    for (int i = 0; i < 11; i++) {
      int bit = (value >> (11 - i - 1)) & 1;

      int totalBitIndex = bitOffset + i;
      int byteIndex = totalBitIndex / 8;
      int bitInByte = 7 - (totalBitIndex % 8);

      array[byteIndex] |= (byte) (bit << bitInByte);
    }
  }
}
