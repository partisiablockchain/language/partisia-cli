package com.partisiablockchain.language.bip44;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.bip32.Bip32;
import com.partisiablockchain.language.bip32.ExtendedKey;
import com.secata.tools.coverage.ExceptionConverter;

/**
 * Used for deriving BIP44 keys. This is done according to <a
 * href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki">the BIP44
 * specification</a>.
 */
public final class Bip44 {

  private static final int PURPOSE = 44 | Bip32.HARDENED_INDEXES_START;

  /** Private constructor to prevent instantiation. */
  private Bip44() {}

  /**
   * Derives a BIP 44 key.
   *
   * <p>This is done by taking the following 5 level BIP32 derivation path: m / purpose' /
   * coin_type' / account' / change / address_index (the apostrophe indicates that the derivation is
   * hardened). This is according to <a
   * href="https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki">the BIP44
   * specification</a>.
   *
   * @param masterKey The key to derive the BIP44 key from.
   * @param coinType The coin type, <a
   *     href="https://github.com/satoshilabs/slips/blob/master/slip-0044.md">list can be found
   *     here.</a>
   * @param account The index of the account for which to get the key.
   * @param change The change.
   * @param addressIndex Index used as child index in the BIP32 key derivation.
   * @return ExtendedKey adhering to the BIP44 specification.
   */
  public static ExtendedKey deriveBip44Key(
      ExtendedKey masterKey, int coinType, int account, int change, int addressIndex) {

    int[] bip44Path =
        new int[] {
          PURPOSE,
          coinType | Bip32.HARDENED_INDEXES_START,
          account | Bip32.HARDENED_INDEXES_START,
          change,
          addressIndex
        };

    ExtendedKey currentKey = masterKey;
    for (int index : bip44Path) {
      ExtendedKey finalCurrentKey = currentKey;
      currentKey = ExceptionConverter.call(() -> Bip32.deriveChildKey(finalCurrentKey, index));
    }

    return currentKey;
  }
}
