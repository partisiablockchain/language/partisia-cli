package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.builder.AbstractBuilder;
import com.partisiablockchain.language.abiclient.rpc.FnRpcBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.EnumTypeSpec;
import com.partisiablockchain.language.abimodel.model.EnumVariant;
import com.partisiablockchain.language.abimodel.model.FieldAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.FnAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.OptionTypeSpec;
import com.partisiablockchain.language.abimodel.model.SizedArrayTypeSpec;
import com.partisiablockchain.language.abimodel.model.StructTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.abimodel.model.VecTypeSpec;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.HexFormat;
import java.util.List;
import java.util.Set;

/**
 * A builder that can assemble the necessary and correct bytes required to execute some non-ZK
 * action.
 */
public final class ActionRpcBuilder {

  private static final String FILE_DENOMINATOR = "file:";
  private static final String VEC_LEFT_DENOMINATOR = "[";
  private static final String VEC_RIGHT_DENOMINATOR = "]";
  private static final String STRUCT_LEFT_DENOMINATOR = "{";
  private static final String STRUCT_RIGHT_DENOMINATOR = "}";
  private final FileAbi abi;
  private final FileManager fileManager;

  /**
   * Assembles the RPC for calling non-ZK actions described by the provided ABI.
   *
   * @param abi The abi of the contract to generate remote procedure calls for.
   * @param fileManager A {@link FileManager} for handling file IO operations.
   */
  public ActionRpcBuilder(FileAbi abi, FileManager fileManager) {
    this.abi = abi;
    this.fileManager = fileManager;
  }

  /**
   * Builds the RPC for the provided action using the ABI of this builder to ensure correctness.
   *
   * @param action The action to generate RPC for.
   * @param arguments The values of the action's parameters.
   * @return The bytes for calling the action with the provided arguments.
   */
  public byte[] buildRpc(FnAbi action, String[] arguments) {
    PeekingIterator<String> argumentIterator =
        new PeekingIterator<>(Arrays.stream(arguments).iterator());
    return serializeArguments(action, argumentIterator);
  }

  /**
   * Serializes all arguments in the provided iterable. A {@link RuntimeException} is thrown if the
   * amount of arguments does not match the amount of parameters to the action or if an argument
   * violates the type-constraints of its corresponding parameter.
   *
   * @param action The action to generate RPC for.
   * @param argumentIterator An iterator containing the values of the action's parameters.
   * @return The bytes for calling the action with the provided arguments.
   */
  byte[] serializeArguments(FnAbi action, PeekingIterator<String> argumentIterator) {
    FnRpcBuilder rpcBuilder = new FnRpcBuilder(action.name(), abi.contract());
    List<ArgumentAbi> parameters = action.arguments();

    for (int i = 0; i < parameters.size(); i++) {
      ArgumentAbi parameter = parameters.get(i);
      if (!argumentIterator.hasNext()) {
        String actionSignature = AbiParser.printFunction(abi, action);
        throw new RuntimeException(
            "Received too few arguments for action:%n%sExpected %d argument(s) but got %d."
                .formatted(actionSignature, parameters.size(), i));
      }
      serializeNextArgument(rpcBuilder, argumentIterator, parameter, action.name());
    }

    if (argumentIterator.hasNext()) {
      String actionSignature = AbiParser.printFunction(abi, action);
      throw new RuntimeException(
          "Provided too many arguments to action:%n%sExpected %d argument(s)."
              .formatted(actionSignature, parameters.size()));
    }
    return rpcBuilder.getBytes();
  }

  /**
   * Serializes the next argument of the provided iterator and adds the resulting bytes to the
   * builder. A {@link RuntimeException} is thrown if the argument is malformed w.r.t. the parameter
   * type.
   *
   * @param rpcBuilder The builder to add the next argument to.
   * @param argumentIterator The iterator containing all sub-elements of the next element.
   * @param parameter The abi of the next argument.
   * @param actionName The name of the action having its rpc built.
   */
  void serializeNextArgument(
      AbstractBuilder rpcBuilder,
      PeekingIterator<String> argumentIterator,
      ArgumentAbi parameter,
      String actionName) {
    TypeSpec parameterType = parameter.type();
    try {
      addArgumentToBuilder(rpcBuilder, argumentIterator, parameterType);
    } catch (Exception e) {
      throw new RuntimeException(
          "Could not serialize argument to parameter '%s' of type '%s' in action '%s':%n%s"
              .formatted(parameter.name(), parameterType, actionName, e.getMessage()));
    }
  }

  /**
   * Adds the next element of the provided iterator to the builder. The element is type-checked and
   * serialized in the process. This happens recursively for generic types such as vectors and
   * structs.
   *
   * @param rpcBuilder The builder to add the next argument to.
   * @param argumentIterator The iterator containing all sub-elements of the next element.
   * @param argumentType The type of the next argument.
   */
  @SuppressWarnings("LeftCurlyNl")
  void addArgumentToBuilder(
      AbstractBuilder rpcBuilder, PeekingIterator<String> argumentIterator, TypeSpec argumentType) {
    String argument = argumentIterator.next();
    TypeSpec.TypeIndex typeIndex = argumentType.typeIndex();
    switch (typeIndex) {
      case u8 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "0", "255", "u8");
        rpcBuilder.addU8(value.byteValue());
      }
      case u16 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "0", "65535", "u16");
        rpcBuilder.addU16(value.shortValue());
      }
      case u32 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "0", "4294967295", "u32");
        rpcBuilder.addU32(value.intValue());
      }
      case u64 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "0", "18446744073709551615", "u64");
        rpcBuilder.addU64(value.longValue());
      }
      case u128 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "0", "340282366920938463463374607431768211455", "u128");
        rpcBuilder.addU128(value);
      }
      case u256 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(
            value,
            "0",
            "115792089237316195423570985008687907853269984665640564039457584007913129639935",
            "u256");
        rpcBuilder.addU256(value);
      }
      case i8 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "-128", "127", "i8");
        rpcBuilder.addI8(value.byteValue());
      }
      case i16 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "-32768", "32767", "i16");
        rpcBuilder.addI16(value.shortValue());
      }
      case i32 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "-2147483648", "2147483647", "i32");
        rpcBuilder.addI32(value.intValue());
      }
      case i64 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(value, "-9223372036854775808", "9223372036854775807", "i64");
        rpcBuilder.addI64(Long.parseLong(argument));
      }
      case i128 -> {
        BigInteger value = toBigInteger(argument);
        validateInteger(
            value,
            "-170141183460469231731687303715884105728",
            "170141183460469231731687303715884105727",
            "i128");
        rpcBuilder.addI128(new BigInteger(argument));
      }
      case String -> rpcBuilder.addString(argument);
      case bool -> {
        String lowerCaseBool = argument.toLowerCase();
        Set<String> booleanLiterals = new HashSet<>(Arrays.asList("true", "false"));
        if (!booleanLiterals.contains(lowerCaseBool)) {
          throw new RuntimeException(
              "Invalid boolean literal '%s'. Use either 'true' or 'false'.".formatted(argument));
        }
        rpcBuilder.addBool(Boolean.parseBoolean(argument));
      }
      case Address -> {
        AddressValidator.addressFromString(argument);
        rpcBuilder.addAddress(argument);
      }
      case Hash -> rpcBuilder.addHash(argument);
      case PublicKey -> rpcBuilder.addPublicKey(argument);
      case Signature -> rpcBuilder.addSignature(argument);
      case BlsPublicKey -> rpcBuilder.addBlsPublicKey(argument);
      case BlsSignature -> rpcBuilder.addBlsSignature(argument);
      case SizedArray -> {
        SizedArrayTypeSpec arrayTypeSpec = (SizedArrayTypeSpec) argumentType;
        if (arrayTypeSpec.valueType().typeIndex() == TypeSpec.TypeIndex.u8) {
          addByteArray(argument, arrayTypeSpec, rpcBuilder);
        } else {
          addArray(argument, arrayTypeSpec, rpcBuilder, argumentIterator);
        }
      }
      case Vec -> {
        TypeSpec vecInnerType = ((VecTypeSpec) argumentType).valueType();
        if (vecInnerType.typeIndex() == TypeSpec.TypeIndex.u8) {
          addByteVector(argument, rpcBuilder);
        } else {
          addVectorLiteral(argument, vecInnerType, rpcBuilder, argumentIterator);
        }
      }
      case Named -> {
        NamedTypeSpec namedTypeSpec = abi.contract().getNamedType((NamedTypeRef) argumentType);
        if (namedTypeSpec instanceof StructTypeSpec structTypeSpec) {
          AbstractBuilder structBuilder = rpcBuilder.addStruct();
          addStruct(structBuilder, argumentIterator, argument, structTypeSpec);
        } else {
          addEnum(rpcBuilder, argumentIterator, argument, (EnumTypeSpec) namedTypeSpec);
        }
      }
      case Option -> {
        AbstractBuilder optionBuilder = rpcBuilder.addOption();
        if (argument.equalsIgnoreCase("some")) {
          if (!argumentIterator.hasNext()
              || !argumentIterator.next().equals(STRUCT_LEFT_DENOMINATOR)) {
            throw new RuntimeException("Option Some is missing beginning denominator '{'");
          }
          addArgumentToBuilder(
              optionBuilder, argumentIterator, ((OptionTypeSpec) argumentType).valueType());
          if (!argumentIterator.hasNext()
              || !argumentIterator.next().equals(STRUCT_RIGHT_DENOMINATOR)) {
            throw new RuntimeException("Option Some is missing ending denominator '}'");
          }
        } else if (!argument.equalsIgnoreCase("none")) {
          throw new RuntimeException(
              "Option expected 'Some' or 'None' but got '%s'".formatted(argument));
        }
      }
      default ->
          throw new RuntimeException(
              "Type '%s' is not supported."
                  .formatted(TypeSpec.TypeIndex.fromValue(typeIndex.getValue())));
    }
  }

  private static void addByteArray(
      String argument, SizedArrayTypeSpec argumentType, AbstractBuilder rpcBuilder) {
    byte[] bytes = HexFormat.of().parseHex(argument);
    int sizeOfArray = argumentType.length();
    int numberOfRpcBytes = bytes.length;
    if (numberOfRpcBytes != sizeOfArray) {
      throw new RuntimeException(
          "Provided %d bytes for an array needing exactly %d bytes."
              .formatted(numberOfRpcBytes, sizeOfArray));
    }
    rpcBuilder.addSizedByteArray(bytes);
  }

  private void addEnum(
      AbstractBuilder rpcBuilder,
      PeekingIterator<String> argumentIterator,
      String argument,
      EnumTypeSpec enumTypeSpec) {
    EnumVariant enumVariant =
        enumTypeSpec.variants().stream()
            .filter(variant -> abi.contract().getNamedType(variant.def()).name().equals(argument))
            .findAny()
            .orElseThrow(
                () ->
                    new RuntimeException(
                        "Enum '%s' does not contain variant '%s'"
                            .formatted(enumTypeSpec.name(), argument)));
    StructTypeSpec structTypeSpec = (StructTypeSpec) abi.contract().getNamedType(enumVariant.def());
    AbstractBuilder enumBuilder = rpcBuilder.addEnumVariant(enumVariant.discriminant());
    if (structTypeSpec.fields().isEmpty()) {
      return;
    }
    if (!argumentIterator.hasNext()) {
      throw new RuntimeException(
          "Enum variant '%s' is missing struct".formatted(structTypeSpec.name()));
    }
    addStruct(enumBuilder, argumentIterator, argumentIterator.next(), structTypeSpec);
  }

  private static BigInteger toBigInteger(String argumentValue) {
    return ExceptionConverter.call(
        () -> new BigInteger(argumentValue), "'%s' is not a number.".formatted(argumentValue));
  }

  private static void validateInteger(
      BigInteger value, String minValue, String maxValue, String type) {
    BigInteger minValueBigInt = new BigInteger(minValue);
    BigInteger maxValueBigInt = new BigInteger(maxValue);

    if (value.compareTo(minValueBigInt) == -1 || value.compareTo(maxValueBigInt) == 1) {
      throw new RuntimeException(
          "Value '%d' is outside valid range of [%s,%s] for %s."
              .formatted(value, minValue, maxValue, type));
    }
  }

  /**
   * Adds u8-vectors to the rpc-builder. A u8-vector is provided either as a hexidecimal string or a
   * path prepended with {@value FILE_DENOMINATOR}.
   */
  private void addByteVector(String argument, AbstractBuilder rpcBuilder) {
    byte[] argumentBytes;
    String lowerCaseArgument = argument.toLowerCase();
    if (lowerCaseArgument.startsWith(FILE_DENOMINATOR)) {
      String pathSubstring = lowerCaseArgument.substring(FILE_DENOMINATOR.length());
      argumentBytes =
          fileManager.readBytes(
              Paths.get(pathSubstring), "Failed to read bytes from %s".formatted(pathSubstring));
    } else {
      argumentBytes =
          ExceptionConverter.call(
              () -> HexFormat.of().parseHex(argument),
              "u8-vectors must be provided as a hex string or as a file of the form"
                  + " 'file:path/to/file'.");
    }
    rpcBuilder.addVecU8(argumentBytes);
  }

  /** Adds vector arguments on the form '[ ... ]' to the rpc-builder. */
  private void addVectorLiteral(
      String argument,
      TypeSpec vecInnerType,
      AbstractBuilder rpcBuilder,
      PeekingIterator<String> argumentIterator) {
    if (!argument.equals(VEC_LEFT_DENOMINATOR)) {
      throw new RuntimeException(
          "Vector is missing a beginning denominator '%s' before element '%s'."
              .formatted(VEC_LEFT_DENOMINATOR, argument));
    }
    AbstractBuilder vecBuilder = rpcBuilder.addVec();
    while (argumentIterator.hasNext()) {
      String vecInnerValue = argumentIterator.peek();
      if (vecInnerValue.equals(VEC_RIGHT_DENOMINATOR)) {
        argumentIterator.next();
        return;
      }
      addArgumentToBuilder(vecBuilder, argumentIterator, vecInnerType);
    }
    throw new RuntimeException(
        "Vector is missing its ending denominator '%s'.".formatted(VEC_RIGHT_DENOMINATOR));
  }

  /** Adds array arguments on the form '[ ... ]' to the rpc-builder. */
  private void addArray(
      String argument,
      SizedArrayTypeSpec arrayType,
      AbstractBuilder rpcBuilder,
      PeekingIterator<String> argumentIterator) {
    if (!argument.equals(VEC_LEFT_DENOMINATOR)) {
      throw new RuntimeException(
          "Array is missing a beginning denominator '%s' before element '%s'."
              .formatted(VEC_LEFT_DENOMINATOR, argument));
    }
    AbstractBuilder arrayBuilder = rpcBuilder.addSizedArray();
    for (int i = 0; i < arrayType.length(); i++) {
      if (!argumentIterator.hasNext() || argumentIterator.peek().equals(VEC_RIGHT_DENOMINATOR)) {
        throw new RuntimeException(
            "Array is too short. Expected size is '%d'".formatted(arrayType.length()));
      }
      addArgumentToBuilder(arrayBuilder, argumentIterator, arrayType.valueType());
    }
    if (!argumentIterator.hasNext()) {
      throw new RuntimeException(
          "Array is missing an ending denominator '%s'.".formatted(VEC_RIGHT_DENOMINATOR));
    }
    if (!argumentIterator.next().equals(VEC_RIGHT_DENOMINATOR)) {
      throw new RuntimeException(
          "Array is too long. Expected size is '%d'".formatted(arrayType.length()));
    }
  }

  private void addStruct(
      AbstractBuilder rpcBuilder,
      PeekingIterator<String> argumentIterator,
      String argument,
      StructTypeSpec structTypeSpec) {
    if (!argument.equals(STRUCT_LEFT_DENOMINATOR)) {
      throw new RuntimeException(
          "Struct '%s' is missing a beginning denominator '%s' before element '%s'."
              .formatted(structTypeSpec.name(), STRUCT_LEFT_DENOMINATOR, argument));
    }
    for (FieldAbi fieldAbi : structTypeSpec.fields()) {
      if (!argumentIterator.hasNext() || argumentIterator.peek().equals(STRUCT_RIGHT_DENOMINATOR)) {
        throw new RuntimeException(
            "Struct '%s' is missing field '%s' of type '%s'"
                .formatted(structTypeSpec.name(), fieldAbi.name(), fieldAbi.type()));
      }
      addArgumentToBuilder(rpcBuilder, argumentIterator, fieldAbi.type());
    }
    if (!argumentIterator.hasNext() || !argumentIterator.next().equals(STRUCT_RIGHT_DENOMINATOR)) {
      throw new RuntimeException(
          "Struct '%s' is missing a ending denominator '%s'."
              .formatted(structTypeSpec.name(), STRUCT_RIGHT_DENOMINATOR));
    }
  }

  /**
   * Retrieves the contract abi provided to this rpc-builder.
   *
   * @return The provided contract abi.
   */
  public FileAbi getAbi() {
    return abi;
  }
}
