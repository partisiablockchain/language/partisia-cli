package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * An address validator ensures that addresses are of the correct format to be used on the
 * blockchain.
 */
public final class AddressValidator {

  private AddressValidator() {}

  /**
   * Converts a string-representation of an address to a {@link BlockchainAddress}. The string must
   * be hex-encoded and exactly 42 characters long.
   *
   * @param address The address as a 42 character long hex-encoded string.
   * @return The address wrapped in a BlockchainAddress object.
   */
  public static BlockchainAddress addressFromString(String address) {
    addressIsHexEncoded(address);
    addressHasCorrectLength(address);
    addressHasValidType(address);
    return BlockchainAddress.fromString(address);
  }

  /**
   * Requires that the given address has is prefixed with a valid identifier in its least
   * significant byte according to the given valid types.
   *
   * @param address The address whose type is being validated.
   * @param validTypes The set of valid types the address is allowed to be part of.
   */
  public static void requireAddressType(BlockchainAddress address, AddressTypeSubsets validTypes) {
    boolean addressTypeIsValid =
        Arrays.stream(validTypes.subset).anyMatch(validType -> address.getType() == validType);
    if (!addressTypeIsValid) {
      throw new RuntimeException(
          "Address type-prefix '%s' is not a %s address type"
              .formatted(address.writeAsString().substring(0, 2), validTypes.name().toLowerCase()));
    }
  }

  private static void addressHasCorrectLength(String address) {
    if (address.length() != 42) {
      throw new RuntimeException(
          "'%s' should consist of 42 characters (21 bytes) but had %d"
              .formatted(address, address.length()));
    }
  }

  private static void addressHasValidType(String address) {
    String addressTypeHex = address.substring(0, 2);
    int addressType = Integer.parseInt(addressTypeHex, 16);
    boolean addressTypeIsValid =
        Arrays.stream(BlockchainAddress.Type.values())
            .map(BlockchainAddress.Type::ordinal)
            .collect(Collectors.toSet())
            .contains(addressType);
    if (!addressTypeIsValid) {
      throw new RuntimeException(
          "Address type-prefix '%s' is not a valid address type".formatted(addressTypeHex));
    }
  }

  private static void addressIsHexEncoded(String address) {
    Pattern hexadecimalPattern = Pattern.compile("\\p{XDigit}+");
    Matcher matcher = hexadecimalPattern.matcher(address);
    if (!matcher.matches()) {
      throw new RuntimeException("'%s' should be encoded in hexadecimal".formatted(address));
    }
  }

  /**
   * A coarse-grained partition of the different types of addresses. An address corresponds either
   * to a user or a contract.
   */
  public enum AddressTypeSubsets {

    /** The subset of types corresponding to users. */
    USER(new BlockchainAddress.Type[] {BlockchainAddress.Type.ACCOUNT}),

    /** The subset of types corresponding to contracts. */
    CONTRACT(
        new BlockchainAddress.Type[] {
          BlockchainAddress.Type.CONTRACT_SYSTEM,
          BlockchainAddress.Type.CONTRACT_PUBLIC,
          BlockchainAddress.Type.CONTRACT_ZK,
          BlockchainAddress.Type.CONTRACT_GOV
        });

    final BlockchainAddress.Type[] subset;

    AddressTypeSubsets(BlockchainAddress.Type[] subset) {
      this.subset = subset;
    }
  }
}
