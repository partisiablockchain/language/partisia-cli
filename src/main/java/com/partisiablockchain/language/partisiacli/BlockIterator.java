package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.BlockState;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/** Iterates blocks across shards in production time order. */
public final class BlockIterator implements Iterator<BlockIterator.BlockData> {

  /** Data for a block on a shard. */
  public record BlockData(ShardId shardId, BlockState blockState) {}

  /** The blockchain client used to fetch more blocks. */
  final BlockchainClient client;

  /**
   * Holds the newest block for each shard that happened right after the cursor of the iterator. Is
   * empty at the end of iteration.
   */
  final List<BlockData> nextShardBlocks;

  /**
   * Create a Block Iterator with the given blockchain client. Loads the latest block for each
   * shard.
   *
   * @param client the blockchain client used for communicating with the reader nodes.
   */
  public BlockIterator(BlockchainClient client) {
    this(getTheLatestBlocksForEachShard(client), client);
  }

  /**
   * Create a Block Iterator with the given blockchain client and from a given time.
   *
   * @param client the blockchain client used for communicating with the reader nodes.
   * @param utcTime the timestamp for which to find the latest block.
   */
  public BlockIterator(BlockchainClient client, long utcTime) {
    this(getTheLatestBlockForEachShardBeforeTime(client, utcTime), client);
  }

  /**
   * Create a Block Iterator with the given blockchain client and for a given shardId.
   *
   * @param client the blockchain client used for communicating with the reader nodes.
   * @param shardId the shardId for which to create the block iterator.
   */
  public BlockIterator(BlockchainClient client, ShardId shardId) {
    this(getTheLatestBlocksForShard(client, shardId), client);
  }

  BlockIterator(List<BlockData> blockData, BlockchainClient client) {
    this.nextShardBlocks = blockData;
    this.client = client;
  }

  /**
   * Finds the latest block for each shard.
   *
   * @param client - the blockchain client used for communicating with the reader nodes.
   * @return The latest block for each shard
   */
  private static List<BlockData> getTheLatestBlocksForEachShard(BlockchainClient client) {
    // Find the latest block for each shard.
    List<BlockData> shardBlocks = new ArrayList<>();
    for (ShardId shardId : client.getShardManager().getShards()) {
      BlockState blockState = client.getLatestBlock(shardId);
      shardBlocks.add(new BlockData(shardId, blockState));
    }
    return shardBlocks;
  }

  /**
   * Finds the latest blocks for each shard, before a given UTC time.
   *
   * @param client - the blockchain client used for communicating with the reader nodes.
   * @param utcTime - the utc time to find blocks before.
   * @return The latest block for each shard before the supplied time.
   */
  private static List<BlockData> getTheLatestBlockForEachShardBeforeTime(
      BlockchainClient client, long utcTime) {
    List<BlockData> shardBlocks = new ArrayList<>();
    for (ShardId shardId : client.getShardManager().getShards()) {
      long blockNumber = client.getLatestBlockBeforeTime(shardId, utcTime);
      BlockState blockState = client.getBlockByNumber(shardId, blockNumber);
      shardBlocks.add(new BlockData(shardId, blockState));
    }
    return shardBlocks;
  }

  private static List<BlockData> getTheLatestBlocksForShard(
      BlockchainClient client, ShardId shardId) {
    BlockState blockState = client.getLatestBlock(shardId);
    return new ArrayList<>(Collections.singleton(new BlockData(shardId, blockState)));
  }

  /**
   * Returns the next element in iteration without advancing the underlying iterator. When only
   * genesis blocks remain, they are found based on shardId in the order: Gov -> Shard n -> Shard
   * n-1 -> ... -> Shard0.
   *
   * @return the next element in iteration. Null if there are no more elements.
   */
  public BlockData peek() {
    return nextShardBlocks.stream()
        .max(
            Comparator.comparingLong((BlockData o) -> o.blockState.productionTime())
                .thenComparing(o -> o.shardId().toString()))
        .orElse(null);
  }

  @Override
  public boolean hasNext() {
    return peek() != null;
  }

  @Override
  public BlockData next() {
    BlockData next = peek();
    if (next != null) {
      nextShardBlocks.remove(next);
      ShardId shardId = next.shardId;
      // If next is the genesis block, don't add its parent to nextShardBlocks
      if (!isGenesisBlock(next)) {
        BlockState nextShardBlock =
            client.getBlockByHash(shardId, Hash.fromString(next.blockState.parentBlock()));
        nextShardBlocks.add(new BlockData(shardId, nextShardBlock));
      }
    }
    return next;
  }

  private static boolean isGenesisBlock(BlockData blockData) {
    String genesisParentHash = "0000000000000000000000000000000000000000000000000000000000000000";
    return blockData.blockState.parentBlock().equals(genesisParentHash);
  }
}
