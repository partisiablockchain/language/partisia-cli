package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.partisiacli.executionenvironment.ClientProvider;
import com.partisiablockchain.language.partisiacli.parser.Blockchain;
import com.partisiablockchain.language.zkclient.RealZkClient;
import com.secata.tools.coverage.ExceptionConverter;
import java.net.URL;

/**
 * A mediator of all net-based communication from and to a blockchain. A blockchain net provides
 * clients for communicating with the underlying blockchain and delivers information to be used on
 * its associated webpage.
 */
public final class BlockchainNet {
  private static final String CONTRACT_INFO = "/contracts/";
  private static final String TRANSACTION_INFO = "/transactions/";
  private static final String ACCOUNT_INFO = "/accounts/";
  private final ClientProvider clientProvider;
  private final BlockchainUrls blockchainUrls;

  /**
   * Provides clients for communicating with the blockchain defined by the given REST base-endpoint
   * and prints information to be used on its associated webpage to the given output stream.
   *
   * @param clientProvider A provider of clients for blockchain communication.
   * @param blockchainUrls The urls of the blockchain.
   */
  public BlockchainNet(ClientProvider clientProvider, BlockchainUrls blockchainUrls) {
    this.clientProvider = clientProvider;
    this.blockchainUrls = blockchainUrls;
  }

  /**
   * Checks if the underlying network is the testnet.
   *
   * @return Whether the underlying network is the testnet or not.
   */
  public boolean isTestnet() {
    return blockchainUrls.restBaseUrl().equals(BlockchainUrls.TESTNET_URLS.restBaseUrl());
  }

  /**
   * Has the net been supplied with an url to a browser.
   *
   * @return whether the net been supplied with an url to a browser
   */
  public boolean hasWebUrl() {
    return !blockchainUrls.webBaseUrl().equals(BlockchainUrls.NO_URL);
  }

  /**
   * Returns a {@link BlockchainClient} defined by the given {@link Blockchain} for general
   * blockchain communication.
   *
   * @return A blockchain client.
   */
  public BlockchainClient getBlockchainClient() {
    return clientProvider.blockchainClient(blockchainUrls.restBaseUrl(), blockchainUrls.noShards());
  }

  /**
   * Returns a {@link BlockchainTransactionClient} defined by the given {@link Blockchain} for
   * sending transactions to the blockchain.
   *
   * @param sender The sender to use when signing transactions.
   * @return A blockchain transaction client.
   */
  public BlockchainTransactionClient getBlockchainTransactionClient(SenderAuthentication sender) {
    return clientProvider.blockchainTransactionClient(
        blockchainUrls.restBaseUrl(), blockchainUrls.noShards(), sender);
  }

  /**
   * Returns a {@link BlockchainTransactionClient} defined by the given {@link Blockchain} for
   * sending transactions to the blockchain.
   *
   * @param sender The sender to use when signing transactions.
   * @param validityDuration The amount of time in milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @return A blockchain transaction client.
   */
  public BlockchainTransactionClient getBlockchainTransactionClient(
      SenderAuthentication sender, long validityDuration) {
    return clientProvider.blockchainTransactionClient(
        blockchainUrls.restBaseUrl(), blockchainUrls.noShards(), sender, validityDuration);
  }

  /**
   * Get the {@link ApiClient} targeting this net.
   *
   * @return an api client
   */
  public ApiClient getApiClient() {
    return clientProvider.apiClient(blockchainUrls.restBaseUrl());
  }

  /**
   * Get the Zk client for a specific Zk contract.
   *
   * @param contractAddress the address of the Zk contract.
   * @return the Zk client for the contract.
   */
  public RealZkClient getZkClient(BlockchainAddress contractAddress) {
    return clientProvider.getZkClient(contractAddress, getBlockchainClient());
  }

  /**
   * Get the browser url of an executed transaction.
   *
   * @param transactionHash identifier of the transaction
   * @return browser url of an executed transaction
   */
  public URL getEventUrl(Hash transactionHash) {
    String eventUrl = blockchainUrls.webBaseUrl() + TRANSACTION_INFO + transactionHash;
    return ExceptionConverter.call(() -> new URL(eventUrl));
  }

  /**
   * Get the browser url for a contract.
   *
   * @param deployedContractAddress the contract address
   * @return browser url for the contract
   */
  public URL getDeployUrl(BlockchainAddress deployedContractAddress) {
    String deployString =
        blockchainUrls.webBaseUrl() + CONTRACT_INFO + deployedContractAddress.writeAsString();
    return ExceptionConverter.call(() -> new URL(deployString));
  }

  /**
   * Retrieves the url of a blockchain account.
   *
   * @param accountAddress The address of the account.
   * @return The URL of the account address.
   */
  public URL getAccountUrl(BlockchainAddress accountAddress) {
    String accountUrl = blockchainUrls.webBaseUrl() + ACCOUNT_INFO + accountAddress.writeAsString();
    return ExceptionConverter.call(() -> new URL(accountUrl));
  }
}
