package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * The URLs of a blockchain. A blockchain can be interacted with using its REST-endpoint or its Web
 * URL.
 */
public record BlockchainUrls(String restBaseUrl, String webBaseUrl, int noShards) {

  /** The REST-endpoint for the mainnet. */
  public static final String MAINNET_REST_URL = "https://reader.partisiablockchain.com";

  /** The web url for the mainnnet. */
  public static final String MAINNET_WEB_URL = "https://browser.partisiablockchain.com";

  /** The REST-endpoint for the testnet. */
  public static final String TESTNET_REST_URL = "https://node1.testnet.partisiablockchain.com";

  /** The web url for the testnet. */
  public static final String TESTNET_WEB_URL = "https://browser.testnet.partisiablockchain.com";

  /** Placeholder for blockchains that do not have an associated webpage. */
  public static final String NO_URL = "";

  /**
   * The shorthand for the blockchain urls: ({@value MAINNET_REST_URL}, {@value MAINNET_WEB_URL}).
   */
  public static final String MAINNET = "mainnet";

  /**
   * The shorthand for the blockchain urls: ({@value TESTNET_REST_URL}, {@value TESTNET_WEB_URL}).
   */
  public static final String TESTNET = "testnet";

  /** The blockchain urls of the mainnet. */
  public static BlockchainUrls MAINNET_URLS =
      new BlockchainUrls(MAINNET_REST_URL, MAINNET_WEB_URL, 3);

  /** The blockchain urls of the testnet. */
  public static BlockchainUrls TESTNET_URLS =
      new BlockchainUrls(TESTNET_REST_URL, TESTNET_WEB_URL, 3);

  /**
   * Uses the given pair of URLs to interact with the associated blockchain.
   *
   * @param restBaseUrl The base of the REST-endpoint for the targeted blockchain. Used by the
   *     clients to interact with the blockchain. An example is {@value
   *     BlockchainUrls#MAINNET_REST_URL}.
   * @param webBaseUrl The base of the URL for the targeted blockchain. Used to provide links to a
   *     blockchain's associated webpage. An example is the <a
   *     href="https://testnet.partisiablockchain.com/">Testnet</a>
   * @param noShards The number of shards on the blockchain. For testnet and mainnet this is three.
   */
  public BlockchainUrls {}
}
