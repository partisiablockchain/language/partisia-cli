package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.Blockchain;
import com.partisiablockchain.language.partisiacli.parser.RestAndWebUrlsConverter;
import com.secata.tools.coverage.ExceptionConverter;
import picocli.CommandLine;

/**
 * The cli command executor responsible for executing the underlying business logic of a command
 * given to the interface.
 */
public final class CommandExecutor {

  private final ExecutionEnvironment executionEnvironment;

  /**
   * Runs the commands given to cli. May retrieve state information about the targeted blockchain,
   * alter it by sending transactions or output a file for later use.
   *
   * @param executionEnvironment The environment to be used when executing the commands.
   */
  CommandExecutor(ExecutionEnvironment executionEnvironment) {
    this.executionEnvironment = executionEnvironment;
  }

  /**
   * Executes a cli command-line. May retrieve state information about the targeted blockchain or
   * alter it by sending transactions. This requires some net-based communication with the targeted
   * blockchain. Alternatively, a file may be output. The exact file type depends on the underlying
   * command.
   *
   * @param commandLine The command-line as a parsed object.
   */
  public void execute(CommandLine.ParseResult commandLine) {
    Blockchain blockchain = (Blockchain) commandLine.commandSpec().userObject();
    blockchain.setVerbose();
    UserConfig userConfig =
        new UserConfigStorage(executionEnvironment.fileManager(), executionEnvironment.configPath())
            .loadConfig();
    String targetNet = blockchain.netname;
    if (targetNet == null) {
      targetNet = userConfig.defaultNet();
    }

    BlockchainUrls blockchainUrls = getNamedNet(userConfig, targetNet);

    if (blockchainUrls == null) {
      RestAndWebUrls restAndWebUrls =
          new RestAndWebUrlsConverter()
              .convertRestAndWebUrls(targetNet, commandLine.commandSpec().commandLine());
      String restBaseUrl = restAndWebUrls.restBaseUrl();

      ApiClient apiClient = executionEnvironment.clientProvider().apiClient(restBaseUrl);
      ChainControllerApi chainControllerApi = new ChainControllerApi(apiClient);
      int noShards =
          ExceptionConverter.call(
              () -> chainControllerApi.getChain(null).getShards().size(),
              "Could not reach the blockchain on the given blockchain node endpoint: "
                  + restAndWebUrls.restBaseUrl());

      blockchainUrls = new BlockchainUrls(restBaseUrl, restAndWebUrls.webBaseUrl(), noShards);
    }
    BlockchainNet blockchainNet =
        new BlockchainNet(executionEnvironment.clientProvider(), blockchainUrls);

    executeCommand(commandLine, blockchainNet);
  }

  /**
   * Executes the command associated with the given command-line.
   *
   * @param commandLine The command-line as a parsed object.
   * @param blockchainNet A blockchain net for blockchain communication.
   */
  private void executeCommand(CommandLine.ParseResult commandLine, BlockchainNet blockchainNet) {
    CommandLine.ParseResult command = commandLine.subcommand();

    Command subcommand =
        (Command)
            (command.subcommand().hasSubcommand()
                ? command.subcommand().subcommand().commandSpec().userObject()
                : command.subcommand().commandSpec().userObject());

    subcommand.execute(executionEnvironment, blockchainNet);
  }

  private static BlockchainUrls getNamedNet(UserConfig userConfig, String targetNet) {
    if (userConfig.netMap().containsKey(targetNet)) {
      NetInfo netInfo = userConfig.netMap().get(targetNet);
      return new BlockchainUrls(netInfo.restBaseUrl(), netInfo.webBaseUrl(), netInfo.noOfShards());
    } else if (targetNet.equals(BlockchainUrls.TESTNET)) {
      return BlockchainUrls.TESTNET_URLS;
    } else if (targetNet.equals(BlockchainUrls.MAINNET)) {
      return BlockchainUrls.MAINNET_URLS;
    } else {
      return null;
    }
  }
}
