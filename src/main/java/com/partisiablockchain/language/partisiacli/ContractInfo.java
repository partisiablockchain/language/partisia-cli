package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.traversal.AvlKeyType;
import com.partisiablockchain.dto.traversal.AvlTraverse;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.FnAbi;
import com.partisiablockchain.language.abimodel.model.FnKind;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A contract info provides information about a specific contract. This information could be the
 * contract's abi, state or local account state.
 */
public final class ContractInfo {

  /** Singleton used to represent that no ABI exists. */
  private static final FileAbi NO_ABI =
      new FileAbi("", new AbiVersion(0, 0, 0), new AbiVersion(0, 0, 0), 0, null);

  private final BlockchainAddress address;

  private final Path filePath;
  private final BlockchainClient client;
  private final FileManager fileManager;

  /** Cached ABI for the contract. */
  private FileAbi abi;

  /** Cached state of the contract in binary. */
  private ContractState binaryContractState;

  /** Cached state of the contract in JSON. */
  private ContractState jsonContractState;

  /**
   * Provides information about a specific contract, such as the contract abi, state or local
   * account state.
   *
   * @param address The address of the contract.
   * @param client A client to retrieve information about the contract from the blockchain.
   * @param filePath The path to the file that contains an Abi for this contract. May be a .abi
   *     file, a .pbc file or null.
   * @param fileManager A file manager to load the abi file.
   */
  public ContractInfo(
      BlockchainAddress address, BlockchainClient client, Path filePath, FileManager fileManager) {
    this.address = address;
    this.filePath = filePath;
    this.client = client;
    this.fileManager = fileManager;
    this.abi = null;
    this.binaryContractState = null;
  }

  /**
   * Load and parse an abi from a local .abi or .pbc file. A {@link RuntimeException} is thrown in
   * case of any IO-exceptions.
   *
   * @param filePath The path to the file from which to read abi.
   * @param fileManager A file manager to load files with.
   * @return The parsed abi.
   */
  public static FileAbi loadFileAbi(Path filePath, FileManager fileManager) {
    FileInfo fileInfo = new FileInfo(filePath, fileManager);
    byte[] abiBytes;
    if (fileInfo.fileType == FileInfo.FileType.Abi) {
      abiBytes = fileInfo.fileBytes;
    } else {
      abiBytes =
          ExceptionConverter.call(
              () -> PbcFile.fromBytes(fileInfo.fileBytes).getAbiBytes(),
              "Failed to parse pbc file: %s".formatted(filePath));
    }
    return new AbiParser(abiBytes).parseAbi();
  }

  /**
   * Retrieves and parses the abi located at the provided contract address. Null is returned if no
   * abi is associated with the contract.
   *
   * @return The parsed abi of this contract.
   */
  public FileAbi getAbiFromBlockchain() {
    ContractState stateJson = getStateJson();
    byte[] abi = stateJson.abi();
    if (abi.length == 0) {
      return null;
    }
    return new AbiParser(abi).parseAbi();
  }

  /**
   * Retrieves the parsed abi of this contract either from the blockchain or a file. If no ABI could
   * be retrieved, throws a {@link RuntimeException}.
   *
   * @return The parsed abi of this contract.
   */
  public FileAbi getAbiRequired() {
    FileAbi fileAbi = getAbi();
    if (fileAbi == null) {
      throw new RuntimeException(
          ("Contract at '%s' does not have an abi. "
                  + "%nTry providing it instead using '--abi <abi-file>'. "
                  + "%nOtherwise try using the 'raw' subcommand.")
              .formatted(getAddress().writeAsString()));
    }
    return fileAbi;
  }

  /**
   * Retrieves the contract's parsed abi from this contract's cache. If no abi is stored in the
   * cache, retrieves it from either the chain or a file instead. This may produce a null result, if
   * no file is provided and no abi exists on chain.
   *
   * @return The contract's parsed abi.
   */
  public FileAbi getAbi() {
    if (abi != null) {
      // ABI already loaded.
      return abi == NO_ABI ? null : abi;
    }
    FileAbi fileAbi;
    if (filePath == null) {
      fileAbi = getAbiFromBlockchain();
    } else {
      fileAbi = loadFileAbi(filePath, fileManager);
    }
    // Cache the result
    this.abi = fileAbi == null ? NO_ABI : fileAbi;
    return fileAbi;
  }

  /**
   * Retrieves the contract's state from this contract's cache. If no state is stored in the cache,
   * retrieves it from chain instead.
   *
   * @return The contract's state.
   */
  public ContractState getStateBinary() {
    if (binaryContractState == null) {
      binaryContractState = client.getContractState(address, BlockchainClient.StateOutput.BINARY);
    }
    return binaryContractState;
  }

  /**
   * Retrieves the contract's avl state from the chain.
   *
   * @return The contract's state.
   */
  public ContractState getStateAvlBinary() {
    return client.getContractState(address, BlockchainClient.StateOutput.AVL_BINARY);
  }

  /**
   * Retrieve the contract's state from chain as JSON.
   *
   * @return The contract's state as JSON.
   */
  public ContractState getStateJson() {
    if (jsonContractState == null) {
      jsonContractState = client.getContractState(address, BlockchainClient.StateOutput.JSON);
    }
    return jsonContractState;
  }

  /**
   * Gets the local account state of this contract. This state consists of various meta-data such as
   * the amount of gas that the contract currently holds and the amount of coins stored with the
   * contract.
   *
   * @return The local account state as JSON.
   */
  public JsonNode getLocalAccountState() {
    TraversePath path =
        new TraversePath(
            List.of(
                new FieldTraverse("contracts"),
                new AvlTraverse(AvlKeyType.BLOCKCHAIN_ADDRESS, address.writeAsString())));
    ShardId shard = client.getShardManager().getShard(address);
    return client.traverseLocalAccountPlugin(shard, path);
  }

  /**
   * Retrieves the ABI of the provided action. Throws a {@link RuntimeException} if the action could
   * not be found, or if no action name was provided.
   *
   * @param actionName The name of the action whose ABI to retrieve.
   * @return The action's ABI.
   */
  public FnAbi fnAbiFromString(String actionName) {
    String errorMessage = "Action '%s' does not exist. ".formatted(actionName);
    if (actionName.isEmpty()) {
      errorMessage = "Missing required parameter: '<action-name>'. ";
    }
    return Objects.requireNonNull(
        abi.contract().getFunctionByName(actionName),
        errorMessage
            .concat("Possible actions are:%n%s")
            .formatted(getCallableActionNames(abi.contract())));
  }

  private static String getCallableActionNames(ContractAbi contractAbi) {
    return getCallableActions(contractAbi).stream()
        .map(FnAbi::name)
        .collect(Collectors.joining("\n"));
  }

  /**
   * Returns the set of actions from the provided contract that are callable using a signed
   * transaction.
   *
   * @param contractAbi The contract whose callable actions are to be retrieved.
   * @return The set of callable actions in this contract.
   */
  public static List<FnAbi> getCallableActions(ContractAbi contractAbi) {
    return contractAbi.functions().stream().filter(ContractInfo::callableActionsFilter).toList();
  }

  private static boolean callableActionsFilter(FnAbi fnAbi) {
    return fnAbi.kind() == FnKind.Action
        || fnAbi.kind() == FnKind.ZkSecretInput
        || fnAbi.kind() == FnKind.ZkSecretInputWithExplicitType;
  }

  /**
   * Gets this contract's address.
   *
   * @return The address of this contract.
   */
  public BlockchainAddress getAddress() {
    return address;
  }
}
