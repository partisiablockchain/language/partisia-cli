package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import java.nio.file.Path;

/** A contract info provides information about a file eg. its type and path. */
public final class FileInfo {
  /** The possible file types. */
  public enum FileType {
    /** .abi file. */
    Abi,
    /** .pbc file. */
    Pbc,
    /** .wasm file. */
    Wasm,
    /** .zkwa file. */
    Zkwa,
  }

  /** The path of the file. */
  public final Path filePath;

  /** The type of the file. */
  public final FileType fileType;

  /** The bytes of the file. */
  public final byte[] fileBytes;

  /**
   * Provides info about a file.
   *
   * @param filePath The path to the file.
   * @param fileManager File manager used to read the bytes of the file.
   */
  public FileInfo(Path filePath, FileManager fileManager) {
    this.fileBytes = fileManager.readBytes(filePath, "Failed to read file: %s".formatted(filePath));
    this.filePath = filePath;
    this.fileType = determineFileType(filePath);
  }

  private static FileType determineFileType(Path path) {
    if (path.toString().endsWith(".abi")) {
      return FileType.Abi;
    }
    if (path.toString().endsWith(".pbc")) {
      return FileType.Pbc;
    }
    if (path.toString().endsWith(".wasm")) {
      return FileType.Wasm;
    }
    if (path.toString().endsWith(".zkwa")) {
      return FileType.Zkwa;
    }
    throw new RuntimeException("file must be one of either .abi, .pbc, .wasm, .zkwa");
  }
}
