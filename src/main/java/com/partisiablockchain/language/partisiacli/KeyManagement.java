package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.language.bip32.Bip32;
import com.partisiablockchain.language.bip32.ExtendedKey;
import com.partisiablockchain.language.bip39.Bip39;
import com.partisiablockchain.language.bip44.Bip44;
import com.partisiablockchain.language.partisiacli.account.create.Create;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;

/** Access and provide keys to execution of commands during runtime. */
public final class KeyManagement {

  /**
   * Coin type found <a
   * href="https://github.com/satoshilabs/slips/blob/master/slip-0044.md">here</a>.
   */
  private static final int PARTISIA_COIN_TYPE = 3757;

  private SenderAuthentication authentication;
  private final Path walletOverridePath;
  private final Path pkOverridePath;
  private final FileManager fileManager;
  private final UserConfig userConfig;

  /**
   * Create a new key manager.
   *
   * @param pkOverridePath the path for the private key file to use.
   * @param walletOverridePath the path for the wallet file to use.
   * @param fileManager the file manager to read the key with.
   * @param userConfig the configuration to find the wallet or private key path from, if none is
   *     supplied as override path.
   */
  public KeyManagement(
      Path pkOverridePath,
      Path walletOverridePath,
      FileManager fileManager,
      UserConfig userConfig) {
    this.authentication = null;
    this.fileManager = fileManager;
    this.userConfig = userConfig;
    this.walletOverridePath = walletOverridePath;
    this.pkOverridePath = pkOverridePath;
  }

  private static String readAuthenticationFile(Path walletPath, FileManager fileManager) {
    String errorMessage =
        walletPath.toString().isEmpty()
            ? "Cannot sign without a wallet or private key. Provide one using '--wallet <file>',"
                + " '--pk <file>' or set a private key\n"
                + " to use as default using \"cargo pbc config privatekey <path-to-pk-file>\"."
            : "Failed to read from: %s".formatted(walletPath.getFileName().toString());
    String seedPhrase = fileManager.readString(walletPath, errorMessage);
    if (seedPhrase.isEmpty()) {
      throw new RuntimeException(
          "The provided file %s was empty.".formatted(walletPath.getFileName().toString()));
    }
    return seedPhrase;
  }

  /**
   * Create the authenticator for a given key, used for signing transactions.
   *
   * @return the authenticator for the key.
   */
  public SenderAuthentication getAuthentication() {
    if (authentication != null) {
      return authentication;
    }
    String privateKey;
    if (walletOverridePath != null) {
      privateKey = getPrivateKeyFromPhrase(readAuthenticationFile(walletOverridePath, fileManager));
    } else {
      Path keyPath = getPath(pkOverridePath, userConfig);
      privateKey = readAuthenticationFile(keyPath, fileManager);
    }
    authentication = SenderAuthenticationKeyPair.fromString(privateKey);
    return authentication;
  }

  /**
   * Get the path of the file containing the private key, prioritising user specified input.
   *
   * @param overridePath User specified path.
   * @param userConfig Config to get path from if no user specified path is given.
   * @return the override path if provided, otherwise return the path in the user config.
   */
  private Path getPath(Path overridePath, UserConfig userConfig) {
    if (overridePath != null) {
      return overridePath;
    } else {
      return userConfig.pk();
    }
  }

  private static String getPrivateKeyFromPhrase(String mnemonic) {
    ExceptionConverter.run(() -> Bip39.validateMnemonic(mnemonic), "Invalid mnemonic phrase");

    byte[] seed = Bip39.mnemonicToSeed(mnemonic, "");

    ExtendedKey masterKey = Bip32.generateMasterKey(seed);
    ExtendedKey derivedKey = Bip44.deriveBip44Key(masterKey, PARTISIA_COIN_TYPE, 0, 0, 0);

    return derivedKey.getPrivateKeyHex();
  }

  /**
   * Determines where to output the private-key, prioritising user specified input.
   *
   * @param userSpecifiedTarget A custom target provided by a user.
   * @param accountAddress The name of the default file.
   * @return The destination of the account private-key.
   */
  private static Path determinePrivateKeyDestination(
      Path userSpecifiedTarget, BlockchainAddress accountAddress) {
    return userSpecifiedTarget != null
        ? userSpecifiedTarget
        : getDefaultPrivateKeyFile(accountAddress);
  }

  /**
   * Generates the default file given by '{@literal <}address{@literal >}.pk'.
   *
   * @param accountAddress The name of the default file.
   * @return The default file to output an account private-key to.
   */
  private static Path getDefaultPrivateKeyFile(BlockchainAddress accountAddress) {
    return Paths.get(accountAddress.writeAsString() + ".pk");
  }

  /**
   * Writes the provided private-key to the target file. The key is written in hexidecimal.
   *
   * @param privateKey The private-key to write in the file.
   * @param target The file to output to.
   * @param fileManager The file manager to write the private key with.
   */
  private static void writePrivateKey(BigInteger privateKey, Path target, FileManager fileManager) {
    String accountPrivateKeyHex = privateKey.toString(16);
    fileManager.writeString(
        accountPrivateKeyHex, target, "Failed to write private-key to %s.".formatted(target));
  }

  /**
   * Outputs the newly generated account's private-key to a file. The file will be a default file
   * generated from the account address, unless a user specified an explicit target file.
   *
   * @param privateKey The private-key of the account.
   * @param create The record containing optional user input.
   * @param accountAddress The address to use as a default name for the file.
   * @param fileManager The file manager to write the private key with.
   */
  public static void outputAccountPrivateKey(
      BigInteger privateKey,
      Create create,
      BlockchainAddress accountAddress,
      FileManager fileManager) {
    Path destination = determinePrivateKeyDestination(create.privateKeyFile, accountAddress);
    writePrivateKey(privateKey, destination, fileManager);
  }
}
