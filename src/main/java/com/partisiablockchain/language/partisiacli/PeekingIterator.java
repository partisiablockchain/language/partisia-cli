package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * An iterator that allows to inspect the next element of an iterable without advancing iteration.
 *
 * @param <T> The type of elements which this iterator iterates over.
 */
public final class PeekingIterator<T> implements Iterator<T> {
  private final Iterator<T> iterator;
  private CachedElement<T> nextElement;

  /**
   * Wraps the provided iterator and makes it possible to peek the next element of the iteration.
   *
   * @param iterator The iterator to extend with a peek method.
   */
  public PeekingIterator(Iterator<T> iterator) {
    this.iterator = Objects.requireNonNull(iterator);
    advanceIterator();
  }

  /**
   * Returns the next element without advancing the iteration. If the iteration is over, throws a
   * {@link NoSuchElementException}.
   *
   * @return The next element of the underlying iterable.
   */
  public T peek() {
    if (nextElement == null) {
      throw new NoSuchElementException();
    }
    return nextElement.element();
  }

  @Override
  public boolean hasNext() {
    return nextElement != null;
  }

  @Override
  public T next() {
    CachedElement<T> cachedElement = nextElement;
    if (cachedElement == null) {
      throw new NoSuchElementException();
    }
    advanceIterator();
    return cachedElement.element();
  }

  /**
   * Gets the next element and stores it in cache or terminates iteration if no more elements are
   * left.
   */
  private void advanceIterator() {
    if (iterator.hasNext()) {
      nextElement = new CachedElement<>(iterator.next());
    } else {
      nextElement = null;
    }
  }

  /**
   * A generic singleton for the elements of the provided iterator. This allows us to uniquely
   * distinguish when the provided iterator is exhausted.
   */
  private record CachedElement<T>(T element) {}

  @Override
  public void forEachRemaining(Consumer<? super T> action) {
    throw new UnsupportedOperationException("forEachRemaining");
  }
}
