package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Uses the given pair of URLs to interact with the associated blockchain.
 *
 * @param restBaseUrl The base of the REST-endpoint for the targeted blockchain. Used by the clients
 *     to interact with the blockchain. An example is {@value BlockchainUrls#MAINNET_REST_URL}.
 * @param webBaseUrl The base of the URL for the targeted blockchain. Used to provide links to a
 *     blockchain's associated webpage. An example is the {@value BlockchainUrls#TESTNET_WEB_URL}
 */
public record RestAndWebUrls(String restBaseUrl, String webBaseUrl) {}
