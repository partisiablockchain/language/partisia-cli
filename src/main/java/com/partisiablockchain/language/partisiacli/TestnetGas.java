package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;

/** Responsible for adding gas to an account on the Testnet. */
public final class TestnetGas {

  private TestnetGas() {}

  private static final BigInteger GAS_PRIVATE_KEY =
      new BigInteger(
          "50989053758728485505385510052308426598450638498658968089857168692220111993724");

  /** The address of the system contract for minting coins on the testnet. */
  private static final BlockchainAddress TESTNET_FAUCET_CONTRACT =
      BlockchainAddress.fromString("01860476afca938871ff2c49bf5490235445942e3e");

  private static final KeyPair GAS_KEY = new KeyPair(GAS_PRIVATE_KEY);
  private static final BlockchainAddress GAS_KEY_ADDRESS = GAS_KEY.getPublic().createAddress();

  /**
   * Add gas to an account, calling the faucet contract.
   *
   * @param address the address of the account to add gas to.
   * @param blockchainNet The blockchain net to be used for mediating blockchain communications.
   * @param out The printstream to print with.
   */
  public static void addGasToAccount(
      BlockchainAddress address, BlockchainNet blockchainNet, PrintStream out) {
    BlockchainTransactionClient blockchainTransactionClient =
        blockchainNet.getBlockchainTransactionClient(new SenderAuthenticationKeyPair(GAS_KEY));

    SentTransaction addGasTransaction =
        blockchainTransactionClient.signAndSend(
            Transaction.create(TESTNET_FAUCET_CONTRACT, SafeDataOutputStream.serialize(address)),
            20000);
    blockchainTransactionClient.waitForInclusionInBlock(addGasTransaction);

    SentTransaction refillTransaction =
        blockchainTransactionClient.signAndSend(
            Transaction.create(
                TESTNET_FAUCET_CONTRACT, SafeDataOutputStream.serialize(GAS_KEY_ADDRESS)),
            20000);
    blockchainTransactionClient.waitForInclusionInBlock(refillTransaction);
    out.printf("Added gas to %s.\n", address.writeAsString());
  }
}
