package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.zk.ZkInputBuilder;
import com.partisiablockchain.language.abimodel.model.ArgumentAbi;
import com.partisiablockchain.language.abimodel.model.FnAbi;
import com.partisiablockchain.language.partisiacli.contract.Contract;
import com.partisiablockchain.language.partisiacli.contract.show.Show;
import com.secata.stream.CompactBitArray;
import java.util.Arrays;

/** Generation of RPC where some secret input is included. */
public final class ZkActionRpcBuilder {
  private final ActionRpcBuilder actionRpcBuilder;

  /**
   * Generates the RPC for actions taking a secret input.
   *
   * @param actionRpcBuilder An rpc-builder to serialize arguments given as strings.
   */
  public ZkActionRpcBuilder(ActionRpcBuilder actionRpcBuilder) {
    this.actionRpcBuilder = actionRpcBuilder;
  }

  /**
   * Builds the RPC for the provided secret-input action using the contract provided to this
   * builder, to be included in the next block on-chain.
   *
   * @param zkAction The action taking a secret input.
   * @param actionParameters The values of the additional parameters of the secret-input action. May
   *     be empty.
   * @return The RPC for the provided action containing the serialized secret input along with the
   *     optional parameters.
   */
  public SecretByteInput buildSecretByteInput(FnAbi zkAction, String[] actionParameters) {
    if (actionParameters.length < 1) {
      throw new RuntimeException(
          ("Action '%s' requires a secret input. "
                  + "Use the '%s %s' subcommands to retrieve information about this actions' API.")
              .formatted(
                  zkAction.name(), Contract.class.getSimpleName(), Show.class.getSimpleName()));
    }

    ZkInputBuilder rpcBuilder =
        ZkInputBuilder.createZkInputBuilder(zkAction.name(), actionRpcBuilder.getAbi().contract());
    PeekingIterator<String> argumentIterator =
        new PeekingIterator<>(Arrays.stream(actionParameters).iterator());

    ArgumentAbi secretType = zkAction.secretArgument();

    actionRpcBuilder.serializeNextArgument(
        rpcBuilder, argumentIterator, secretType, zkAction.name());

    CompactBitArray compactSecretInput = rpcBuilder.getBits();

    byte[] publicRpc = actionRpcBuilder.serializeArguments(zkAction, argumentIterator);

    return new SecretByteInput(compactSecretInput, publicRpc);
  }

  /**
   * Contains the secret input along with the public RPC.
   *
   * @param secretInput The secret input.
   * @param publicRpc The public RPC.
   */
  public record SecretByteInput(CompactBitArray secretInput, byte[] publicRpc) {}
}
