package com.partisiablockchain.language.partisiacli.abi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import picocli.CommandLine;

/** The abi command used to interact with abi files. */
@CommandLine.Command(
    name = "abi",
    description = "Interact with ABI files.",
    synopsisSubcommandLabel = "COMMAND",
    subcommands = {Show.class, Codegen.class})
public final class Abi {

  /** Allow invalid Java identifiers. Not allowed by default. */
  @CommandLine.Option(
      names = "--lenient",
      description = "Allow invalid Java identifiers.",
      defaultValue = "false",
      scope = CommandLine.ScopeType.INHERIT)
  public boolean lenient;

  AbiParser.Strictness getStrictness() {
    return lenient ? AbiParser.Strictness.LENIENT : AbiParser.Strictness.STRICT;
  }

  static byte[] readAbiBytes(ExecutionEnvironment executionEnvironment, Path inputFilePath) {
    byte[] fileBytes =
        executionEnvironment
            .fileManager()
            .readBytes(inputFilePath, "Unable to resolve input path.");

    if (inputFilePath.toString().endsWith(".pbc")) {
      return ExceptionConverter.call(
          () -> PbcFile.fromBytes(fileBytes).getAbiBytes(),
          "unable to read input file: " + inputFilePath);
    }
    if (inputFilePath.toString().endsWith(".abi")) {
      return ExceptionConverter.call(
          () -> fileBytes, "unable to read input file: " + inputFilePath);
    }
    throw new RuntimeException("Input file must either be an abi file or a pbc file");
  }
}
