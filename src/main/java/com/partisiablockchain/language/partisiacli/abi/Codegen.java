package com.partisiablockchain.language.partisiacli.abi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abicodegen.AbiCodegen;
import com.partisiablockchain.language.abicodegen.CodegenOptions;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import picocli.CommandLine;

/** The codegen command used to generate java or typescript code based on an abi. */
@CommandLine.Command(
    name = "codegen",
    description = "Generate code to interact with a contract based on an abi.")
public final class Codegen implements Command {
  /** The parent command. */
  @CommandLine.ParentCommand Abi abiCommand;

  /** Mutually exclusive group of options of which exactly one is to be set. */
  static final class Language {
    /** Generate Java code. */
    @CommandLine.Option(names = "--java", description = "Generate java code.", required = true)
    public boolean java;

    /** Generate TypeScript code. */
    @CommandLine.Option(names = "--ts", description = "Generate typescript code.", required = true)
    public boolean ts;
  }

  /** Command spec. */
  @CommandLine.Spec public CommandLine.Model.CommandSpec spec;

  /**
   * The language in which the code is to be generated. Is required to be set using either --java or
   * --ts.
   */
  @CommandLine.ArgGroup(multiplicity = "1")
  Language language;

  /** The named types for which to generate a deserialize function. */
  @CommandLine.Option(
      names = "--deserialize",
      description = "Generate code for deserializing the given named types.")
  public List<String> deserializeNamedTypes = new ArrayList<>();

  /** Enable generating code for deserializing rpc. */
  @CommandLine.Option(
      names = "--deserialize-rpc",
      description = "Generate code for deserializing rpc.",
      defaultValue = "false")
  public boolean deserializeRpc;

  /** Enable generating code for deserializing callbacks. */
  @CommandLine.Option(
      names = "--serialize-callbacks",
      description = "Generate code for serializing callbacks.",
      defaultValue = "false")
  public boolean serializeCallbacks;

  /** Disable generating code for deserializing the contract state. */
  @CommandLine.Option(
      names = "--dont-serialize-state",
      description = "Do not generate code for deserializing contract state.",
      defaultValue = "false")
  public boolean dontSerializeState;

  /** Disable generating code for deserializing actions. */
  @CommandLine.Option(
      names = "--dont-serialize-actions",
      description = "Do not generate code for serializing actions.",
      defaultValue = "false")
  public boolean dontSerializeActions;

  /** Disable generating the '@AbiGenerated' annotation. */
  @CommandLine.Option(
      names = "--dont-annotate",
      description = "Do not generate the '@AbiGenerated' annotation.",
      defaultValue = "false")
  public boolean dontGenerateAnnotations;

  /** Generate the code as part of the given package. */
  @CommandLine.Option(
      names = "--package",
      description = "Generate the code in the given package.",
      paramLabel = "<package>")
  public String packageName;

  /**
   * Generate code for the contract deployed at the given contract address, instead of providing an
   * abi/pbc file.
   */
  @CommandLine.Option(
      names = "--contract",
      description = "Generate code for the contract deployed at the given contract address.",
      paramLabel = "<contract-address>",
      converter = AddressConverter.class)
  public BlockchainAddress address;

  /**
   * Path to ABI or PBC file and path to output the generated code, or just output path if contract
   * address is provided.
   */
  @CommandLine.Parameters(
      description =
          "Path to ABI or PBC file and path to output the generated code, or just output path if"
              + " contract address is provided.",
      arity = "1..2",
      paramLabel = "<abi-or-pbc-path,output-path> or <output-path>")
  public List<Path> inputOutputPaths;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {

    if (!validAbiInput()) {
      throw new CommandLine.ParameterException(
          spec.commandLine(),
          "You must provide one of the following: an abi file, a pbc file or a contract address."
              + "Additionally, you must provide a path to the output file.");
    }

    byte[] abi;
    Path outputPath;
    if (address != null) {
      abi =
          ExceptionConverter.call(
              () -> blockchainNet.getBlockchainClient().getContractState(address).abi(),
              "Could not fetch ABI for contract with address " + address.writeAsString());
      outputPath = inputOutputPaths.get(0);
    } else {
      abi = Abi.readAbiBytes(executionEnvironment, inputOutputPaths.get(0));
      outputPath = inputOutputPaths.get(1);
    }

    CodegenOptions codegenOptions = getCodegenOptions(outputPath);

    var codeGen =
        new AbiCodegen(
            new AbiParser(abi, abiCommand.getStrictness()).parseAbi().contract(), codegenOptions);
    String code = codeGen.generateCode();

    executionEnvironment
        .fileManager()
        .writeBytes(
            code.getBytes(StandardCharsets.UTF_8),
            outputPath,
            "an error occurred while generating code");
  }

  /**
   * Returns true only if either the address option is set and an output path is provided, or both
   * input and output paths are provided and the address is not.
   */
  private boolean validAbiInput() {
    return (inputOutputPaths.size() == 1 && address != null)
        || (inputOutputPaths.size() == 2 && address == null);
  }

  private CodegenOptions getCodegenOptions(Path outputPath) {
    CodegenOptions.TargetLanguage targetLanguage =
        language.java ? CodegenOptions.TargetLanguage.JAVA : CodegenOptions.TargetLanguage.TS;

    String fileName = outputPath.getFileName().toString();
    String fileExtension = language.java ? ".java" : ".ts";
    int index = fileName.indexOf(fileExtension);
    if (index == -1) {
      throw new RuntimeException(
          String.format(
              "The output file '%s' has an invalid filetype. The output path must end in '%s'.",
              fileName, fileExtension));
    }
    String contractName = fileName.substring(0, index);

    return new CodegenOptions(
        targetLanguage,
        contractName,
        abiCommand.getStrictness(),
        packageName,
        !dontSerializeState,
        !dontSerializeActions,
        serializeCallbacks,
        deserializeRpc,
        Set.copyOf(deserializeNamedTypes),
        !dontGenerateAnnotations);
  }
}
