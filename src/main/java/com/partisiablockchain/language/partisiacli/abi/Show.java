package com.partisiablockchain.language.partisiacli.abi;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.abimodel.parser.RustSyntaxPrettyPrinter;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import picocli.CommandLine;

/** The show command in abi used to show information about an abi. */
@CommandLine.Command(name = "show", description = "Show basic information about an abi")
public final class Show implements Command {

  /** Command spec. */
  @CommandLine.Spec public CommandLine.Model.CommandSpec spec;

  /** The parent command. */
  @CommandLine.ParentCommand Abi abiCommand;

  @CommandLine.Parameters(
      paramLabel = "<input-path>",
      arity = "0..1",
      description = ".abi or .pbc file to show.")
  Path abiPath;

  @CommandLine.Option(
      names = "--contract",
      description = "Show abi of the contract deployed at the given contract address.",
      paramLabel = "<contract-address>",
      converter = AddressConverter.class)
  BlockchainAddress address;

  private static final class PrintOptions {
    @CommandLine.Option(
        names = {"--all-invocations"},
        defaultValue = "false",
        description = "List all the invocations of the contract.")
    boolean printInvocations;

    @CommandLine.Option(
        names = {"--actions"},
        defaultValue = "false",
        description = "Print the init and actions of the contract.")
    boolean actions;

    @CommandLine.Option(
        names = {"--callbacks"},
        defaultValue = "false",
        description = "Print the callbacks of the contract.")
    boolean callbacks;

    @CommandLine.Option(
        names = {"--state"},
        defaultValue = "false",
        description = "Print the contract state.")
    boolean printState;

    @CommandLine.Option(
        names = {"--binder-version"},
        defaultValue = "false",
        description = "Print the binder version of the contract.")
    boolean printBinderVersion;

    @CommandLine.Option(
        names = {"--client-version"},
        defaultValue = "false",
        description = "Print the client version of the contract.")
    boolean printClientVersion;

    boolean printAll() {
      return !(printInvocations
          || actions
          || callbacks
          || printState
          || printBinderVersion
          || printClientVersion);
    }

    boolean printActions() {
      return actions && !printInvocations;
    }

    boolean printCallbacks() {
      return callbacks && !printInvocations;
    }
  }

  @CommandLine.ArgGroup(exclusive = false)
  Show.PrintOptions options = new Show.PrintOptions();

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {

    if (!validAbiInput()) {
      throw new CommandLine.ParameterException(
          spec.commandLine(),
          "You must provide either the abi or pbc path, or a contract address.");
    }

    // Get the abi either by using the contract address or from the abi/pbc path.
    byte[] abi;
    if (address != null) {
      abi =
          ExceptionConverter.call(
              () -> blockchainNet.getBlockchainClient().getContractState(address).abi(),
              "Could not fetch ABI for contract with address " + address.writeAsString());
    } else {
      abi = Abi.readAbiBytes(executionEnvironment, abiPath);
    }

    FileAbi fileAbi = new AbiParser(abi, abiCommand.getStrictness()).parseAbi();
    StringBuilder builder = getPrintFromOptions(fileAbi);

    executionEnvironment.out().println(builder);
  }

  /** Builds the string to print according to the given options. */
  private StringBuilder getPrintFromOptions(FileAbi fileAbi) {
    RustSyntaxPrettyPrinter printer = new RustSyntaxPrettyPrinter(fileAbi);
    StringBuilder builder = new StringBuilder();
    if (options.printAll()) {
      builder.append(printer.printModel());
    }
    if (options.printBinderVersion) {
      builder.append(printer.printBinderVersion());
    }
    if (options.printClientVersion) {
      builder.append(printer.printClientVersion());
    }
    if (options.printState) {
      builder.append(printer.printState());
    }
    if (options.printActions()) {
      builder.append(printer.printActions());
    }
    if (options.printCallbacks()) {
      builder.append(printer.printCallbacks());
    }
    if (options.printInvocations) {
      builder.append(printer.printAllInvocations());
    }
    return builder;
  }

  /** Returns true if exactly one of the parameters are set. */
  private boolean validAbiInput() {
    return address != null && abiPath == null || address == null && abiPath != null;
  }
}
