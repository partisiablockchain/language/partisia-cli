package com.partisiablockchain.language.partisiacli.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.account.create.Create;
import com.partisiablockchain.language.partisiacli.account.keyaddress.KeyAddress;
import com.partisiablockchain.language.partisiacli.account.mintgas.MintGas;
import com.partisiablockchain.language.partisiacli.account.show.Show;
import picocli.CommandLine;

/** The account command is used to interact with contracts on the chain. */
@CommandLine.Command(
    name = "account",
    description = "Account creation and information about accounts on the Partisia Blockchain.",
    synopsisSubcommandLabel = "COMMAND",
    subcommands = {Create.class, Show.class, MintGas.class, KeyAddress.class})
public final class Account {}
