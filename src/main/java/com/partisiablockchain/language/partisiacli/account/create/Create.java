package com.partisiablockchain.language.partisiacli.account.create;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Curve;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.TestnetGas;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Random;
import picocli.CommandLine;

/** Create a new account to use on the blockchain. */
@CommandLine.Command(
    name = "create",
    description =
        """
        Create a private key and print the associated address on the blockchain.
        By default, the private key will be output to the file '<address>.pk'.
        If the target is the Testnet, then the account is also filled with gas.""")
public final class Create implements Command {

  /** The path to output the generated private key to the specified file instead. */
  @CommandLine.Option(
      names = "--file",
      paramLabel = "<path>",
      description = "Output the generated private key to the specified file.")
  public Path privateKeyFile;

  /** Create a new account, and if the target chain is the Testnet, then fill it with gas. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    PrintStream out = executionEnvironment.out();
    Random rng = executionEnvironment.rng();

    BigInteger accountPrivateKey = new BigInteger(Curve.CURVE.getN().bitLength(), rng);
    KeyPair accountKeyPair = new KeyPair(accountPrivateKey);
    BlockchainAddress accountAddress = accountKeyPair.getPublic().createAddress();

    out.printf("Address: %s\n", accountAddress.writeAsString());

    KeyManagement.outputAccountPrivateKey(
        accountKeyPair.getPrivateKey(), this, accountAddress, executionEnvironment.fileManager());

    if (blockchainNet.isTestnet()) {
      TestnetGas.addGasToAccount(accountAddress, blockchainNet, out);
    }
  }
}
