package com.partisiablockchain.language.partisiacli.account.keyaddress;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import java.io.PrintStream;
import java.math.BigInteger;
import java.nio.file.Path;
import picocli.CommandLine;

/** Prints the blockchain address of a private key. */
@CommandLine.Command(
    name = "address",
    description = "Print the blockchain address for a given private key.")
public final class KeyAddress implements Command {

  /** The file containing the private key to print the blockchain address for. */
  @CommandLine.Parameters(
      paramLabel = "<path-to-private-key>",
      description = "Print the address of the given private key.")
  public Path privateKeyFile;

  /** The path to output the generated private key to the specified file instead. */
  @CommandLine.Option(
      names = "--link",
      description = "Print a link to the account page for the given private key.")
  public boolean link;

  /** Print the blockchain address of a given key. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    PrintStream out = executionEnvironment.out();

    String pk =
        executionEnvironment
            .fileManager()
            .readString(
                privateKeyFile, "Failed to read private key at " + privateKeyFile.toString());
    BlockchainAddress addressForKey =
        new KeyPair(new BigInteger(pk, 16)).getPublic().createAddress();
    if (link) {
      out.printf("Browser link to account: %s", blockchainNet.getAccountUrl(addressForKey));
    } else {
      out.print(addressForKey.writeAsString());
    }
  }
}
