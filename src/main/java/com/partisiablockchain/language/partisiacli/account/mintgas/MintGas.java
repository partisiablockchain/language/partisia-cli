package com.partisiablockchain.language.partisiacli.account.mintgas;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.TestnetGas;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import picocli.CommandLine;

/** Builds, signs and sends a transaction payload that mints gas on the testnet. */
@CommandLine.Command(
    name = "mintgas",
    description = "Mint gas for the given account. (This is only possible on the testnet)")
public final class MintGas implements Command {

  /** The address of the user to give more gas to. */
  @CommandLine.Parameters(
      paramLabel = "<account-address>",
      description = "The address of the account to mint gas to.",
      converter = AddressConverter.class)
  public BlockchainAddress gasRecipient;

  /** Fill the given account with gas. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    if (blockchainNet.isTestnet()) {
      TestnetGas.addGasToAccount(gasRecipient, blockchainNet, executionEnvironment.out());
    } else {
      throw new RuntimeException("Cannot mint gas, unless the target is Testnet.");
    }
  }
}
