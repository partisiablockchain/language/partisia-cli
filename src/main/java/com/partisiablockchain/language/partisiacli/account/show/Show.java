package com.partisiablockchain.language.partisiacli.account.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import java.io.PrintStream;
import picocli.CommandLine;

/** Show the public info for a given account. */
@CommandLine.Command(name = "show", description = "Show information about an account (as JSON).")
public final class Show implements Command {

  /** The address of the account to show information about. */
  @CommandLine.Parameters(
      paramLabel = "<address>",
      description = "The address of the account to show information about.",
      index = "0",
      converter = AddressConverter.class)
  public BlockchainAddress accountAddress;

  /** Show the public info for a given account (as JSON). */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    PrintStream out = executionEnvironment.out();

    AddressValidator.requireAddressType(accountAddress, AddressValidator.AddressTypeSubsets.USER);
    BlockchainClient blockchainClient = blockchainNet.getBlockchainClient();
    JsonNode accountInfo = blockchainClient.getAccountInfo(accountAddress);
    out.print(accountInfo.toPrettyString());
  }
}
