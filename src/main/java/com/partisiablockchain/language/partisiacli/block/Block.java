package com.partisiablockchain.language.partisiacli.block;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.block.latest.Latest;
import com.partisiablockchain.language.partisiacli.block.show.Show;
import picocli.CommandLine;

/** The block command is used to view latest and specific blocks. */
@CommandLine.Command(
    name = "block",
    synopsisSubcommandLabel = "COMMAND",
    description = "View latest or specific blocks",
    subcommands = {Show.class, Latest.class})
public final class Block {}
