package com.partisiablockchain.language.partisiacli.block.latest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.language.partisiacli.BlockIterator;
import java.io.PrintStream;

/** Prints the identifiers of the latest blocks, by default 10. */
final class LatestBlocksPrinter {

  private final Latest latest;
  private final BlockchainClient client;
  private final PrintStream out;
  private final ObjectMapper objectMapper;

  /**
   * Prints the identifiers of the latest blocks , the number of printed is 10 by default, otherwise
   * it is specified by the given {@link Latest}.
   *
   * @param latest The latest record specifying what and how many to print.
   * @param client A client to retrieve information about the latest blocks from the blockchain.
   * @param out A stream to output the transaction identifiers on.
   */
  LatestBlocksPrinter(Latest latest, BlockchainClient client, PrintStream out) {
    this.latest = latest;
    this.client = client;
    this.out = out;
    this.objectMapper = new ObjectMapper();
  }

  /** Prints the latest blocks. */
  void printLatestBlocks() {
    BlockIterator blocks = new BlockIterator(client);
    int numTx = 0;
    while (numTx < Integer.parseInt(latest.number)) {

      BlockIterator.BlockData blockData = blocks.next();

      BlockState blockState = blockData.blockState();
      JsonNode blockJson = objectMapper.convertValue(blockState, JsonNode.class);
      out.println(blockJson.toPrettyString());
      numTx++;
    }
  }
}
