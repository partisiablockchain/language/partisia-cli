package com.partisiablockchain.language.partisiacli.block.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.BlockState;
import java.io.PrintStream;
import java.util.List;

/** A block information printer prints all information about its provided block. */
final class BlockInformationPrinter {

  private final Show show;
  private final BlockchainClient client;
  private final PrintStream out;
  private final ObjectMapper objectMapper;

  /**
   * Prints information about a block, specified by the given {@link Show} record.
   *
   * @param show The show record specifying which block to print information about.
   * @param client A client to retrieve information about the block from the blockchain.
   * @param out A stream to output information on.
   */
  BlockInformationPrinter(Show show, BlockchainClient client, PrintStream out) {
    this.show = show;
    this.client = client;
    this.out = out;
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Prints all information about the block provided to this printer, as given by the Show command,
   * as JSON.
   */
  void printInfo() {
    BlockState block = locateBlock();
    JsonNode blockJson = objectMapper.convertValue(block, JsonNode.class);

    out.println(blockJson.toPrettyString());
  }

  /**
   * Searches all shards of the targeted blockchain for the provided block. A {@link
   * RuntimeException} is thrown if no block is found.
   *
   * @return The BlockState having the provided identifier.
   */
  @SuppressWarnings("EmptyCatchBlock")
  private BlockState locateBlock() {
    Hash blockHash = Hash.fromString(show.blockIdentifier);
    List<ShardId> shards = client.getShardManager().getShards();
    BlockState block = null;
    for (ShardId shard : shards) {
      try {
        block = client.getBlockByHash(shard, blockHash);
        break;
      } catch (Exception ignored) {
      }
    }
    if (block == null) {
      throw new RuntimeException(
          "Could not find block with id '%s'.".formatted(show.blockIdentifier));
    }
    return block;
  }
}
