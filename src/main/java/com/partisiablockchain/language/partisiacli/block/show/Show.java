package com.partisiablockchain.language.partisiacli.block.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import picocli.CommandLine;

/** Shows information about a block (as JSON). */
@CommandLine.Command(name = "show", description = "Show information about a block (as JSON).")
public final class Show implements Command {

  /** The hash of the block to show information about. */
  @CommandLine.Parameters(
      index = "0",
      paramLabel = "<block-identifier>",
      description = "The hash of the block to show information about.")
  public String blockIdentifier;

  /** Shows information about a block (as JSON). */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    BlockInformationPrinter blockInformationPrinter =
        new BlockInformationPrinter(
            this, blockchainNet.getBlockchainClient(), executionEnvironment.out());
    blockInformationPrinter.printInfo();
  }
}
