package com.partisiablockchain.language.partisiacli.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.config.listconfig.ListConfig;
import com.partisiablockchain.language.partisiacli.config.net.Net;
import com.partisiablockchain.language.partisiacli.config.privatekey.PrivateKey;
import picocli.CommandLine;

/** The config command is used to configure properties like private key . */
@CommandLine.Command(
    name = "config",
    description = "Set default values for options used during execution of commands.",
    synopsisSubcommandLabel = "COMMAND",
    subcommands = {PrivateKey.class, Net.class, ListConfig.class})
public final class Config {}
