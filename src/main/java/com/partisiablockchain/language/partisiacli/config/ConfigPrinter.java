package com.partisiablockchain.language.partisiacli.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.io.PrintStream;

/** Prints the fields and values for the currently set configuration. */
public final class ConfigPrinter {
  private final PrintStream out;
  private final UserConfig config;

  /**
   * Prints the fields and values for the currently set configuration.
   *
   * @param userConfig The current configuration.
   * @param out Stream to output the configuration on.
   */
  public ConfigPrinter(UserConfig userConfig, PrintStream out) {
    this.out = out;
    this.config = userConfig;
  }

  /** Prints the configuration. */
  public void printConfig() {
    out.println("privatekey: " + config.pk().toString().replace(File.separatorChar, '/'));
    out.println("defaultNet: " + config.defaultNet());
    out.println(
        "netMap: "
            + ExceptionConverter.call(
                () ->
                    new ObjectMapper()
                        .writerWithDefaultPrettyPrinter()
                        .writeValueAsString(config.netMap())));
  }
}
