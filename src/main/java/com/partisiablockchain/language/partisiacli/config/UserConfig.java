package com.partisiablockchain.language.partisiacli.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.language.partisiacli.BlockchainUrls;
import com.partisiablockchain.language.partisiacli.NetInfo;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * User configuration when using the cli tool.
 *
 * @param pk path for private key.
 * @param defaultNet name of target defaultNet.
 * @param netMap named target net configurations
 */
public record UserConfig(
    @JsonSerialize(using = RelativePathSerializer.class) Path pk,
    String defaultNet,
    Map<String, NetInfo> netMap) {

  /**
   * Default constructor.
   *
   * @param pk path for private key.
   * @param defaultNet name of target defaultNet.
   * @param netMap named target net configurations
   */
  public UserConfig {
    pk = Objects.requireNonNullElse(pk, Path.of(""));
    defaultNet = Objects.requireNonNullElse(defaultNet, BlockchainUrls.TESTNET);
    netMap = Objects.requireNonNullElse(netMap, new HashMap<>());
  }

  /**
   * Creates a new UserConfig with the given pk path.
   *
   * @param pk The path to the .pk file.
   * @return UserConfig with updated pk field.
   */
  public UserConfig withPk(Path pk) {
    return new UserConfig(pk, this.defaultNet, this.netMap);
  }

  /**
   * Creates a new UserConfig with the given defaultNet.
   *
   * @param net The name of the new defaultNet.
   * @return UserConfig with updated defaultNet field.
   */
  public UserConfig withDefaultNet(String net) {
    return new UserConfig(this.pk, net, this.netMap);
  }

  /**
   * Creates a new UserConfig with the given HashMap from string to {@link NetInfo}.
   *
   * @param netMap Map from string to {@link NetInfo}
   * @return UserConfig with updated netMap field.
   */
  public UserConfig withNetMap(Map<String, NetInfo> netMap) {
    return new UserConfig(this.pk(), this.defaultNet, netMap);
  }

  private static final class RelativePathSerializer extends JsonSerializer<Path> {

    @Override
    public void serialize(
        Path path, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      String pathString = path.toString().replace(File.separatorChar, '/');
      jsonGenerator.writeString(pathString);
    }
  }
}
