package com.partisiablockchain.language.partisiacli.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.partisiacli.NetInfo;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;

/** Load and save the user configuration, from and to persistent storage. */
public final class UserConfigStorage {

  Path configPath;
  FileManager fileManager;
  ObjectMapper objectMapper;

  /**
   * Handles the path for the config file. If the file does not exist it will be created.
   *
   * @param fileManager A {@link FileManager} for handling IO operations.
   * @param configPath The path where the config file is/should be stored.
   */
  public UserConfigStorage(FileManager fileManager, Path configPath) {
    this.fileManager = fileManager;
    this.configPath = configPath;
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Reads the config file and returns it as a record.
   *
   * @return The config file as a record.
   */
  public UserConfig loadConfig() {
    ObjectMapper objectMapper = new ObjectMapper();
    HashMap<String, NetInfo> defaultNetMap = new HashMap<>();
    UserConfig defaultConfig = new UserConfig(Path.of(""), "testnet", defaultNetMap);
    if (!fileManager.fileExists(configPath)) {
      return defaultConfig;
    }
    String configString =
        fileManager.readString(
            configPath.toAbsolutePath(),
            "Failed to read config file from: %s".formatted(configPath));

    JsonNode configJson =
        ExceptionConverter.call(
            () -> objectMapper.readTree(configString),
            "Error while converting config file to JsonNode \n"
                + " Make sure the config file contains valid json");

    if (configJson.toString().isEmpty()) {
      return defaultConfig;
    }

    return ExceptionConverter.call(
        () -> objectMapper.treeToValue(configJson, UserConfig.class),
        "Error while creating a java record from config file");
  }

  /**
   * Save config to config file.
   *
   * @param config The config to save to the config file.
   */
  public void saveConfig(UserConfig config) {
    if (!fileManager.fileExists(config.pk())) {
      throw new IllegalArgumentException(
          "Private key file "
              + config.pk().toString().replace(File.separatorChar, '/')
              + " does not exist");
    }
    fileManager.writeString(
        objectMapper.valueToTree(config).toPrettyString(),
        configPath,
        "Error while writing to config.json located at " + configPath);
  }
}
