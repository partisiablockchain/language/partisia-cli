package com.partisiablockchain.language.partisiacli.config.listconfig;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.config.ConfigPrinter;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import picocli.CommandLine;

/** List the configurations set. */
@CommandLine.Command(name = "list", description = "List the configurations set.")
public final class ListConfig implements Command {

  /** List the configurations set. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    UserConfig userConfig =
        new UserConfigStorage(executionEnvironment.fileManager(), executionEnvironment.configPath())
            .loadConfig();
    ConfigPrinter configPrinter = new ConfigPrinter(userConfig, executionEnvironment.out());
    configPrinter.printConfig();
  }
}
