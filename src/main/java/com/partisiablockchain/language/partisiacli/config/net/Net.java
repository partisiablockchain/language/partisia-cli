package com.partisiablockchain.language.partisiacli.config.net;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.BlockchainUrls;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.NetInfo;
import com.partisiablockchain.language.partisiacli.RestAndWebUrls;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.RestAndWebUrlsConverter;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.PrintStream;
import java.util.Map;
import picocli.CommandLine;

/** Set the default net option to be used during runtime. */
@CommandLine.Command(name = "net", description = "Set the net option to be used during runtime.")
public final class Net implements Command {

  /** Command spec. */
  @CommandLine.Spec public CommandLine.Model.CommandSpec spec;

  /** The name to save the target defaultNet under. */
  @CommandLine.Parameters(
      description = "The name to save the target net under",
      paramLabel = "<name>",
      defaultValue = "")
  public String name;

  /** The net endpoint. */
  @CommandLine.Parameters(
      description =
          "The blockchain net to save.%n"
              + "<reader-url>   Target a custom net (with no browser)%n"
              + "<reader-url>,<browser-url>"
              + "   Target a custom net with a custom browser.",
      paramLabel = "<net-endpoint>",
      defaultValue = "")
  public String targetNet;

  /** Description of the target net. */
  @CommandLine.Option(names = "--description", description = "Add a description of the target net")
  public String description;

  /** Set as default net. */
  @CommandLine.Option(names = "--default", description = "Set as default net")
  public boolean setDefault;

  /** Unsets the default net configuration. */
  @CommandLine.Option(names = "--delete", description = "delete the net configuration")
  public boolean delete;

  /** Prints the saved nets. */
  @CommandLine.Option(names = {"--list", "-l"})
  public boolean list;

  /** Set the default net option to be used during runtime. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    UserConfigStorage userConfigStorage =
        new UserConfigStorage(
            executionEnvironment.fileManager(), executionEnvironment.configPath());
    UserConfig userConfig = userConfigStorage.loadConfig();
    PrintStream out = executionEnvironment.out();

    if (list) {
      NetPrinter netPrinter = new NetPrinter(userConfigStorage, out);
      netPrinter.printNets();
    } else {
      if (name.isEmpty()) {
        throw new CommandLine.ParameterException(
            spec.commandLine(), "Missing required parameter <name>");
      }
      if (!targetNet.isEmpty()) {
        RestAndWebUrls restAndWebUrls =
            new RestAndWebUrlsConverter().convertRestAndWebUrls(targetNet, spec.commandLine());
        Map<String, NetInfo> newNetMap = userConfig.netMap();
        String restBaseUrl = restAndWebUrls.restBaseUrl();

        // Detect number of shards using the rest base url.
        ApiClient apiClient = executionEnvironment.clientProvider().apiClient(restBaseUrl);
        ChainControllerApi chainControllerApi = new ChainControllerApi(apiClient);
        int noShards =
            ExceptionConverter.call(
                () -> chainControllerApi.getChain(null).getShards().size(),
                "Could not reach the blockchain on the given blockchain node endpoint: "
                    + restAndWebUrls.restBaseUrl());

        newNetMap.put(
            this.name,
            new NetInfo(restBaseUrl, restAndWebUrls.webBaseUrl(), noShards, description));
        userConfig = userConfig.withNetMap(newNetMap);
      }
      if (setDefault) {
        boolean isValidNetName =
            userConfig.netMap().containsKey(name)
                || name.equals(BlockchainUrls.TESTNET)
                || name.equals(BlockchainUrls.MAINNET);
        if (!isValidNetName) {
          throw new RuntimeException(
              "No net endpoint with name \""
                  + name
                  + "\" exists in the current config, you can add it and set as default by using"
                  + " \"cargo pbc config net <name> <net-endpoint> --default\". \n"
                  + " To view currently set net configurations, use \"cargo pbc config net"
                  + " --list\"");
        }
        userConfig = userConfig.withDefaultNet(name);
      }
      if (delete) {
        Map<String, NetInfo> newNetMap = userConfig.netMap();
        newNetMap.remove(name);
        userConfig = userConfig.withNetMap(newNetMap);
        if (userConfig.defaultNet().equals(name)) {
          userConfig = userConfig.withDefaultNet(BlockchainUrls.TESTNET);
        }
      }
      userConfigStorage.saveConfig(userConfig);
    }
  }
}
