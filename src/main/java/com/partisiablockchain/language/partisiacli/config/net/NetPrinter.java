package com.partisiablockchain.language.partisiacli.config.net;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.language.partisiacli.BlockchainUrls;
import com.partisiablockchain.language.partisiacli.NetInfo;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/** Prints the currently saved nets from the config file. */
public final class NetPrinter {
  private static final NetInfo DEFAULT_TESTNET =
      new NetInfo(
          BlockchainUrls.TESTNET_REST_URL,
          BlockchainUrls.TESTNET_WEB_URL,
          3,
          "The Partisia Blockchain Testnet");
  private static final NetInfo DEFAULT_MAINNET =
      new NetInfo(
          BlockchainUrls.MAINNET_REST_URL,
          BlockchainUrls.MAINNET_WEB_URL,
          3,
          "The Partisia Blockchain Mainnet");

  private final PrintStream out;
  private final UserConfigStorage userConfiguration;

  /**
   * Prints the currently saved nets from the config file.
   *
   * @param userConfiguration The current configuration.
   * @param out Stream to output the configuration on.
   */
  NetPrinter(UserConfigStorage userConfiguration, PrintStream out) {
    this.out = out;
    this.userConfiguration = userConfiguration;
  }

  /** Print the net configurations. */
  void printNets() {
    UserConfig config = userConfiguration.loadConfig();
    Map<String, NetInfo> netInfos = new HashMap<>();
    netInfos.put(BlockchainUrls.TESTNET, DEFAULT_TESTNET);
    netInfos.put(BlockchainUrls.MAINNET, DEFAULT_MAINNET);
    netInfos.putAll(config.netMap());
    out.println(
        "Available net configurations: "
            + ExceptionConverter.call(
                () ->
                    new ObjectMapper()
                        .writerWithDefaultPrettyPrinter()
                        .writeValueAsString(netInfos)));
  }
}
