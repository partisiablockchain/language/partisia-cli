package com.partisiablockchain.language.partisiacli.config.privatekey;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import java.nio.file.Path;
import picocli.CommandLine;

/** Set the default private key to be used. */
@CommandLine.Command(name = "privatekey", description = "Set the default private key to be used.")
public final class PrivateKey implements Command {

  /** Path to the private key file. */
  @CommandLine.Parameters(
      description = "Path to the \".pk\"-file containing the private key to use for signing.",
      paramLabel = "<Path-to-private-key-file>",
      index = "0",
      defaultValue = "")
  public Path path;

  /** Remove the current config value. */
  @CommandLine.Option(names = "--unset", description = "remove the configuration")
  public boolean unset;

  /** Set the default private key to be used. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    UserConfigStorage userConfigStorage =
        new UserConfigStorage(
            executionEnvironment.fileManager(), executionEnvironment.configPath());
    UserConfig userConfig = userConfigStorage.loadConfig();

    Path pkPath = path;
    userConfig = userConfig.withPk(pkPath);

    if (unset) {
      userConfig = userConfig.withPk(Path.of(""));
    }

    userConfigStorage.saveConfig(userConfig);
  }
}
