package com.partisiablockchain.language.partisiacli.contract;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.contract.checkstandard.CheckStandard;
import com.partisiablockchain.language.partisiacli.contract.refuelgas.RefuelGas;
import com.partisiablockchain.language.partisiacli.contract.secret.Secret;
import com.partisiablockchain.language.partisiacli.contract.show.Show;
import picocli.CommandLine;

/** The contract command is used to interact with contracts on the chain. */
@CommandLine.Command(
    name = "contract",
    description = "Interacts with contracts.",
    synopsisSubcommandLabel = "COMMAND",
    subcommands = {Show.class, CheckStandard.class, Secret.class, RefuelGas.class})
public final class Contract {}
