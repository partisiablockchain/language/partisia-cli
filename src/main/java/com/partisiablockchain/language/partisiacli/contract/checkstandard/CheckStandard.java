package com.partisiablockchain.language.partisiacli.contract.checkstandard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.contractstandard.ContractStandard;
import com.partisiablockchain.language.contractstandard.ContractStandards;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import picocli.CommandLine;

/** Check if a contract is a valid contract according to a format. */
@CommandLine.Command(
    name = "check-standard",
    description = "Check if a contract is a valid contract.")
public final class CheckStandard implements Command {
  /** The path to the abi-file. */
  @CommandLine.Option(names = "--path", description = "Abi-path to deployed contract.")
  Path path;

  /** The blockchain address to the deployed contract. */
  @CommandLine.Option(
      names = "--address",
      description = "Blockchain address for deployed contract.",
      converter = AddressConverter.class)
  BlockchainAddress address;

  private static final List<NameAndStandard> STANDARDS =
      List.of(
          new NameAndStandard("MPC-20", ContractStandards.MPC20),
          new NameAndStandard("MPC-20 V2", ContractStandards.MPC20_V2),
          new NameAndStandard("MPC-721", ContractStandards.MPC721),
          new NameAndStandard("MPC-721 URI extension", ContractStandards.MPC721_URI),
          new NameAndStandard("MPC-721 V2", ContractStandards.MPC721_V2),
          new NameAndStandard("MPC-721 V2 URI extension", ContractStandards.MPC721_V2_URI));

  /** Check if a contract is a valid contract according to a format. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();
    PrintStream out = executionEnvironment.out();
    FileAbi abi;
    if (address != null) {
      AddressValidator.requireAddressType(address, AddressValidator.AddressTypeSubsets.CONTRACT);

      ContractInfo contract =
          new ContractInfo(address, blockchainNet.getBlockchainClient(), path, fileManager);

      abi = contract.getAbi();
    } else if (path != null) {
      AbiParser abiParser =
          new AbiParser(fileManager.readBytes(path, String.format("No abi found at: %s.", path)));
      try {
        abi = abiParser.parseAbi();
      } catch (Exception e) {
        throw new RuntimeException(
            "Unable to parse abi. Received error '%s'".formatted(e.getMessage()));
      }
    } else {
      throw new RuntimeException(
          "Not a valid command. Provide either a contract address or abi file.");
    }
    List<String> result = checkStandards(abi);

    if (result.isEmpty()) {
      out.println("The contract does not implement any standard");
    } else {
      out.printf(
          "The contract implements the following standard%s:%n", result.size() > 1 ? "s" : "");
    }
    for (String standard : result) {
      out.println(standard);
    }
  }

  private static List<String> checkStandards(FileAbi abi) {
    List<String> result = new ArrayList<>();
    for (NameAndStandard nameAndStandard : STANDARDS) {
      if (nameAndStandard.standard().check(abi.contract())) {
        result.add(nameAndStandard.name());
      }
    }
    return result;
  }

  private record NameAndStandard(String name, ContractStandard<?> standard) {}
}
