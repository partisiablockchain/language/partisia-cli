package com.partisiablockchain.language.partisiacli.contract.refuelgas;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.transaction.TransactionSender;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import picocli.CommandLine;

/** Builds, signs and sends a transaction payload that adds more gas to a contract. */
@CommandLine.Command(
    name = "refuelgas",
    description = "Build, sign and send a transaction that adds more gas to a contract.")
public final class RefuelGas extends Build {

  /** The address of the contract to add gas to. */
  @CommandLine.Parameters(
      paramLabel = "<contract-address>",
      description = "The address of the contract to add gas to.",
      converter = AddressConverter.class)
  public BlockchainAddress contractAddress;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    AddressValidator.requireAddressType(
        contractAddress, AddressValidator.AddressTypeSubsets.CONTRACT);
    TransactionCommand.executeTransaction(
        blockchainNet,
        executionEnvironment,
        this,
        Transaction.create(contractAddress, new byte[] {}),
        TransactionSender::reportTransaction);
  }
}
