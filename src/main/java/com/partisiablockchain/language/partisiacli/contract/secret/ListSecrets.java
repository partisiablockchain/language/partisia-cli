package com.partisiablockchain.language.partisiacli.contract.secret;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import picocli.CommandLine;

/** List the owned secret associated to a given account. */
@CommandLine.Command(
    name = "list",
    description = "List all secrets for a given contract.",
    abbreviateSynopsis = true)
public final class ListSecrets implements Command {

  /** The parent command. */
  @CommandLine.ParentCommand Secret secretCommand;

  /** List only secrets owned by the given private key. */
  @CommandLine.Option(
      names = {"--owned"},
      description = "List only secrets owned by the given private key.")
  public boolean owned;

  /** The address of the contract to get the list of owned secret from. */
  @CommandLine.Parameters(
      paramLabel = "<contract-address>",
      description = "The address of the Zk contract the secrets are associated to.",
      index = "0",
      converter = AddressConverter.class)
  BlockchainAddress contractAddress;

  /** List the owned secret associated to a given account. */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();
    PrintStream out = executionEnvironment.out();
    ContractInfo contractInfo =
        new ContractInfo(contractAddress, blockchainNet.getBlockchainClient(), null, fileManager);
    SecretInfo secretInfo = new SecretInfo(contractInfo);
    UserConfig userConfig =
        new UserConfigStorage(executionEnvironment.fileManager(), executionEnvironment.configPath())
            .loadConfig();
    contractInfo.getStateJson();

    List<SecretInfo.SecretVariable> allSecrets = secretInfo.getListOfSecrets();
    String secretsAsJson;
    if (this.owned) {
      List<SecretInfo.SecretVariable> ownedSecrets =
          getOwnedSecrets(allSecrets, fileManager, userConfig);
      secretsAsJson = getSecretsAsJson(ownedSecrets);
    } else {
      secretsAsJson = getSecretsAsJson(allSecrets);
    }

    out.print(secretsAsJson);
  }

  private List<SecretInfo.SecretVariable> getOwnedSecrets(
      List<SecretInfo.SecretVariable> listOfSecrets,
      FileManager fileManager,
      UserConfig userConfig) {
    KeyManagement keyManagement =
        new KeyManagement(secretCommand.privateKey, secretCommand.wallet, fileManager, userConfig);
    return listOfSecrets.stream()
        .filter(
            secretVariable ->
                secretVariable.owner().equals(keyManagement.getAuthentication().getAddress()))
        .toList();
  }

  private String getSecretsAsJson(List<SecretInfo.SecretVariable> ownedVariables) {
    ObjectMapper mapper =
        JsonMapper.builder()
            .addModule(
                new SimpleModule()
                    .addSerializer(SecretInfo.SecretVariable.class, new SecretVariableSerializer()))
            .build();

    ObjectWriter objectWriter =
        mapper
            .writerFor(new TypeReference<List<SecretInfo.SecretVariable>>() {})
            .withDefaultPrettyPrinter();

    return ExceptionConverter.call(() -> objectWriter.writeValueAsString(ownedVariables));
  }

  private static final class SecretVariableSerializer
      extends JsonSerializer<SecretInfo.SecretVariable> {

    @Override
    public void serialize(
        SecretInfo.SecretVariable secretVariable,
        JsonGenerator json,
        SerializerProvider serializerProvider)
        throws IOException {
      json.writeStartObject();
      json.writeNumberField("id", secretVariable.id());
      json.writeStringField("owner", secretVariable.owner().writeAsString());
      json.writeBooleanField("sealed", secretVariable.sealed());
      if (secretVariable.transaction() != null) {
        json.writeStringField("transaction", secretVariable.transaction().toString());
      }
      json.writeObjectFieldStart("information");
      json.writeStringField("data", secretVariable.information().data());
      json.writeEndObject();
      json.writeEndObject();
    }
  }
}
