package com.partisiablockchain.language.partisiacli.contract.secret;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.nio.file.Path;
import picocli.CommandLine;

/** Interact with secrets pertaining to a Zk contract. */
@CommandLine.Command(
    name = "secret",
    description = "Interact with secrets from a Zk contract.",
    synopsisSubcommandLabel = "COMMAND",
    abbreviateSynopsis = true,
    subcommands = {ListSecrets.class, Show.class})
public final class Secret {

  /** File containing wallet to be used for filtering secrets. */
  @CommandLine.Option(
      names = {"--wallet"},
      description = "File containing the wallet to use for accessing secrets.",
      paramLabel = "<file>",
      scope = CommandLine.ScopeType.INHERIT)
  Path wallet;

  /** File containing private key to be used for filtering secrets. */
  @CommandLine.Option(
      names = {"--privatekey", "--pk"},
      description = "File containing private key to use for accessing secrets.",
      paramLabel = "<file>",
      scope = CommandLine.ScopeType.INHERIT)
  Path privateKey;
}
