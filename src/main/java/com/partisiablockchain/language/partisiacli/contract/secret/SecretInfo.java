package com.partisiablockchain.language.partisiacli.contract.secret;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import java.util.List;

final class SecretInfo {

  private final ContractInfo contractInfo;

  SecretInfo(ContractInfo contractInfo) {
    this.contractInfo = contractInfo;
  }

  /**
   * Get the list of secret variables from the contract.
   *
   * @return the list of secret variables.
   */
  List<SecretVariable> getListOfSecrets() {
    JsonNode variables = contractInfo.getStateJson().serializedContract().get("variables");
    List<JsonNode> values = variables.findValues("value");
    List<SecretVariable> list =
        values.stream()
            .map(
                variable ->
                    new SecretVariable(
                        variable.get("id").asInt(),
                        BlockchainAddress.fromString(variable.get("owner").asText()),
                        new Information(variable.get("information").get("data").asText()),
                        variable.get("sealed").asBoolean(),
                        variable.has("transaction")
                            ? Hash.fromString(variable.get("transaction").asText())
                            : null))
            .toList();
    return list;
  }

  @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
  @JsonSerialize
  record SecretVariable(
      int id,
      @JsonSerialize BlockchainAddress owner,
      Information information,
      boolean sealed,
      Hash transaction) {}

  @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
  @JsonSerialize
  record Information(String data) {}
}
