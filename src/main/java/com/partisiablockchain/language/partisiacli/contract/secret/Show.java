package com.partisiablockchain.language.partisiacli.contract.secret;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.language.abiclient.value.JsonValueConverter;
import com.partisiablockchain.language.abiclient.value.ScValue;
import com.partisiablockchain.language.abiclient.zk.ZkSecretReader;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.NamedTypeRef;
import com.partisiablockchain.language.abimodel.model.NamedTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.zkclient.RealZkClient;
import com.secata.stream.BitInput;
import com.secata.stream.CompactBitArray;
import java.io.PrintStream;
import picocli.CommandLine;

/** Get the secret value of an owned secret from the ZK nodes. */
@CommandLine.Command(
    name = "show",
    description = "Get the value of an owned secret from Zk nodes.",
    abbreviateSynopsis = true)
public final class Show implements Command {

  /** The parent command. */
  @CommandLine.ParentCommand Secret secretCommand;

  /** The address of the contract to get the list of owned secret from. */
  @CommandLine.Parameters(
      paramLabel = "<contract-address>",
      description = "The address of the Zk contract the secret is associated to.",
      converter = AddressConverter.class)
  BlockchainAddress contractAddress;

  /** The id of the secret to show. */
  @CommandLine.Parameters(paramLabel = "<secret-id>", description = "The id of the secret to show.")
  int secretId;

  /** The name of the public type to use for deserialization of the secret. */
  @CommandLine.Parameters(
      paramLabel = "<secret-type>",
      description = "The type to use for showing the secret, e.g. i32.")
  String secretType;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();
    PrintStream out = executionEnvironment.out();
    RealZkClient zkClient = blockchainNet.getZkClient(contractAddress);
    UserConfig userConfig =
        new UserConfigStorage(executionEnvironment.fileManager(), executionEnvironment.configPath())
            .loadConfig();
    KeyManagement keyManager =
        new KeyManagement(secretCommand.privateKey, secretCommand.wallet, fileManager, userConfig);

    CompactBitArray secretVariable =
        zkClient.fetchSecretVariable(keyManager.getAuthentication(), secretId);

    BlockchainClient blockchainClient = blockchainNet.getBlockchainClient();
    ContractInfo contractInfo =
        new ContractInfo(contractAddress, blockchainClient, null, fileManager);

    FileAbi abi = contractInfo.getAbi();
    TypeSpec type = getTypeFromString(secretType, abi.contract());

    ScValue secret = readSecret(abi.contract(), type, secretVariable.data());

    JsonNode secretJson = JsonValueConverter.toJson(secret);
    out.printf("Secret: %s \n", secretJson.toPrettyString());
  }

  private TypeSpec getTypeFromString(String typeString, ContractAbi abi) {
    return switch (typeString.toLowerCase()) {
      case "bool" -> new SimpleTypeSpec(TypeSpec.TypeIndex.bool);
      case "i8" -> new SimpleTypeSpec(TypeSpec.TypeIndex.i8);
      case "i16" -> new SimpleTypeSpec(TypeSpec.TypeIndex.i16);
      case "i32" -> new SimpleTypeSpec(TypeSpec.TypeIndex.i32);
      case "i64" -> new SimpleTypeSpec(TypeSpec.TypeIndex.i64);
      case "i128" -> new SimpleTypeSpec(TypeSpec.TypeIndex.i128);
      default -> getNamedType(typeString, abi);
    };
  }

  private TypeSpec getNamedType(String typeName, ContractAbi abi) {
    NamedTypeSpec namedType = abi.getNamedType(typeName);
    int index = abi.namedTypes().indexOf(namedType);
    if (index == -1) {
      throw new RuntimeException(String.format("Unknown type: %s", typeName));
    }
    return new NamedTypeRef(index);
  }

  private ScValue readSecret(ContractAbi contractAbi, TypeSpec type, byte[] secretData) {
    ZkSecretReader secretReader = new ZkSecretReader(BitInput.create(secretData), contractAbi);
    try {
      return secretReader.readSecret(type);
    } catch (Exception e) {
      String typeString;
      if (type instanceof NamedTypeRef namedTypeRef) {
        typeString = contractAbi.getNamedType(namedTypeRef).name();

      } else {
        typeString = TypeSpec.TypeIndex.values()[type.typeIndex().getValue()].toString();
      }
      throw new RuntimeException(String.format("Failed to read secret data into %s", typeString));
    }
  }
}
