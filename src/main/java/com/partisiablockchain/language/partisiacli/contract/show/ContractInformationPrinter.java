package com.partisiablockchain.language.partisiacli.contract.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.JsonValueConverter;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.FnAbi;
import com.partisiablockchain.language.abimodel.parser.RustSyntaxPrettyPrinter;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;
import org.bouncycastle.util.encoders.Base64;

/**
 * A contract information printer prints information about a contract, such as current state, gas
 * balance and available actions. The information printed depends on the configuration of the
 * provided {@link Show} record.
 */
final class ContractInformationPrinter {

  /** The format of a compressed JSON node. */
  private static final String COMPRESSED_FIELD = "(...)";

  /** The show record containing the configuration of the show command. */
  private final Show show;

  /** The contract's current state. */
  private final ContractInfo contract;

  private final PrintStream out;

  /**
   * Prints information about a contract, such as current state, gas balance and available actions.
   * The information printed depends on the configuration of the provided {@link Show} record.
   *
   * @param show The show record specifying which categories to print.
   * @param client A client to communicate with the blockchain.
   * @param fileManager A {@link FileManager} for handling file IO operations.
   * @param out A stream to output information on.
   */
  ContractInformationPrinter(
      Show show, BlockchainClient client, FileManager fileManager, PrintStream out) {
    this.show = show;
    this.out = out;
    this.contract =
        new ContractInfo(show.contractAddress, client, show.abiOption.filePath, fileManager);
  }

  /**
   * Prints information about a contract as specified by the given {@link Show} record as JSON. The
   * default is to print all available information of the contract.
   */
  void printInfo() {
    final boolean requestedInformationSubset =
        show.balance || show.binder || show.state || show.action;
    final boolean showAll = show.all || !requestedInformationSubset;

    ObjectNode jsonInfo = new ObjectNode(new JsonNodeFactory(false));
    if (show.balance || showAll) {
      addContractBalance(jsonInfo);
    }
    if (show.binder || showAll) {
      addContractBinder(jsonInfo);
    }
    if (show.state || showAll) {
      if (show.avl || showAll) {
        addContractStateAvl(jsonInfo);
      } else {
        addContractState(jsonInfo);
      }
    }
    if ((show.zk || showAll)
        && show.contractAddress.getType() == BlockchainAddress.Type.CONTRACT_ZK) {
      addZkState(jsonInfo);
    }
    String actions = "";
    if (show.action) {
      actions = printContractActions();
    }

    String info =
        jsonInfo.isEmpty() ? actions : "%s%n%s".formatted(jsonInfo.toPrettyString(), actions);
    out.println(info);
  }

  /**
   * Appends the balance related information of the contract, such as coins and gas stored, to the
   * provided JSON node.
   */
  private void addContractBalance(ObjectNode jsonInfo) {
    JsonNode jsonNode = contract.getLocalAccountState();
    jsonInfo.putIfAbsent("balances", jsonNode);
  }

  /**
   * Appends the binder related information of the contract, excluding state information and abis,
   * to the provided JSON node.
   */
  private void addContractBinder(ObjectNode jsonInfo) {
    ContractState contractState = contract.getStateBinary();
    ObjectNode binderStateJson =
        (ObjectNode) new ObjectMapper().convertValue(contractState, JsonNode.class);
    binderStateJson.put("serializedContract", COMPRESSED_FIELD);

    if (contractState.abi().length != 0) {
      binderStateJson.put("abi", COMPRESSED_FIELD);
    }
    jsonInfo.putIfAbsent("binderState", binderStateJson);
  }

  /** Appends the deserialized state of the contract to the provided JSON node. */
  private void addContractState(ObjectNode jsonInfo) {
    ContractState contractState = getContractState(show.contractAddress.getType());
    FileAbi fileAbi = contract.getAbi();

    JsonNode contractStateJson;
    if (fileAbi != null) {
      JsonNode serializedContractNode = contractState.serializedContract();
      String stateData;
      if (contractState.type() == ContractType.ZERO_KNOWLEDGE) {
        JsonNode base64State = serializedContractNode.get("openState").get("openState").get("data");
        stateData = base64State.asText();
      } else {
        stateData = serializedContractNode.asText();
      }
      StateReader stateReader = new StateReader(Base64.decode(stateData), fileAbi.contract());
      ScValueStruct state = stateReader.readState();
      contractStateJson = JsonValueConverter.toJson(state);
    } else {
      contractStateJson = getDeserializedStateFromReader();
    }
    jsonInfo.putIfAbsent("contractState", contractStateJson);
  }

  /** Appends the deserialized state and avl trees of the contract to the provided JSON node. */
  private void addContractStateAvl(ObjectNode jsonInfo) {
    ContractState contractState = contract.getStateAvlBinary();
    FileAbi fileAbi = contract.getAbi();

    JsonNode contractStateJson;
    if (fileAbi != null) {
      JsonNode serializedContractNode = contractState.serializedContract();
      String stateData = serializedContractNode.get("state").asText();
      byte[] stateAvlData = null;
      if (serializedContractNode.has("avlTrees")) {
        JsonNode jsonNodeAvl = contractState.serializedContract();
        String serializedAvlState = jsonNodeAvl.get("avlTrees").asText();
        stateAvlData = Base64.decode(serializedAvlState);
      }
      StateReader stateReader =
          new StateReader(Base64.decode(stateData), fileAbi.contract(), stateAvlData);
      ScValueStruct state = stateReader.readState();
      contractStateJson = JsonValueConverter.toJson(state);
    } else {
      contractStateJson = getDeserializedStateFromReader();
    }
    jsonInfo.putIfAbsent("contractState", contractStateJson);
  }

  /**
   * Appends the state information of the contract that involves ZK computation to the provided JSON
   * node.
   */
  private void addZkState(ObjectNode jsonInfo) {
    jsonInfo.putIfAbsent("zkState", getDeserializedStateFromReader());
  }

  /** Prints the actions of the contract. The init action is omitted. */
  private String printContractActions() {
    FileAbi abi = contract.getAbi();
    if (abi != null) {
      RustSyntaxPrettyPrinter rustSyntaxPrettyPrinter = new RustSyntaxPrettyPrinter(abi);
      List<FnAbi> actions = ContractInfo.getCallableActions(abi.contract());
      String listedActions =
          actions.stream()
              .map(rustSyntaxPrettyPrinter::printFunction)
              .collect(Collectors.joining(""));
      return "Actions:%n%s".formatted(listedActions);
    } else {
      return "Actions:\nCannot show actions without an abi.";
    }
  }

  /**
   * Retrieves the deserialized state of the contract as provided by the reader node of the chain.
   *
   * @return The deserialized state of the contract as JSON.
   */
  private JsonNode getDeserializedStateFromReader() {
    ContractState contractState = contract.getStateJson();
    return contractState.serializedContract();
  }

  /**
   * Gets the contract's state, the format of which depends on the contract type. Zero-knowledge
   * contracts returns the state as JSON. Any other type returns the state as binary.
   *
   * @param contractType The type of the contract.
   * @return The state of the contract formatted according to the contract's type.
   */
  private ContractState getContractState(BlockchainAddress.Type contractType) {
    return contractType == BlockchainAddress.Type.CONTRACT_ZK
        ? contract.getStateJson()
        : contract.getStateBinary();
  }
}
