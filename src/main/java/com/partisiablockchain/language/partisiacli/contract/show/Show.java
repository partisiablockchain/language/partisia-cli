package com.partisiablockchain.language.partisiacli.contract.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.partisiacli.transaction.AbiOption;
import picocli.CommandLine;

/** Shows information about a contract (as JSON). */
@CommandLine.Command(name = "show", description = "Show information about a contract (as JSON).")
public final class Show implements Command {

  /** The address of the contract to show information about. */
  @CommandLine.Parameters(
      paramLabel = "<address>",
      description = "The address of the contract to show information about.",
      index = "0",
      converter = AddressConverter.class)
  BlockchainAddress contractAddress;

  /** Show all categories of information about a contract. This is the default behaviour. */
  @CommandLine.Option(
      names = "--all",
      description =
          "Show all information about the contract; state, zk-state, binder and balances (DEFAULT)")
  boolean all;

  /** Show information about the contract balances. */
  @CommandLine.Option(names = "--balance", description = "Show the contract balances.")
  boolean balance;

  /** Show information about the contract binder. */
  @CommandLine.Option(names = "--binder", description = "Show the contract binder.")
  boolean binder;

  /** Show information about the contract state. */
  @CommandLine.Option(names = "--state", description = "Show the contract state.")
  boolean state;

  /** Show information about the actions of the contract. */
  @CommandLine.Option(
      names = "--action",
      description = "Show the actions of the contract (Rust syntax).")
  boolean action;

  /** Show the zk-state of a contract using zero-knowledge computations. */
  @CommandLine.Option(
      names = "--zk",
      description = "Show the zk-state of a contract using zero-knowledge computations.")
  boolean zk;

  /** Show information about the contract state. */
  @CommandLine.Option(names = "--avl", description = "Show the avl tree state.")
  boolean avl;

  /** Support for loading a contract ABI from a file. */
  @CommandLine.Mixin AbiOption abiOption;

  /** Shows information about a contract (as JSON). */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    AddressValidator.requireAddressType(
        contractAddress, AddressValidator.AddressTypeSubsets.CONTRACT);

    ContractInformationPrinter contractInformationPrinter =
        new ContractInformationPrinter(
            this,
            blockchainNet.getBlockchainClient(),
            executionEnvironment.fileManager(),
            executionEnvironment.out());
    contractInformationPrinter.printInfo();
  }
}
