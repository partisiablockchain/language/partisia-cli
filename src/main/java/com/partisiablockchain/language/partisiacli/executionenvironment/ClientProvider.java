package com.partisiablockchain.language.partisiacli.executionenvironment;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.language.zkclient.RealZkClient;

/**
 * Provides clients used to interact with the Blockchain. This interface is used to support testing
 * interactions with the blockchain.
 */
public interface ClientProvider {

  /**
   * Returns a {@link ChainControllerApi} targeting the blockchain by the provided REST-endpoint.
   *
   * @param restBase the base of the REST-endpoint of the blockchain
   * @return a client for general blockchain interactions
   */
  ApiClient apiClient(String restBase);

  /**
   * Get the http webclient used during runtime.
   *
   * @return the http webclient.
   */
  WebClient webClient();

  /**
   * Get a Zk client to interact with the nodes and the deployed Zk contract.
   *
   * @param contractAddress the address of the Zk contract.
   * @param blockchainClient the blockchain client, to get info about the contract.
   * @return the Zk client for the specific Zk contract.
   */
  RealZkClient getZkClient(BlockchainAddress contractAddress, BlockchainClient blockchainClient);

  /**
   * Returns a {@link BlockchainClient} targeting the blockchain defined by the provided
   * REST-endpoint and number of shards.
   *
   * @param restBase The base of the REST-endpoint of the blockchain.
   * @param numberOfShards The number of shards on the blockchain.
   * @return A client for general blockchain interactions.
   */
  BlockchainClient blockchainClient(String restBase, int numberOfShards);

  /**
   * Returns a {@link BlockchainTransactionClient} targeting the blockchain defined by the provided
   * REST-endpoint and number of shards, using the given sender.
   *
   * @param restBase The base of the REST-endpoint of the blockchain.
   * @param numberOfShards The number of shards on the blockchain.
   * @param sender The authentication of sender, used to sign transactions.
   * @param validityDuration The amount of time in milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @return A client specialized in sending transactions to the blockchain.
   */
  BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender, long validityDuration);

  /**
   * Returns a {@link BlockchainTransactionClient} targeting the blockchain defined by the provided
   * REST-endpoint and number of shards, using the given sender.
   *
   * @param restBase The base of the REST-endpoint of the blockchain.
   * @param numberOfShards The number of shards on the blockchain.
   * @param sender The authentication of sender, used to sign transactions.
   * @return A client specialized in sending transactions to the blockchain.
   */
  BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender);
}
