package com.partisiablockchain.language.partisiacli.executionenvironment;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.language.zkclient.RealZkClient;
import com.secata.tools.rest.RestResources;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import java.util.concurrent.TimeUnit;
import org.glassfish.jersey.server.ResourceConfig;

/** Provide clients for communicating directly with the blockchain. */
public final class ClientProviderMain implements ClientProvider {

  @Override
  public ApiClient apiClient(String restBase) {
    ApiClient apiClient = new ApiClient();
    apiClient.setBasePath(restBase);
    return apiClient;
  }

  @Override
  public WebClient webClient() {
    Client client =
        ClientBuilder.newBuilder()
            .connectTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .withConfig(new ResourceConfig().register(RestResources.DEFAULT))
            .build();
    return new JerseyWebClient(client);
  }

  @Override
  public RealZkClient getZkClient(
      BlockchainAddress contractAddress, BlockchainClient blockchainClient) {
    return RealZkClient.create(contractAddress, webClient(), blockchainClient);
  }

  @Override
  public BlockchainClient blockchainClient(String restBase, int numberOfShards) {
    return BlockchainClient.create(restBase, numberOfShards);
  }

  @Override
  public BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender) {
    BlockchainClient blockchainClient = blockchainClient(restBase, numberOfShards);
    return BlockchainTransactionClient.create(blockchainClient, sender);
  }

  @Override
  public BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender, long validityDuration) {
    BlockchainClient blockchainClient = blockchainClient(restBase, numberOfShards);
    return BlockchainTransactionClient.create(blockchainClient, sender, validityDuration, 600_000L);
  }
}
