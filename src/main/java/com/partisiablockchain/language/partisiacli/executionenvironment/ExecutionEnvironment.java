package com.partisiablockchain.language.partisiacli.executionenvironment;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.Random;

/**
 * The execution environment that the commandline is executed in. All access to the underlying
 * operating system and the blockchain happens through the execution environment. This allows for
 * replacing the execution environment during testing.
 */
public record ExecutionEnvironment(
    ClientProvider clientProvider,
    PrintStream out,
    InputStream in,
    PrintStream err,
    TerminationStrategy terminationStrategy,
    FileManager fileManager,
    Random rng,
    Path configPath,
    Path walletPath) {

  /**
   * Provides the dependencies to use throughout program execution.
   *
   * @param clientProvider Provides clients to communicate with the blockchain.
   * @param out A {@link PrintStream} to output success-oriented information on.
   * @param in An {@link InputStream} used to get user input from.
   * @param err A {@link PrintStream} to output error-oriented information on.
   * @param terminationStrategy How to terminate the program with an exit code when encountering an
   *     error.
   * @param fileManager Handles file IO operations.
   * @param rng A random number generator to provide randomness.
   * @param configPath Path to the config file.
   * @param walletPath Path for the wallet.
   */
  public ExecutionEnvironment {}
}
