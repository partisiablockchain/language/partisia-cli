package com.partisiablockchain.language.partisiacli.executionenvironment;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/** Supports working with files under a working directory. */
public final class FileManager {
  private final Path workingDirectory;

  /**
   * Handles any IO operations concerning files by resolving all its inputs against the provided
   * working directory.
   *
   * @param workingDirectory The folder acting as the base of relative paths.
   */
  public FileManager(Path workingDirectory) {
    this.workingDirectory = workingDirectory;
  }

  /**
   * Writes the given bytes to the target file. The path to the file may be relative to the working
   * directory of this file manager. If the file already exists, it is overwritten. Throws a {@link
   * RuntimeException} in case of any IO-failure.
   *
   * @param bytes The bytes to write.
   * @param target The file to write the bytes to.
   * @param errorMessage The error message to output in case of IO-failure.
   */
  public void writeBytes(byte[] bytes, Path target, String errorMessage) {
    ExceptionConverter.run(
        () -> Files.write(workingDirectory.resolve(target), bytes), errorMessage);
  }

  /**
   * Converts the file specified by the given {@link Path} to bytes. The path to the file may be
   * relative to the working directory of this file manager. Throws a {@link RuntimeException} in
   * case of any IO-failure.
   *
   * @param source The path to the file to read.
   * @param errorMessage The error message to output in case of IO-failure.
   * @return The file in byte-representation.
   */
  public byte[] readBytes(Path source, String errorMessage) {
    return ExceptionConverter.call(
        () -> Files.readAllBytes(workingDirectory.resolve(source)), errorMessage);
  }

  /**
   * Converts the file specified by the given {@link Path} to a {@link String}. The path to the file
   * may be relative to the working directory of this file manager. Throws a {@link
   * RuntimeException} in case of any IO-failure.
   *
   * @param source The path to the file to read.
   * @param errorMessage The error message to output in case of IO-failure.
   * @return The file in string-representation.
   */
  public String readString(Path source, String errorMessage) {
    String string =
        ExceptionConverter.call(
            () -> Files.readString(workingDirectory.resolve(source)), errorMessage);
    return Objects.requireNonNull(string).trim();
  }

  /**
   * Writes the given string to the target file. The path to the file may be relative to the working
   * directory of this file manager. If the file already exists, it is overwritten. Throws a {@link
   * RuntimeException} in case of any IO-failure.
   *
   * @param string The string to write.
   * @param target The file to write the string to.
   * @param errorMessage The error message to output in case of IO-failure.
   */
  public void writeString(String string, Path target, String errorMessage) {
    ExceptionConverter.run(
        () -> Files.writeString(workingDirectory.resolve(target), string), errorMessage);
  }

  /**
   * Checks that a given target file exists. The path to the file may be relative to the working
   * directory of this file manager.
   *
   * @param target The file to check the existence of.
   * @return Whether the file exists.
   */
  public boolean fileExists(Path target) {
    return Files.exists(workingDirectory.resolve(target));
  }

  /**
   * Creates a backup of the file at the given target.
   *
   * @param path The path of the file to copy.
   */
  public void createBak(Path path) {
    Path fullPath = workingDirectory.resolve(path);
    ExceptionConverter.run(
        () -> Files.copy(fullPath, Path.of(fullPath.toFile().getAbsolutePath() + ".bak")),
        "Could not copy file %s".formatted(fullPath.getFileName()));
  }
}
