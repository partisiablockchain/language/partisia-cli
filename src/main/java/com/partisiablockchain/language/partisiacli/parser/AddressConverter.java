package com.partisiablockchain.language.partisiacli.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import picocli.CommandLine;

/**
 * Validates and converts CLI address-related arguments to {@link
 * com.partisiablockchain.BlockchainAddress}.
 */
public final class AddressConverter implements CommandLine.ITypeConverter<BlockchainAddress> {

  @Override
  public BlockchainAddress convert(String s) throws Exception {
    BlockchainAddress blockchainAddress = AddressValidator.addressFromString(s);
    return blockchainAddress;
  }
}
