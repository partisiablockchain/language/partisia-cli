package com.partisiablockchain.language.partisiacli.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.partisiablockchain.language.partisiacli.abi.Abi;
import com.partisiablockchain.language.partisiacli.account.Account;
import com.partisiablockchain.language.partisiacli.block.Block;
import com.partisiablockchain.language.partisiacli.config.Config;
import com.partisiablockchain.language.partisiacli.contract.Contract;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.wallet.Wallet;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

/** The main command of the command-line interface used to interact with the blockchain. */
@CommandLine.Command(
    name = "cargo pbc",
    synopsisSubcommandLabel = "COMMAND",
    subcommands = {
      TransactionCommand.class,
      Contract.class,
      Account.class,
      Block.class,
      Config.class,
      Wallet.class,
      Abi.class
    })
public final class Blockchain {

  /** Command spec. */
  @CommandLine.Spec public CommandLine.Model.CommandSpec spec;

  /** Which blockchain to target. */
  @CommandLine.Option(
      names = {"--net"},
      description =
          "The blockchain net to target. To see all named nets, run \"cargo pbc config net -l\".%n"
              + "\"mainnet\"  Target the mainnet%n"
              + "\"testnet\"  Target the testnet%n"
              + "<reader-url>  Target a custom net (with no browser)%n"
              + "<reader-url>,<browser-url>  Target a custom net with a custom browser.",
      scope = CommandLine.ScopeType.INHERIT)
  public String netname;

  /**
   * Controls the verbosity of the tool throughout execution. The ROOT-logger level is by default
   * 'OFF'.
   */
  @CommandLine.Option(
      names = {"-v", "--verbose"},
      description = "Print all available information. Default is to print minimum information.",
      defaultValue = "false",
      scope = CommandLine.ScopeType.INHERIT)
  public boolean verbose = false;

  @CommandLine.Option(
      names = {"-h", "--help"},
      description = "Print usage description of the command.",
      usageHelp = true,
      scope = CommandLine.ScopeType.INHERIT)
  boolean usageHelpRequested;

  /**
   * Set the logging level according to verbose. Enables blockchain client logging for debugging and
   * transaction execution status prints. The default is no logging.
   */
  public void setVerbose() {
    Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    if (verbose) {
      rootLogger.setLevel(Level.DEBUG);
    } else {
      rootLogger.setLevel(Level.OFF);
    }
  }
}
