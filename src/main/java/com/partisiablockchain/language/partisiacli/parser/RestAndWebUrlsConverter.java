package com.partisiablockchain.language.partisiacli.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainUrls;
import com.partisiablockchain.language.partisiacli.RestAndWebUrls;
import com.secata.tools.coverage.ExceptionConverter;
import java.net.URL;
import picocli.CommandLine;

/**
 * Validates and converts arguments to CLI into {@link RestAndWebUrls}. An argument must be either a
 * reader url or a pair of a comma-separated reader and dashboard url.
 */
public final class RestAndWebUrlsConverter {

  /**
   * Convert a target net as written on the commandline to {@link RestAndWebUrls}.
   *
   * @param value the target net to convert
   * @param commandLine the commandline where we are trying to parse the target net
   * @return the parsed {@link RestAndWebUrls}
   */
  public RestAndWebUrls convertRestAndWebUrls(String value, CommandLine commandLine) {
    try {
      return new RestAndWebUrlsConverter().convert(value);
    } catch (Exception e) {
      throw new CommandLine.ParameterException(
          commandLine,
          ("Invalid URL(s) '%s': The provided string must be either '<reader-url>' or"
                  + " '<reader-url>,<browser-url>'.")
              .formatted(value, e));
    }
  }

  RestAndWebUrls convert(String value) throws Exception {
    String targetLowerCase = value.toLowerCase();
    String[] args = targetLowerCase.split(",");

    if (args.length == 1 || args.length == 2) {
      String restBaseUrl = urlFromString(args[0]).toString();

      String webBaseUrl =
          (args.length == 2) ? urlFromString(args[1]).toString() : BlockchainUrls.NO_URL;

      return new RestAndWebUrls(restBaseUrl, webBaseUrl);

    } else {
      throw new Exception("More than 2 arguments provided");
    }
  }

  private static URL urlFromString(String url) {
    return ExceptionConverter.call(() -> new URL(url), "Argument is not an URL.");
  }
}
