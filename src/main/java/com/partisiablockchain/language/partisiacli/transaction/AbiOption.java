package com.partisiablockchain.language.partisiacli.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.nio.file.Path;
import picocli.CommandLine;

/** Support for loading a contract ABI from a file. */
public final class AbiOption {
  /** Load contract ABI from a file instead of fetching it from the blockchain. */
  @CommandLine.Option(
      names = "--abi",
      description =
          "Load contract ABI from an .abi file or a .pbc file instead of fetching it from the"
              + " blockchain.",
      paramLabel = "<abi-file>")
  public Path filePath;
}
