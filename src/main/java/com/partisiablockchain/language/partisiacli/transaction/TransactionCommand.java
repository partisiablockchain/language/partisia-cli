package com.partisiablockchain.language.partisiacli.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.partisiacli.transaction.TransactionSender.sendAndReportResult;

import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.transaction.action.Action;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import com.partisiablockchain.language.partisiacli.transaction.deploy.Deploy;
import com.partisiablockchain.language.partisiacli.transaction.latest.Latest;
import com.partisiablockchain.language.partisiacli.transaction.raw.Raw;
import com.partisiablockchain.language.partisiacli.transaction.send.Send;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.SignOptions;
import com.partisiablockchain.language.partisiacli.transaction.show.Show;
import com.partisiablockchain.language.partisiacli.transaction.sign.Sign;
import com.partisiablockchain.language.partisiacli.transaction.upgrade.Upgrade;
import com.secata.stream.SafeDataOutputStream;
import java.nio.file.Path;
import java.util.function.BiFunction;
import picocli.CommandLine;

/** The transaction command is used to build, sign and send transactions. */
@CommandLine.Command(
    name = "transaction",
    synopsisSubcommandLabel = "COMMAND",
    description = "Builds, signs and sends transactions.",
    subcommands = {
      Action.class,
      Deploy.class,
      Raw.class,
      Sign.class,
      Send.class,
      Show.class,
      Latest.class,
      Upgrade.class
    })
public final class TransactionCommand {

  /**
   * Executes a transaction by signing and sending it. If the {@link Build.StopOptions} has a build
   * output file set, the transaction will be written to that file and no further action will be
   * taken. If the {@link Build.StopOptions} has a sign output file set, the signed transaction will
   * be written to that file and no further action will be taken.
   *
   * @param blockchainNet The {@link BlockchainNet} to execute the command on.
   * @param executionEnvironment The {@link ExecutionEnvironment} to execute the transaction command
   *     under.
   * @param subcommand The transaction subcommand.
   * @param transaction The {@link com.partisiablockchain.client.transaction.Transaction} to
   *     execute.
   * @param successMessageSupplier supplier for the message printed if the transaction was a success
   */
  public static void executeTransaction(
      BlockchainNet blockchainNet,
      ExecutionEnvironment executionEnvironment,
      Build subcommand,
      com.partisiablockchain.client.transaction.Transaction transaction,
      BiFunction<BlockchainNet, Hash, String> successMessageSupplier) {

    FileManager fileManager = executionEnvironment.fileManager();
    UserConfig userConfig =
        new UserConfigStorage(fileManager, executionEnvironment.configPath()).loadConfig();
    SignOptions signOptions = subcommand.signOptions;
    Build.StopOptions stopOptions = subcommand.stopOptions;
    if (stopOptions.buildOutputFile != null) {
      Path outputFile = stopOptions.buildOutputFile;
      fileManager.writeBytes(
          SafeDataOutputStream.serialize(transaction),
          outputFile,
          "Failed to write file: %s".formatted(outputFile));
      return;
    }
    KeyManagement keyManager =
        new KeyManagement(signOptions.privateKey, signOptions.wallet, fileManager, userConfig);
    SignedTransaction signedTransaction =
        TransactionSigner.sign(
            blockchainNet,
            transaction,
            keyManager.getAuthentication(),
            signOptions.gas,
            signOptions.validityDuration);
    if (stopOptions.signOutputFile != null) {
      Path outputFile = stopOptions.signOutputFile;
      fileManager.writeBytes(
          SafeDataOutputStream.serialize(signedTransaction),
          outputFile,
          "Failed to write signed transaction to: %s".formatted(outputFile));
      return;
    }

    sendAndReportResult(
        blockchainNet,
        executionEnvironment.out(),
        signedTransaction,
        keyManager.getAuthentication(),
        subcommand.showStatusOption.status,
        successMessageSupplier);
  }
}
