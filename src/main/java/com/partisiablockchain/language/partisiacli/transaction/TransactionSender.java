package com.partisiablockchain.language.partisiacli.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.ExecutedTransactionTree;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.ShowStatusOption;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/** Used to send signed transactions. */
public final class TransactionSender {

  private TransactionSender() {}

  /**
   * Sends the provided signed transaction and outputs relevant information on the terminal based on
   * the target address of the transaction.
   *
   * @param blockchainNet The net to send the transaction on.
   * @param out A stream to output information on.
   * @param signedTransaction The signed transaction to send.
   * @param senderAuthentication the signer authentication.
   * @param status Which execution status to show for the signed transaction.
   * @param successMessageSupplier supplier for the message printed if the transaction was a success
   */
  public static void sendAndReportResult(
      BlockchainNet blockchainNet,
      PrintStream out,
      SignedTransaction signedTransaction,
      SenderAuthentication senderAuthentication,
      ShowStatusOption.Status status,
      BiFunction<BlockchainNet, Hash, String> successMessageSupplier) {
    BlockchainTransactionClient transactionClient =
        blockchainNet.getBlockchainTransactionClient(senderAuthentication);
    SentTransaction sentTransaction = transactionClient.send(signedTransaction);

    if (status != ShowStatusOption.Status.tx) {
      List<ExecutedTransaction> executedTransactions =
          handleWaitRequests(status, transactionClient, sentTransaction);
      reportTransactionResult(blockchainNet, out, executedTransactions, successMessageSupplier);
    } else {
      reportTransactionSent(blockchainNet, out, sentTransaction);
    }
  }

  private static List<ExecutedTransaction> handleWaitRequests(
      ShowStatusOption.Status status,
      BlockchainTransactionClient transactionClient,
      SentTransaction sentTransaction) {
    if (status == ShowStatusOption.Status.block) {
      ExecutedTransaction executedTransaction =
          transactionClient.waitForInclusionInBlock(sentTransaction);
      return List.of(executedTransaction);
    }
    ExecutedTransactionTree executedTransactionTree =
        transactionClient.waitForSpawnedEvents(sentTransaction);
    List<ExecutedTransaction> executedTransactions = executedTransactionTree.executedEvents();

    List<ExecutedTransaction> executedEvents =
        new ArrayList<>(List.of(executedTransactionTree.executedTransaction()));
    executedEvents.addAll(executedTransactions);
    return executedEvents;
  }

  /**
   * Prints information associated with a successful transaction to some contract type. Throws a
   * {@link RuntimeException} in case of a failed transaction.
   *
   * @param blockchainNet The net to send the transaction on.
   * @param out A stream to output information on.
   * @param executedTransactions A list of all resulting executed transactions caused by sending a
   *     transaction to a contract.
   * @param successMessageSupplier supplier for the message printed if the transaction was a success
   */
  public static void reportTransactionResult(
      BlockchainNet blockchainNet,
      PrintStream out,
      List<ExecutedTransaction> executedTransactions,
      BiFunction<BlockchainNet, Hash, String> successMessageSupplier) {
    for (ExecutedTransaction executedTransaction : executedTransactions) {
      if (!executedTransaction.executionSucceeded()) {
        throw new RuntimeException(
            "Transaction '%s' failed with cause: %s"
                .formatted(
                    executedTransaction.identifier(),
                    executedTransaction.failureCause().errorMessage()));
      }
    }

    Hash initialTransaction = Hash.fromString(executedTransactions.get(0).identifier());
    String successMessage = successMessageSupplier.apply(blockchainNet, initialTransaction);
    out.println(successMessage);
  }

  /**
   * Prints the transaction hash of a sent transaction. The transaction may not have been (fully)
   * executed when searching for it on the blockchain.
   *
   * @param blockchainNet The net to send the transaction on.
   * @param out A stream to output information on.
   * @param sentTransaction The transaction to print an identifier for.
   */
  public static void reportTransactionSent(
      BlockchainNet blockchainNet, PrintStream out, SentTransaction sentTransaction) {
    Hash txIdentifier = sentTransaction.transactionPointer().identifier();
    String sentMessage = "Transaction sent: ";
    String display =
        blockchainNet.hasWebUrl()
            ? sentMessage + blockchainNet.getEventUrl(txIdentifier)
            : sentMessage + txIdentifier;
    out.println(display);
  }

  /**
   * Get the message printed for a successful execution of a generic transaction.
   *
   * @param blockchainNet the net to send the transaction on.
   * @param txIdentifier identifier of the executed transaction
   * @return successful transaction execution message
   */
  public static String reportTransaction(BlockchainNet blockchainNet, Hash txIdentifier) {
    String transactionSuccessful = "Transaction successfully sent: ";
    return blockchainNet.hasWebUrl()
        ? transactionSuccessful + blockchainNet.getEventUrl(txIdentifier)
        : transactionSuccessful + txIdentifier;
  }
}
