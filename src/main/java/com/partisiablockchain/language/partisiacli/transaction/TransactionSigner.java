package com.partisiablockchain.language.partisiacli.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.language.partisiacli.BlockchainNet;

/** Used to sign a given transaction. */
public final class TransactionSigner {

  private TransactionSigner() {}

  /**
   * Signs the given transaction.
   *
   * @param blockchainNet The {@link BlockchainNet} to find the {@link BlockchainTransactionClient}
   *     to use for signing.
   * @param transaction The transaction to sign.
   * @param authentication the signer authentication.
   * @param gas The gas to use for signing.
   * @param validToTime The amount of time in milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @return The signed transaction.
   */
  public static SignedTransaction sign(
      BlockchainNet blockchainNet,
      com.partisiablockchain.client.transaction.Transaction transaction,
      SenderAuthentication authentication,
      long gas,
      long validToTime) {

    BlockchainTransactionClient transactionClient =
        blockchainNet.getBlockchainTransactionClient(authentication, validToTime);
    return transactionClient.sign(transaction, gas);
  }
}
