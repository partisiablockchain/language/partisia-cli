package com.partisiablockchain.language.partisiacli.transaction.action;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.FnAbi;
import com.partisiablockchain.language.abimodel.model.FnKind;
import com.partisiablockchain.language.partisiacli.ActionRpcBuilder;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.ZkActionRpcBuilder;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.partisiacli.transaction.AbiOption;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.transaction.TransactionSender;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import com.partisiablockchain.language.zkclient.RealZkClient;
import picocli.CommandLine;

/** Builds, signs and sends a transaction payload using the contract abi. */
@CommandLine.Command(
    name = "action",
    description =
        "Build, sign and send a transaction that calls a specific action with parameters. Uses the"
            + " contract ABI.")
public final class Action extends Build {

  /** Support for loading a contract ABI from a file. */
  @CommandLine.Mixin public AbiOption abiOption;

  /** The address of the contract to send a transaction to. */
  @CommandLine.Parameters(
      paramLabel = "<contract-address>",
      description = "The address of the contract to send a transaction to.",
      index = "0",
      converter = AddressConverter.class)
  public BlockchainAddress contractAddress;

  /** The name of the action to invoke. */
  @CommandLine.Parameters(
      paramLabel = "<action-name>",
      description = "The name of the action to invoke.",
      defaultValue = "",
      index = "1")
  public String actionName;

  /** The parameters of the action being invoked. */
  @CommandLine.Parameters(
      paramLabel = "<action-parameters>",
      description = "The parameters of the action being invoked.",
      index = "2",
      arity = "0..*")
  public String[] actionParameters = new String[0];

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    Transaction transaction;
    FileManager fileManager = executionEnvironment.fileManager();
    UserConfig userConfig =
        new UserConfigStorage(fileManager, executionEnvironment.configPath()).loadConfig();

    AddressValidator.requireAddressType(
        contractAddress, AddressValidator.AddressTypeSubsets.CONTRACT);

    ContractInfo contractInfo =
        new ContractInfo(
            contractAddress, blockchainNet.getBlockchainClient(), abiOption.filePath, fileManager);

    FileAbi abi = contractInfo.getAbiRequired();

    FnAbi function = contractInfo.fnAbiFromString(actionName);

    if (abi.contract().isZk() && function.kind().equals(FnKind.ZkSecretInputWithExplicitType)) {
      RealZkClient realZkClient = blockchainNet.getZkClient(contractAddress);
      ZkActionRpcBuilder.SecretByteInput inputBytes =
          new ZkActionRpcBuilder(new ActionRpcBuilder(abi, fileManager))
              .buildSecretByteInput(function, actionParameters);
      SenderAuthentication keyPair =
          new KeyManagement(signOptions.privateKey, signOptions.wallet, fileManager, userConfig)
              .getAuthentication();

      transaction =
          realZkClient.buildOnChainInputTransaction(
              keyPair.getAddress(), inputBytes.secretInput(), inputBytes.publicRpc());
    } else {
      ActionRpcBuilder actionRpcBuilder = new ActionRpcBuilder(abi, fileManager);
      byte[] rpc = actionRpcBuilder.buildRpc(function, actionParameters);
      transaction = Transaction.create(contractAddress, rpc);
    }
    TransactionCommand.executeTransaction(
        blockchainNet,
        executionEnvironment,
        this,
        transaction,
        TransactionSender::reportTransaction);
  }
}
