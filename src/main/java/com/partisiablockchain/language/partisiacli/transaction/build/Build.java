package com.partisiablockchain.language.partisiacli.transaction.build;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.ShowStatusOption;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.SignOptions;
import java.nio.file.Path;
import picocli.CommandLine;

/**
 * Specifies how to build the transaction payload. The options make up the three phases a
 * transaction has to go through to be put on the blockchain. The default is to build, sign and send
 * the transaction at once, but it is also possible to partition this sequence into individual
 * steps. E.g. a user may choose to first build a transaction and then sign and send it at a later
 * point in time.
 */
public abstract class Build implements Command {

  /** Specifies which phase of the transaction execution to optionally stop early. */
  @CommandLine.ArgGroup() public StopOptions stopOptions = new StopOptions();

  /** The information needed to sign a transaction. */
  @CommandLine.Mixin public SignOptions signOptions = new SignOptions();

  /** The status to show for the execution of a sent transaction. */
  @CommandLine.Mixin public ShowStatusOption showStatusOption = new ShowStatusOption();

  /**
   * The set of mutually exclusive options that stops the execution of a transaction early, to save
   * it to a file instead of putting on the blockchain.
   */
  public static final class StopOptions {
    /** Where to output the serialized signed transaction, so it can be sent at a later time. */
    @CommandLine.Option(
        names = "--sign",
        paramLabel = "<out-file>",
        description =
            "Stop after signing the transaction saving it to a binary file instead of sending it.")
    public Path signOutputFile;

    /** Where to output the serialized unsigned transaction, so it can be signed at a later time. */
    @CommandLine.Option(
        names = "--build",
        paramLabel = "<out-file>",
        description =
            "Stop after building the transaction saving it to a binary file instead of signing and"
                + " sending it.")
    public Path buildOutputFile;
  }
}
