package com.partisiablockchain.language.partisiacli.transaction.deploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.abiclient.rpc.FnRpcBuilder;
import com.partisiablockchain.language.abiclient.state.StateReader;
import com.partisiablockchain.language.abiclient.value.ScValueMap;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.partisiacli.ActionRpcBuilder;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.ContractInfo;
import com.partisiablockchain.language.partisiacli.FileInfo;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.sections.PbcFile;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Create deployment transactions for a contract with the binder supporting the version of the
 * contract.
 *
 * <p>Can deploy both PUB and REAL contracts by switching on the abi of the contract to be deployed.
 * Also supports different versions of the deploy-contracts, by reading the state of the
 * deploy-contract using its abi.
 */
public final class ContractDeployment {

  /** The address of the public deploy contract. */
  public static final BlockchainAddress PUB_DEPLOY_ADDRESS =
      BlockchainAddress.fromString("0197a0e238e924025bad144aa0c4913e46308f9a4d");

  /** The address of the real deploy contract. */
  public static final BlockchainAddress REAL_DEPLOY_ADDRESS =
      BlockchainAddress.fromString("018bc1ccbb672b87710327713c97d43204905082cb");

  private final byte[] contractBytes;
  private final byte[] abiBytes;
  private final String[] initArguments;
  private final FileAbi contractToBeDeployedAbi;
  private final long collateralWithDecimals;
  private final FileManager fileManager;
  private final BlockchainClient client;
  private final ActionRpcBuilder actionRpcBuilder;

  /**
   * Create the contract deployment.
   *
   * @param deploy the given parameters.
   * @param fileManager provides the needed bytecode for deployment.
   * @param client the blockchain client to interact with.
   */
  ContractDeployment(Deploy deploy, FileManager fileManager, BlockchainClient client) {
    this.fileManager = fileManager;
    this.client = client;
    FileInfo byteCodeFileInfo = new FileInfo(deploy.bytecodeFile, fileManager);

    if (byteCodeFileInfo.fileType == FileInfo.FileType.Pbc) {
      if (deploy.abiFile != null) {
        throw new RuntimeException("--abi option is not allowed when providing a .pbc file");
      }
      PbcFile pbcFile;
      byte[] pbcBytes =
          fileManager.readBytes(
              deploy.bytecodeFile,
              "Exception occurred when trying to read file %s".formatted(deploy.bytecodeFile));
      try {
        pbcFile = PbcFile.fromBytes(pbcBytes);
      } catch (RuntimeException e) {
        throw new RuntimeException(
            "Exception occurred when trying to parse %s as a .pbc file:\n"
                    .formatted(deploy.bytecodeFile.getFileName())
                + e.getMessage(),
            e);
      }

      if (pbcFile.isZk) {
        contractBytes = pbcFile.getZkwaBytes();
      } else {
        contractBytes = pbcFile.getWasmBytes();
      }

      abiBytes = pbcFile.getAbiBytes();
      contractToBeDeployedAbi = new AbiParser(abiBytes).parseAbi();

      actionRpcBuilder = new ActionRpcBuilder(contractToBeDeployedAbi, fileManager);
      initArguments = deploy.initParameters;
    } else {
      if (byteCodeFileInfo.fileType != FileInfo.FileType.Wasm
          && byteCodeFileInfo.fileType != FileInfo.FileType.Zkwa) {
        throw new RuntimeException("byte-code must be either wasm, zkwa or pbc");
      }
      contractBytes =
          fileManager.readBytes(
              deploy.bytecodeFile,
              "Failed to read byte-code from: %s".formatted(deploy.bytecodeFile));
      abiBytes =
          fileManager.readBytes(
              deploy.abiFile, "Failed to read abi from: %s".formatted(deploy.abiFile));
      contractToBeDeployedAbi = new AbiParser(abiBytes).parseAbi();
      actionRpcBuilder = new ActionRpcBuilder(contractToBeDeployedAbi, fileManager);
      initArguments = deploy.initParameters;
    }
    if (contractToBeDeployedAbi.contract().isZk()) {
      collateralWithDecimals = deploy.collateral * 10_000L;
    } else {
      collateralWithDecimals = 0L;
    }
  }

  private byte[] realDeployBytes() {
    ContractState realDeploy =
        new ContractInfo(REAL_DEPLOY_ADDRESS, client, null, fileManager).getStateBinary();

    byte[] realDeployState = getBinaryState(realDeploy);

    ContractAbi deployAbi = new AbiParser(realDeploy.abi()).parseAbi().contract();

    Map<Integer, VersionInterval> binderVersions =
        getSupportedBinderVersions(realDeployState, deployAbi);

    AbiVersion binderVersion = contractToBeDeployedAbi.versionBinder();
    int binderId = determineBinderId(binderVersions, binderVersion);

    if (binderId == -1) {
      throw new RuntimeException(
          String.format("No REAL binders found for binder version %s", binderVersion.toString()));
    }

    return realDeployRpc(deployAbi, binderId);
  }

  /**
   * Create the deployment rpc for a zk contract using the deploy-contract's abi.
   *
   * @param deployAbi the deploy-contracts abi
   * @param binderId the binder id to deploy with
   * @return the deployment rpc
   */
  private byte[] realDeployRpc(ContractAbi deployAbi, int binderId) {
    byte[] initializationBytes =
        actionRpcBuilder.buildRpc(contractToBeDeployedAbi.contract().init(), initArguments);
    FnRpcBuilder fnBuilder = new FnRpcBuilder("deployContractV3", deployAbi);
    fnBuilder.addVecU8(contractBytes);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addVecU8(abiBytes);
    fnBuilder.addI64(collateralWithDecimals);
    fnBuilder.addVec();
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  private byte[] pubDeployBytes() {
    ContractState pubDeploy =
        new ContractInfo(PUB_DEPLOY_ADDRESS, client, null, fileManager).getStateBinary();

    byte[] pubDeployState = getBinaryState(pubDeploy);

    ContractAbi deployAbi = new AbiParser(pubDeploy.abi()).parseAbi().contract();

    Map<Integer, VersionInterval> binderVersions =
        getSupportedBinderVersions(pubDeployState, deployAbi);

    AbiVersion binderVersion = contractToBeDeployedAbi.versionBinder();
    int binderId = determineBinderId(binderVersions, binderVersion);

    if (binderId == -1) {
      throw new RuntimeException(
          String.format("No public binders found for binder version %s", binderVersion.toString()));
    }

    return pubDeployRpc(deployAbi, binderId);
  }

  /**
   * Create the deployment rpc for a public contract using the deploy-contract's abi.
   *
   * @param deployAbi the deploy-contracts abi
   * @param binderId the binder id to deploy with
   * @return the deployment rpc
   */
  private byte[] pubDeployRpc(ContractAbi deployAbi, int binderId) {
    byte[] initializationBytes =
        actionRpcBuilder.buildRpc(contractToBeDeployedAbi.contract().init(), initArguments);
    FnRpcBuilder fnBuilder = new FnRpcBuilder("deployContractWithBinderId", deployAbi);
    fnBuilder.addVecU8(contractBytes);
    fnBuilder.addVecU8(abiBytes);
    fnBuilder.addVecU8(initializationBytes);
    fnBuilder.addI32(binderId);

    return fnBuilder.getBytes();
  }

  /**
   * Find the supported binder version intervals for a deploy contract.
   *
   * @param deployState state bytes of the deploy-contract
   * @param abi abi of the deploy-contract
   * @return a map from binder id to supported binder version interval
   */
  static Map<Integer, VersionInterval> getSupportedBinderVersions(
      byte[] deployState, ContractAbi abi) {
    ScValueStruct state = new StateReader(deployState, abi).readState();
    ScValueMap binders = state.getFieldValue("binders").optionValue().innerValue().mapValue();
    return binders.map().entrySet().stream()
        .collect(
            Collectors.toMap(
                entry -> entry.getKey().optionValue().innerValue().asInt(),
                entry ->
                    versionIntervalFromScBinderInfo(
                        entry.getValue().optionValue().innerValue().structValue())));
  }

  /**
   * Get the supported version interval from the binder info struct.
   *
   * @param binderInfo binder info as a ScValueStruct
   * @return the version interval as a java type
   */
  private static VersionInterval versionIntervalFromScBinderInfo(ScValueStruct binderInfo) {
    ScValueStruct versionInterval =
        binderInfo.getFieldValue("versionInterval").optionValue().innerValue().structValue();
    ScValueStruct scMin =
        versionInterval
            .getFieldValue("supportedBinderVersionMin")
            .optionValue()
            .innerValue()
            .structValue();
    ScValueStruct scMax =
        versionInterval
            .getFieldValue("supportedBinderVersionMax")
            .optionValue()
            .innerValue()
            .structValue();
    AbiVersion min =
        new AbiVersion(
            scMin.getFieldValue("major").asInt(),
            scMin.getFieldValue("minor").asInt(),
            scMin.getFieldValue("patch").asInt());
    AbiVersion max =
        new AbiVersion(
            scMax.getFieldValue("major").asInt(),
            scMax.getFieldValue("minor").asInt(),
            scMax.getFieldValue("patch").asInt());
    return new VersionInterval(min, max);
  }

  /**
   * Get the address of the deployment contract.
   *
   * @return the address of the deployment contract.
   */
  BlockchainAddress getDeployContractAddress() {
    if (contractToBeDeployedAbi.contract().isZk()) {
      return REAL_DEPLOY_ADDRESS;
    } else {
      return PUB_DEPLOY_ADDRESS;
    }
  }

  private byte[] getBinaryState(ContractState deploy) {
    byte[] deployState;

    deployState =
        ExceptionConverter.call(
            deploy.serializedContract()::binaryValue,
            "Could not read binary state of Deploy contract.");

    return deployState;
  }

  /**
   * Get the deployment rpc to deploy a public or zk contract.
   *
   * @return the rpc bytes to deploy the contract.
   */
  byte[] getDeploymentRpc() {
    if (contractToBeDeployedAbi.contract().isZk()) {
      return realDeployBytes();
    } else {
      return pubDeployBytes();
    }
  }

  String reportDeploy(BlockchainNet blockchainNet, Hash txIdentifier) {
    BlockchainAddress.Type contractType =
        contractToBeDeployedAbi.contract().isZk()
            ? BlockchainAddress.Type.CONTRACT_ZK
            : BlockchainAddress.Type.CONTRACT_PUBLIC;
    BlockchainAddress contractAddress = BlockchainAddress.fromHash(contractType, txIdentifier);
    String deploySuccessful =
        "Deployed contract successfully.\n"
            + "Contract deployed at: "
            + contractAddress.writeAsString()
            + "\n";
    return blockchainNet.hasWebUrl()
        ? deploySuccessful
            + "View it in browser here: "
            + blockchainNet.getDeployUrl(contractAddress)
        : deploySuccessful;
  }

  /**
   * Find the binder id of a binder which supports a given binder version.
   *
   * @param binders map from binder ids to supported version interval
   * @param binderVersion the binder version of the contract about to be deployed
   * @return the binder id supporting the given binder version or -1 if no binder supports the
   *     version
   */
  static int determineBinderId(Map<Integer, VersionInterval> binders, AbiVersion binderVersion) {

    return binders.entrySet().stream()
        .filter(entry -> checkDeployVersion(binderVersion, entry.getValue()))
        .map(Map.Entry::getKey)
        .findFirst()
        .orElse(-1);
  }

  private static boolean checkDeployVersion(
      AbiVersion binderVersion, VersionInterval versionInterval) {
    return checkVersion(
        binderVersion,
        versionInterval.max().major(),
        versionInterval.max().minor(),
        versionInterval.min().major(),
        versionInterval.min().minor());
  }

  private static boolean checkVersion(
      AbiVersion binderVersion, int maxMajor, int maxMinor, int minMajor, int minMinor) {
    return maxMajor >= binderVersion.major()
        && maxMinor >= binderVersion.minor()
        && minMajor <= binderVersion.major()
        && minMinor <= binderVersion.minor();
  }

  /**
   * A version interval.
   *
   * @param min min version
   * @param max max version
   */
  record VersionInterval(AbiVersion min, AbiVersion max) {}
}
