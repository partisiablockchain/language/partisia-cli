package com.partisiablockchain.language.partisiacli.transaction.deploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import java.nio.file.Path;
import picocli.CommandLine;

/**
 * Builds, signs and sends a transaction payload which deploys a new smart-contract to the
 * blockchain.
 */
@CommandLine.Command(
    name = "deploy",
    description =
        "Build, sign and send a transaction that deploys a new smart-contract to the blockchain.")
public final class Deploy extends Build {

  /** Where to read possible abi file from. */
  @CommandLine.Option(
      names = {"--abi"},
      description = "The path to the smart contract ABI file (.abi) to deploy.")
  public Path abiFile;

  /** Where to read the bytecode from. */
  @CommandLine.Parameters(
      description =
          "The path to the smart contract bytecode file (.wasm, .zkwa or .pbc) to deploy. "
              + "If it is a .wasm or .zkwa file, the contract ABI should also be provided.",
      paramLabel = "<bytecode-file>",
      index = "0")
  public Path bytecodeFile;

  /** The initialization arguments for the contract being deployed. */
  @CommandLine.Parameters(
      description = "The initialization arguments for the contract being deployed",
      paramLabel = "<init-parameters>",
      index = "1",
      arity = "0..*")
  public String[] initParameters = new String[] {};

  /**
   * The total amount of MPC tokens that the ZK nodes must stake to run ZK calculations on this
   * contract.
   */
  @CommandLine.Option(
      names = "--collateral",
      description =
          "The total amount of MPC tokens that the ZK nodes must stake to run ZK calculations on"
              + " this contract. Default is ${DEFAULT-VALUE} tokens.",
      defaultValue = "2000")
  public long collateral;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();

    ContractDeployment deployment =
        new ContractDeployment(this, fileManager, blockchainNet.getBlockchainClient());
    byte[] deployRpc = deployment.getDeploymentRpc();

    TransactionCommand.executeTransaction(
        blockchainNet,
        executionEnvironment,
        this,
        Transaction.create(deployment.getDeployContractAddress(), deployRpc),
        deployment::reportDeploy);
  }
}
