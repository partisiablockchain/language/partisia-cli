package com.partisiablockchain.language.partisiacli.transaction.latest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import picocli.CommandLine;

/** Prints the latest transactions. */
@CommandLine.Command(
    name = "latest",
    description = "Get the latest transactions from the blockchain.")
public final class Latest implements Command {

  /** The number of transactions to print. */
  @CommandLine.Parameters(
      index = "0",
      paramLabel = "[<Number>]",
      description = "The number of transactions to get",
      defaultValue = "10")
  public String number;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    LatestTransactionsPrinter latestTransactionsPrinter =
        new LatestTransactionsPrinter(
            this, blockchainNet.getBlockchainClient(), executionEnvironment.out());
    latestTransactionsPrinter.printLatestTransactions();
  }
}
