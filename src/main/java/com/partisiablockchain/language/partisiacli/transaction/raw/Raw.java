package com.partisiablockchain.language.partisiacli.transaction.raw;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.language.partisiacli.AddressValidator;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.transaction.TransactionSender;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import java.util.HexFormat;
import picocli.CommandLine;

/** Builds, signs and sends a transaction payload using specific rpc bytes. */
@CommandLine.Command(
    name = "raw",
    description = "Build, sign and send a transaction with specific rpc bytes.")
public final class Raw extends Build {

  /** The address of the contract on the blockchain as a raw string. */
  @CommandLine.Parameters(
      description = "The address of the contract on the blockchain.",
      paramLabel = "<contract-address>",
      converter = AddressConverter.class)
  public BlockchainAddress contractAddress;

  /** The transaction RPC as hex. */
  @CommandLine.Parameters(description = "The RPC for the transaction as hex.", paramLabel = "<rpc>")
  public String rpcHex;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    AddressValidator.requireAddressType(
        contractAddress, AddressValidator.AddressTypeSubsets.CONTRACT);
    TransactionCommand.executeTransaction(
        blockchainNet,
        executionEnvironment,
        this,
        Transaction.create(contractAddress, HexFormat.of().parseHex(rpcHex)),
        TransactionSender::reportTransaction);
  }
}
