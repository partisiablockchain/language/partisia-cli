package com.partisiablockchain.language.partisiacli.transaction.send;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.transaction.TransactionSender;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.ShowStatusOption;
import com.secata.stream.SafeDataInputStream;
import java.nio.file.Path;
import picocli.CommandLine;

/** Sends a prebuilt signed transaction loaded from a binary file. */
@CommandLine.Command(
    name = "send",
    description = "Send a prebuilt signed transaction loaded from a binary file.")
public final class Send implements Command {
  /** The signed transaction to send. */
  @CommandLine.Parameters(
      paramLabel = "<input-file>",
      description = "The signed transaction to send.",
      index = "0")
  public Path inputFile;

  /** The status to show for the execution of a sent transaction. */
  @CommandLine.Mixin public ShowStatusOption showStatusOption;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();
    byte[] txBytes =
        fileManager.readBytes(inputFile, "Failed to read file: %s".formatted(inputFile));
    SignedTransaction signedTransaction =
        readSignedTransaction(txBytes, blockchainNet.getBlockchainClient());
    UserConfig userConfig =
        new UserConfigStorage(executionEnvironment.fileManager(), executionEnvironment.configPath())
            .loadConfig();
    SenderAuthentication authentication;
    if (executionEnvironment.walletPath().toFile().exists()) {
      authentication =
          new KeyManagement(
                  null,
                  executionEnvironment.walletPath(),
                  executionEnvironment.fileManager(),
                  userConfig)
              .getAuthentication();
    } else {
      authentication = SenderAuthenticationKeyPair.fromString("aa");
    }
    TransactionSender.sendAndReportResult(
        blockchainNet,
        executionEnvironment.out(),
        signedTransaction,
        authentication,
        showStatusOption.status,
        TransactionSender::reportTransaction);
  }

  /**
   * Converts the given bytes to a {@link SignedTransaction}. There is no guarantee that the bytes
   * hold a valid transaction. As such the method is recommended to use on bytes stemming from a
   * previously serialized {@link SignedTransaction}-
   *
   * @param signedTransaction Bytes representing a signed transaction.
   * @param client A client used to retrieve the {@link ChainId} associated with the serialized
   *     transaction.
   * @return The parsed signed transaction.
   */
  private static SignedTransaction readSignedTransaction(
      byte[] signedTransaction, BlockchainClient client) {
    ChainId chainId = client.getChainId();
    return SignedTransaction.read(
        chainId.chainId(), SafeDataInputStream.createFromBytes(signedTransaction));
  }
}
