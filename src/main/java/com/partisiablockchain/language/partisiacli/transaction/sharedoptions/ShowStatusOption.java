package com.partisiablockchain.language.partisiacli.transaction.sharedoptions;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import picocli.CommandLine;

/**
 * Specifies the different stages of information to show about the execution of a sent transaction.
 * The default is to (wait and) show all (recursive) spawned events.
 */
public final class ShowStatusOption {
  /** The status to show for the execution of a sent transaction. */
  @CommandLine.Option(
      names = {"--show"},
      paramLabel = "<status>",
      description =
          "The status to show for the execution of a sent transaction.%n"
              + "Waits until the specified status is available.%n"
              + "\"tx\"   The sent transaction.%n"
              + "\"block\"   Inclusion in a block.%n"
              + "\"all\"   All (recursive) spawned events. (DEFAULT)")
  public Status status = Status.all;

  /**
   * The status to show for the execution of a sent transaction. The default is to wait for the
   * entirety of the transaction execution.
   */
  public enum Status {
    /** Wait for the transaction to be sent. */
    tx,

    /** Wait for the transaction to be included in a block. */
    block,

    /** Wait for the execution of the transaction and all (potentially) spawned events. */
    all
  }
}
