package com.partisiablockchain.language.partisiacli.transaction.sharedoptions;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.nio.file.Path;
import picocli.CommandLine;

/** Information needed to sign a transaction. */
public final class SignOptions {

  /** Describes the default gas to assign a signed transaction. */
  public static final String DEFAULT_GAS = "5000";

  /** Default validity time of a signed transaction. */
  public static final String DEFAULT_VALIDITY_DURATION = "180000";

  /** Describes where to read the wallet. */
  @CommandLine.Option(
      names = {"--wallet"},
      description = "File containing the wallet to use for signing.",
      paramLabel = "<file>")
  public Path wallet;

  /** Describes where to read the private key of an account. */
  @CommandLine.Option(
      names = {"--privatekey", "--pk"},
      description = "File containing the private key to use for signing.",
      paramLabel = "<file>")
  public Path privateKey;

  /**
   * Describes how long a signed transaction is valid for inclusion in a block. The default is
   * {@value DEFAULT_VALIDITY_DURATION} ms.
   */
  @CommandLine.Option(
      names = "--validityDuration",
      defaultValue = DEFAULT_VALIDITY_DURATION,
      description =
          "How long the signature should be valid (in ms). By default it is valid for"
              + " ${DEFAULT-VALUE} ms.",
      paramLabel = "<ms>")
  public long validityDuration;

  /**
   * Describes how much gas to assign a signed transaction. The default amount is {@value
   * DEFAULT_GAS}
   */
  @CommandLine.Option(
      names = "--gas",
      defaultValue = DEFAULT_GAS,
      description =
          "The amount of gas to send with the transaction. By default ${DEFAULT-VALUE} is sent.",
      paramLabel = "<amount>")
  public long gas;
}
