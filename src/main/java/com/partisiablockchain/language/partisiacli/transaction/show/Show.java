package com.partisiablockchain.language.partisiacli.transaction.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import picocli.CommandLine;

/** Shows information about a transaction and all its spawned sub-events (as JSON). */
@CommandLine.Command(
    name = "show",
    description = "Show information about a transaction and all its spawned sub-events (as JSON).")
public final class Show implements Command {

  /** The hash of the transaction to show information about. */
  @CommandLine.Parameters(
      index = "0",
      paramLabel = "<tx-identifier>",
      description = "The hash of the transaction to show information about.")
  public String txIdentifier;

  /** Only show the original transaction (no sub-events shown). */
  @CommandLine.Option(
      names = "--nospawned",
      description = "Only show the original transaction (no sub-events shown).")
  public boolean noSpawned;

  /** Do not deserialize the payload of the transactions (and spawned events). */
  @CommandLine.Option(
      names = "--dont-deserialize",
      description = "Do not deserialize the payload of the transactions (and spawned events).")
  public boolean dontDeserialize;

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    TransactionInformationPrinter transactionInformationPrinter =
        new TransactionInformationPrinter(
            this, blockchainNet.getBlockchainClient(), executionEnvironment.out());

    transactionInformationPrinter.printInfo();
  }
}
