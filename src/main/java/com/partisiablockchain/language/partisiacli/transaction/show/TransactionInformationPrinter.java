package com.partisiablockchain.language.partisiacli.transaction.show;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.FailureCause;
import com.partisiablockchain.dto.TransactionCost;
import com.partisiablockchain.language.abiclient.rpc.RpcReader;
import com.partisiablockchain.language.abiclient.rpc.RpcValueFn;
import com.partisiablockchain.language.abiclient.transaction.TransactionReader;
import com.partisiablockchain.language.abiclient.value.JsonRpcConverter;
import com.partisiablockchain.language.abiclient.value.JsonValueConverter;
import com.partisiablockchain.language.abiclient.value.ScValueEnum;
import com.partisiablockchain.language.abiclient.value.ScValueStruct;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.FnKind;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A transaction information printer prints all information about its provided transaction. The
 * information consists of blockchain-related meta-data (such as which block it came from, when it
 * was produced, whether it has been finalized, etc.), its deserialized payload and information
 * about any events it may have spawned.
 */
final class TransactionInformationPrinter {
  private final Show show;
  private final BlockchainClient client;
  private final PrintStream out;
  private final ObjectMapper objectMapper;

  /**
   * Prints information about a transaction, specified by the given {@link Show} record, and all
   * events spawned by it.
   *
   * @param show The show record specifying which transaction to print information about.
   * @param client A client to retrieve information about the transaction from the blockchain.
   * @param out A stream to output information on.
   */
  TransactionInformationPrinter(Show show, BlockchainClient client, PrintStream out) {
    this.show = show;
    this.client = client;
    this.out = out;
    this.objectMapper = new ObjectMapper();
  }

  /**
   * Prints all information about the transaction provided to this printer, as given by the Show
   * command, as JSON. This includes a collection of meta-information and the deserialized payload,
   * which is also recursively shown for all sub-events spawned by the transaction. The information
   * about sub-events may be optionally excluded.
   */
  void printInfo() {
    ExecutedTransaction transaction = locateTransaction();
    ArrayNode transactionJson;
    if (show.noSpawned) {
      transactionJson = toArray(transactionToJson(transaction));
    } else {
      transactionJson = toArray(transactionToJson(transaction));
      addExpandedSubEvents(transaction, transactionJson);
    }
    out.println(transactionJson.toPrettyString());
  }

  /**
   * Searches all shards of the targeted blockchain for the provided transaction. A {@link
   * RuntimeException} is thrown if no transaction is found.
   *
   * @return The executed transaction DTO having the provided identifier.
   */
  @SuppressWarnings("EmptyCatchBlock")
  private ExecutedTransaction locateTransaction() {
    Hash txHash = Hash.fromString(show.txIdentifier);
    List<ShardId> shards = client.getShardManager().getShards();
    ExecutedTransaction transaction = null;
    for (ShardId shard : shards) {
      try {
        transaction = client.getTransaction(shard, txHash);
        break;
      } catch (Exception ignored) {
      }
    }
    if (transaction == null) {
      throw new RuntimeException(
          "Could not find transaction with id '%s'.".formatted(show.txIdentifier));
    }
    return transaction;
  }

  /**
   * Converts the given transaction to a JSON node with its payload deserialized.
   *
   * @param transaction The transaction to convert.
   * @return The transaction as a JSON node.
   */
  private JsonNode transactionToJson(ExecutedTransaction transaction) {
    RearrangedExecutedTransaction rearrangedExecutedTransaction =
        RearrangedExecutedTransaction.create(transaction);
    ObjectNode transactionJson =
        (ObjectNode) objectMapper.convertValue(rearrangedExecutedTransaction, JsonNode.class);
    JsonNode deserializedPayloadJson = getDeserializedPayloadAsJson(rearrangedExecutedTransaction);
    transactionJson.replace("transactionPayload", deserializedPayloadJson);
    return transactionJson;
  }

  /**
   * Recursively retrieves information about all events spawned by the provided transaction and
   * appends this information to the given JSON node.
   *
   * @param parentTransaction The executed transaction to have its sub-event information expanded.
   * @param originalTransactionJson The executed transaction as JSON. Sub-event information will be
   *     appended to this node throughout the recursion.
   */
  private void addExpandedSubEvents(
      ExecutedTransaction parentTransaction, ArrayNode originalTransactionJson) {
    List<Event> subEvents = parentTransaction.events();
    List<ExecutedTransaction> childTransactions = new ArrayList<>();
    for (Event event : subEvents) {
      ExecutedTransaction childTransaction =
          client.getTransaction(
              new ShardId(event.destinationShard()), Hash.fromString(event.identifier()));
      JsonNode childJson = transactionToJson(childTransaction);
      childTransactions.add(childTransaction);

      originalTransactionJson = originalTransactionJson.add(childJson);
    }

    for (ExecutedTransaction childTransaction : childTransactions) {
      addExpandedSubEvents(childTransaction, originalTransactionJson);
    }
  }

  /**
   * Deserializes the payload of the given transaction and returns it as a JSON node. Additionally,
   * it deserializes and replaces the payload of the inner event or transaction, unless the
   * dontSerialize flag is set.
   *
   * @param transaction The transaction whose payload to deserialize.
   * @return The deserialized payload as JSON.
   */
  private JsonNode getDeserializedPayloadAsJson(RearrangedExecutedTransaction transaction) {
    if (transaction.isEvent()) {
      // Deserialize event payload
      ScValueStruct deserializedEventPayload =
          TransactionReader.deserializeEventPayload(transaction.transactionPayload());

      JsonNode deserializedEventJson = JsonValueConverter.toJson(deserializedEventPayload);

      if (show.dontDeserialize) {
        return deserializedEventJson;
      }

      ScValueEnum innerEvent =
          deserializedEventPayload
              .getFieldValue("event_transaction")
              .structValue()
              .getFieldValue("inner_event")
              .enumValue();
      JsonNode innerEventJson = deserializedEventJson.get("event_transaction").get("inner_event");

      String eventType = innerEvent.name();

      // Handles different event types
      switch (eventType) {
        case "Transaction" -> {
          ScValueEnum innerTransaction =
              innerEvent
                  .item()
                  .getFieldValue("inner_transaction")
                  .structValue()
                  .getFieldValue("transaction")
                  .enumValue();
          JsonNode innerTransactionJson =
              innerEventJson.get("inner_transaction").get("transaction");

          String innerTransactionType = innerTransaction.name();

          // Handle different inner transaction types
          switch (innerTransactionType) {
            case "InteractContract" -> {
              ScValueStruct interactContractTransaction =
                  innerTransaction
                      .item()
                      .getFieldValue("interact_with_contract_transaction")
                      .structValue();
              RpcValueFn rpc = getTransactionRpc(interactContractTransaction);
              JsonNode transactionJson =
                  innerTransactionJson.get("interact_with_contract_transaction");
              ((ObjectNode) transactionJson).replace("payload", JsonRpcConverter.toJson(rpc));
              return deserializedEventJson;
            }
            case "DeployContract" -> {
              ScValueStruct deployContractTransaction =
                  innerTransaction
                      .item()
                      .getFieldValue("create_contract_transaction")
                      .structValue();
              RpcValueFn rpc = getDeployRpc(deployContractTransaction);
              JsonNode transactionJson = innerTransactionJson.get("create_contract_transaction");
              ((ObjectNode) transactionJson).replace("rpc", JsonRpcConverter.toJson(rpc));
              return deserializedEventJson;
            }
            default -> {
              return deserializedEventJson;
            }
          }
        }
        case "Callback" -> {
          ScValueStruct callback =
              innerEvent.item().getFieldValue("callback_to_contract").structValue();
          RpcValueFn rpc = getCallbackRpc(callback);
          JsonNode callbackJson = innerEventJson.get("callback_to_contract");
          ((ObjectNode) callbackJson).replace("callback_rpc", JsonRpcConverter.toJson(rpc));
          return deserializedEventJson;
        }
        default -> {
          // If the event type is neither callback nor transaction, the payload is not deserialized
          return deserializedEventJson;
        }
      }
    } else {
      // Deserialize transaction payload
      ScValueStruct deserializeTransactionPayload =
          TransactionReader.deserializeTransactionPayload(transaction.transactionPayload());
      JsonNode deserializedTransactionJson =
          JsonValueConverter.toJson(deserializeTransactionPayload);
      if (show.dontDeserialize) {
        return deserializedTransactionJson;
      }
      ScValueStruct transactionStruct =
          deserializeTransactionPayload
              .getFieldValue("inner_part")
              .structValue()
              .getFieldValue("transaction")
              .structValue();
      JsonNode transactionJson = deserializedTransactionJson.get("inner_part").get("transaction");
      RpcValueFn rpc = getTransactionRpc(transactionStruct);
      ((ObjectNode) transactionJson).replace("payload", JsonRpcConverter.toJson(rpc));
      return deserializedTransactionJson;
    }
  }

  /**
   * Deserializes and returns the init transaction payload.
   *
   * @param deployContractTransaction The ScValueStruct of the transaction.
   * @return the rpc.
   */
  private RpcValueFn getDeployRpc(ScValueStruct deployContractTransaction) {
    byte[] payload = deployContractTransaction.getFieldValue("rpc").vecU8Value();
    byte[] abi = deployContractTransaction.getFieldValue("abi").vecU8Value();
    return getRpcFromAbi(abi, payload, FnKind.Init);
  }

  /**
   * Deserializes and returns the transaction payload.
   *
   * @param transaction The ScValueStruct of the transaction
   * @return the rpc.
   */
  private RpcValueFn getTransactionRpc(ScValueStruct transaction) {
    BlockchainAddress address = transaction.getFieldValue("contract_id").asBlockchainAddress();
    byte[] payload = transaction.getFieldValue("payload").vecU8Value();
    byte[] abi = client.getContractState(address).abi();
    return getRpcFromAbi(abi, payload, FnKind.Action);
  }

  /**
   * Deserializes and returns the callback payload.
   *
   * @param callback The ScValueStruct of the callback.
   * @return the rpc.
   */
  private RpcValueFn getCallbackRpc(ScValueStruct callback) {
    BlockchainAddress address = callback.getFieldValue("address").asBlockchainAddress();
    byte[] payload = callback.getFieldValue("callback_rpc").vecU8Value();
    byte[] abi = client.getContractState(address).abi();
    return getRpcFromAbi(abi, payload, FnKind.Callback);
  }

  /**
   * Deserializes and returns the rpc of the given payload using the contract abi and function kind.
   *
   * @param abi The contract abi.
   * @param payload The transaction payload
   * @param fnKind the function kind, either Action, Callback or Init
   * @return the rpc.
   */
  private static RpcValueFn getRpcFromAbi(byte[] abi, byte[] payload, FnKind fnKind) {
    FileAbi fileAbi = new AbiParser(abi).parseAbi();
    RpcReader reader = new RpcReader(payload, fileAbi, fnKind);
    return reader.readRpc();
  }

  /**
   * Wraps the provided JSON node in an array.
   *
   * @param json The JSON node to insert in an empty Array node.
   * @return The JSON node in an array.
   */
  private ArrayNode toArray(JsonNode json) {
    ArrayNode arrayNode = objectMapper.createObjectNode().arrayNode();
    return arrayNode.add(json);
  }

  /**
   * An executed transaction with its fields rearranged to achieve a more human-readable JSON
   * string.
   */
  private record RearrangedExecutedTransaction(
      String identifier,
      String from,
      String block,
      long blockTime,
      long productionTime,
      boolean finalized,
      boolean executionSucceeded,
      FailureCause failureCause,
      TransactionCost transactionCost,
      boolean isEvent,
      byte[] transactionPayload,
      List<Event> events) {

    /**
     * Returns the provided executed transaction DTO with its fields rearranged according to this
     * method.
     *
     * @param transaction The executed transaction to rearrange.
     * @return The rearranged executed transaction.
     */
    private static RearrangedExecutedTransaction create(ExecutedTransaction transaction) {
      return new RearrangedExecutedTransaction(
          transaction.identifier(),
          transaction.from(),
          transaction.block(),
          transaction.blockTime(),
          transaction.productionTime(),
          transaction.finalized(),
          transaction.executionSucceeded(),
          transaction.failureCause(),
          transaction.transactionCost(),
          transaction.isEvent(),
          transaction.transactionPayload(),
          transaction.events());
    }
  }
}
