package com.partisiablockchain.language.partisiacli.transaction.sign;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.transaction.TransactionSigner;
import com.partisiablockchain.language.partisiacli.transaction.sharedoptions.SignOptions;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.nio.file.Path;
import picocli.CommandLine;

/** Loads a prebuilt unsigned transaction from a binary file and sign it. */
@CommandLine.Command(
    name = "sign",
    description = "Sign a prebuilt unsigned transaction loaded from a binary file.")
public final class Sign implements Command {

  /** The unsigned transaction to sign. */
  @CommandLine.Parameters(
      paramLabel = "<input-file>",
      description = "The unsigned transaction to sign.",
      index = "0")
  public Path inputFile;

  /** Where to save the signed transaction. */
  @CommandLine.Parameters(
      paramLabel = "<output-file>",
      description = "Where to save the signed transaction.",
      index = "1")
  public Path outputFile;

  /** The information needed to sign the transaction. */
  @CommandLine.Mixin public SignOptions signOptions = new SignOptions();

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();
    UserConfig userConfig =
        new UserConfigStorage(fileManager, executionEnvironment.configPath()).loadConfig();
    byte[] txBytes =
        fileManager.readBytes(inputFile, "Failed to read file: %s".formatted(inputFile));
    Transaction transaction = readTransaction(txBytes);
    KeyManagement keyManager =
        new KeyManagement(signOptions.privateKey, signOptions.wallet, fileManager, userConfig);
    SignedTransaction signedTransaction =
        TransactionSigner.sign(
            blockchainNet,
            transaction,
            keyManager.getAuthentication(),
            signOptions.gas,
            signOptions.validityDuration);
    fileManager.writeBytes(
        SafeDataOutputStream.serialize(signedTransaction),
        outputFile,
        "Failed to write file: %s".formatted(outputFile));
  }

  /**
   * Converts the given bytes to a {@link com.partisiablockchain.client.transaction.Transaction}.
   * There is no guarantee that the bytes hold a valid transaction. As such the method is
   * recommended to use on bytes stemming from a previously serialized {@link
   * com.partisiablockchain.client.transaction.Transaction}-
   *
   * @param transaction Bytes representing a transaction.
   * @return The parsed transaction.
   */
  private static Transaction readTransaction(byte[] transaction) {
    return Transaction.read(SafeDataInputStream.createFromBytes(transaction));
  }
}
