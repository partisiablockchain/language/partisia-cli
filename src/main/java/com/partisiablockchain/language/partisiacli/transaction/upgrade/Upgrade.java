package com.partisiablockchain.language.partisiacli.transaction.upgrade;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.deploymentclient.DeploymentClient;
import com.partisiablockchain.language.deploymentclient.UpgradeBuilder;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.FileInfo;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.AddressConverter;
import com.partisiablockchain.language.partisiacli.transaction.TransactionCommand;
import com.partisiablockchain.language.partisiacli.transaction.build.Build;
import com.partisiablockchain.language.partisiacli.transaction.deploy.ContractDeployment;
import java.nio.file.Path;
import java.util.HexFormat;
import picocli.CommandLine;

/**
 * Builds, signs and sends a transaction payload which upgrades a smart-contract on the blockchain.
 */
@CommandLine.Command(
    name = "upgrade",
    description =
        "Build, sign and send a transaction that upgrades a smart-contract on the blockchain.")
public final class Upgrade extends Build {

  /** Where to read possible abi file from. */
  @CommandLine.Option(
      names = {"--abi"},
      description = "The path to the smart contract ABI file (.abi) to deploy.")
  public Path abiFile;

  /** The address of the contract being upgraded. */
  @CommandLine.Parameters(
      description = "The address of the contract to upgrade.",
      paramLabel = "<contract-address>",
      index = "0",
      converter = AddressConverter.class)
  public BlockchainAddress contractAddress;

  /** Where to read the bytecode from. */
  @CommandLine.Parameters(
      description =
          "The path to the smart contract bytecode file (.wasm or .pbc) to upgrade to. "
              + "If it is a .wasm or .zkwa file, the contract ABI should also be provided.",
      paramLabel = "<bytecode-file>",
      index = "1")
  public Path bytecodeFile;

  /** The initialization arguments for the contract being deployed. */
  @CommandLine.Parameters(
      description = "The upgrade rpc (in hex) for the contract being upgraded",
      paramLabel = "<upgrade-rpc>",
      index = "2",
      arity = "0..1")
  public String upgradeRpc = "";

  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    FileManager fileManager = executionEnvironment.fileManager();

    FileInfo byteCodeFileInfo = new FileInfo(bytecodeFile, fileManager);
    if (byteCodeFileInfo.fileType != FileInfo.FileType.Pbc
        && byteCodeFileInfo.fileType != FileInfo.FileType.Wasm) {
      throw new RuntimeException("byte-code must be either wasm or pbc");
    }
    if (byteCodeFileInfo.fileType == FileInfo.FileType.Pbc && abiFile != null) {
      throw new RuntimeException("--abi option is not allowed when providing a .pbc file");
    }
    if (byteCodeFileInfo.fileType == FileInfo.FileType.Wasm && abiFile == null) {
      throw new RuntimeException(
          "You must provide an abi with the --abi option when using a wasm file");
    }

    DeploymentClient deploymentClient =
        DeploymentClient.create(null, new ChainControllerApi(blockchainNet.getApiClient()));
    UpgradeBuilder upgradeBuilder = deploymentClient.upgradeBuilder();
    if (byteCodeFileInfo.fileType == FileInfo.FileType.Pbc) {
      byte[] pbcBytes =
          fileManager.readBytes(
              bytecodeFile,
              "Exception occurred when trying to read file %s".formatted(bytecodeFile));
      upgradeBuilder.pbcFile(pbcBytes);
    } else {
      byte[] contractBytes =
          fileManager.readBytes(
              bytecodeFile, "Failed to read byte-code from: %s".formatted(bytecodeFile));
      byte[] abiBytes =
          fileManager.readBytes(abiFile, "Failed to read abi from: %s".formatted(abiFile));

      upgradeBuilder.abi(abiBytes).wasm(contractBytes);
    }

    upgradeBuilder.contractAddress(contractAddress).upgradeRpc(HexFormat.of().parseHex(upgradeRpc));

    TransactionCommand.executeTransaction(
        blockchainNet,
        executionEnvironment,
        this,
        Transaction.create(ContractDeployment.PUB_DEPLOY_ADDRESS, upgradeBuilder.buildRpc()),
        this::reportUpgrade);
  }

  private String reportUpgrade(BlockchainNet blockchainNet, Hash txIdentifier) {
    String deploySuccessful =
        "Upgraded contract %s successfully.\n".formatted(contractAddress.writeAsString());
    if (blockchainNet.hasWebUrl()) {
      return deploySuccessful
          + "View upgrade transaction in browser here: "
          + blockchainNet.getEventUrl(txIdentifier);
    } else {
      return deploySuccessful + "Upgrade transaction identifier: " + txIdentifier;
    }
  }
}
