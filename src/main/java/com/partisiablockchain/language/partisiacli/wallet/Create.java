package com.partisiablockchain.language.partisiacli.wallet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.language.bip39.Bip39;
import com.partisiablockchain.language.partisiacli.BlockchainNet;
import com.partisiablockchain.language.partisiacli.Command;
import com.partisiablockchain.language.partisiacli.KeyManagement;
import com.partisiablockchain.language.partisiacli.TestnetGas;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.config.UserConfigStorage;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import java.nio.file.Path;
import picocli.CommandLine;

/** Create a new wallet to use on the blockchain. */
@CommandLine.Command(
    name = "create",
    description =
        """
        Create a new wallet and save it to a file
        (by default the file is ~/.pbc/id_pbc ).
        A wallet consists of a 12-word mnemonic phrase,
        which is used to derive private keys that can be used on the blockchain.
        If the target is the Testnet, then the first account is also filled with gas.""")
public final class Create implements Command {

  /** Optional path to output created wallet file onto. */
  @CommandLine.Option(
      names = {"--output-path", "-o"},
      paramLabel = "<path>",
      description = "Output the newly created wallet to the specified file.")
  public Path outputPath;

  /** Overwrites the wallet in the location if it exists. */
  @CommandLine.Option(
      names = {"--force", "-f"},
      description = "Overwrite existing wallet file in this location.",
      defaultValue = "false")
  Boolean force;

  /**
   * Create a new wallet, and if the target chain is Testnet, fill the first account in the wallet
   * with gas.
   */
  @Override
  public void execute(ExecutionEnvironment executionEnvironment, BlockchainNet blockchainNet) {
    byte[] entropy = new byte[16];
    executionEnvironment.rng().nextBytes(entropy);
    String mnemonic = Bip39.generateMnemonic(entropy);
    Path path;
    if (outputPath != null) {
      path = outputPath;
    } else {
      path = executionEnvironment.walletPath();
    }

    FileManager fileManager = executionEnvironment.fileManager();

    if (!force) {
      createBackup(path, fileManager);
    }

    fileManager.writeString(mnemonic, path, "Error while writing mnemonic phrase to file");

    if (blockchainNet.isTestnet()) {
      UserConfig userConfig =
          new UserConfigStorage(fileManager, executionEnvironment.configPath()).loadConfig();
      BlockchainAddress accountAddress =
          new KeyManagement(null, path, fileManager, userConfig).getAuthentication().getAddress();
      TestnetGas.addGasToAccount(accountAddress, blockchainNet, executionEnvironment.out());
    }
  }

  private void createBackup(Path path, FileManager fileManager) {
    if (fileManager.fileExists(path)) {
      fileManager.createBak(path);
    }
  }
}
