package com.partisiablockchain.client.transaction;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.language.partisiacli.executionenvironment.ClientProvider;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import com.partisiablockchain.language.zkclient.RealZkClient;
import com.partisiablockchain.language.zkclient.ZkClientInfiltrator;
import java.util.Random;

/**
 * A {@link ClientProvider} that provides pairs of recording and replaying clients as dictated by
 * the framework, respectively.
 */
public final class ClientProviderRecordReplay implements ClientProvider {

  private final BlockchainRecordReplay recordReplay;
  private final Random rng;

  /**
   * Provides either a pair of recording or replaying clients.
   *
   * @param recordReplay the record replay client provider.
   */
  public ClientProviderRecordReplay(BlockchainRecordReplay recordReplay, Random rng) {
    this.recordReplay = recordReplay;
    this.rng = rng;
  }

  @Override
  public ApiClient apiClient(String restBase) {
    return recordReplay.getApiClient(restBase);
  }

  @Override
  public WebClient webClient() {
    return recordReplay.getWebClient();
  }

  @Override
  public RealZkClient getZkClient(
      BlockchainAddress contractAddress, BlockchainClient blockchainClient) {
    return ZkClientInfiltrator.createForTest(contractAddress, webClient(), blockchainClient, rng);
  }

  @Override
  public BlockchainClient blockchainClient(String restBase, int numberOfShards) {
    return recordReplay.getBlockchainClient(restBase, numberOfShards);
  }

  @Override
  public BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender, long transactionValidity) {
    return recordReplay.getTransactionClient(
        sender, restBase, numberOfShards, transactionValidity, 600_000L);
  }

  @Override
  public BlockchainTransactionClient blockchainTransactionClient(
      String restBase, int numberOfShards, SenderAuthentication sender) {
    return recordReplay.getTransactionClient(sender, restBase, numberOfShards);
  }
}
