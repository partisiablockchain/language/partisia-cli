package com.partisiablockchain.language.bip32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.bip32.Bip32.ser256;
import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.partisiablockchain.crypto.BlockchainPublicKey;
import java.math.BigInteger;
import java.util.Arrays;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests for BIP32. The test vectors are found <a
 * href="https://en.bitcoin.it/wiki/BIP_0032_TestVectors">here</a>.
 */
public final class Bip32Test {

  // Feature: Master key generation

  /**
   * Generating a master key from the seed from test vector 1 produces the private key and chain
   * code from test vector 1.
   */
  @Test
  public void testGenerateMasterKey() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    String expectedPrivateKey = "e8f32e723decf4051aefac8e2c93c9c5b214313817cdb01a1494b917c8436b35";
    String expectedChainCode = "873dff81c02f525623fd1fe5167eac3a55a049de3d314bb42ee227ffed37d508";

    assertEquals(expectedPrivateKey, masterKey.getPrivateKeyHex());
    assertEquals(expectedChainCode, masterKey.getChainCodeHex());
    assertEquals(0, masterKey.index());
    assertEquals(0, masterKey.depth());
  }

  // Feature: Child key derivation.

  /**
   * Deriving m/0' from the seed from test vector 1 produces the private key and chain code for m/0'
   * from test vector 1.
   *
   * @throws Exception If child key derivation results in an invalid key.
   */
  @Test
  public void testDeriveHardenedChildKey() throws Exception {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    // Derive m/0' (hardened)
    int hardenedIndex = 0x80000000;
    ExtendedKey childKey = Bip32.deriveChildKey(masterKey, hardenedIndex);

    String expectedPrivateKey = "edb2e14f9ee77d26dd93b4ecede8d16ed408ce149b6cd80b0715a2d911a0afea";
    String expectedChainCode = "47fdacbd0f1097043b78c63c20c34ef4ed9a111d980047ad16282c7ae6236141";

    assertEquals(expectedPrivateKey, childKey.getPrivateKeyHex());
    assertEquals(expectedChainCode, childKey.getChainCodeHex());
    assertEquals(0x80000000, childKey.index());
    assertEquals(1, childKey.depth());
  }

  /**
   * Deriving the private key m/0'/1 from the seed from test vector 1 produces the private key and
   * chain code for m/0'/1 from test vector 1.
   *
   * @throws Exception If child key derivation results in an invalid key.
   */
  @Test
  public void testDeriveNonHardenedChildKey() throws Exception {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    // Derive m/0'/1
    int hardenedIndex = 0x80000000;
    ExtendedKey childKey0 = Bip32.deriveChildKey(masterKey, hardenedIndex);
    ExtendedKey childKey1 = Bip32.deriveChildKey(childKey0, 1);

    String expectedPrivateKey = "3c6cb8d0f6a264c91ea8b5030fadaa8e538b020f0a387421a12de9319dc93368";
    String expectedChainCode = "2a7857631386ba23dacac34180dd1983734e444fdbf774041578e9b6adb37c19";

    assertEquals(expectedPrivateKey, childKey1.getPrivateKeyHex());
    assertEquals(expectedChainCode, childKey1.getChainCodeHex());
    assertEquals(1, childKey1.index());
    assertEquals(2, childKey1.depth());
  }

  @Test
  public void testDeriveHardenedChildKey_testVector2() throws Exception {
    byte[] seed =
        hexSeedToBytes(
            "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9"
                + "c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    // Derive m/0/2147483647'
    int hardenedIndex = 0x80000000;
    ExtendedKey childKey0 = Bip32.deriveChildKey(masterKey, 0);
    ExtendedKey childKey1 = Bip32.deriveChildKey(childKey0, hardenedIndex + 2147483647);

    String expectedPrivateKey = "877c779ad9687164e9c2f4f0f4ff0340814392330693ce95a58fe18fd52e6e93";
    String expectedChainCode = "be17a268474a6bb9c61e1d720cf6215e2a88c5406c4aee7b38547f585c9a37d9";

    assertEquals(expectedPrivateKey, childKey1.getPrivateKeyHex());
    assertEquals(expectedChainCode, childKey1.getChainCodeHex());
    assertEquals(hardenedIndex + 2147483647, childKey1.index());
    assertEquals(2, childKey1.depth());
  }

  /**
   * Deriving the public key m/0'/1 from the seed from test vector 1 produces the public key and
   * chain code for m/0'/1 from test vector 1.
   *
   * @throws Exception If child key derivation results in an invalid key.
   */
  @Test
  public void testDerivePublicKeyFromPublicParent() throws Exception {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    // Derive m/0'
    int hardenedIndex = 0x80000000;
    ExtendedKey childKey = Bip32.deriveChildKey(masterKey, hardenedIndex);

    ExtendedKey publicKey =
        new ExtendedKey(
            null, childKey.publicKey(), childKey.chainCode(), childKey.depth(), childKey.index());

    ExtendedKey grandChildKey = Bip32.deriveChildKey(publicKey, 1);

    String expectedPublicKey = "03501e454bf00751f24b1b489aa925215d66af2234e3891c3b21a52bedb3cd711c";

    assertEquals(expectedPublicKey, grandChildKey.getPublicKeyHex());
    assertEquals(1, grandChildKey.index());
    assertEquals(2, grandChildKey.depth());
  }

  /**
   * Deriving the public key m/0' throws an exception as one cannot derive a hardened child from a
   * public key.
   */
  @Test
  public void testDeriveHardenedChildFromPublicKey() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);

    ExtendedKey publicKey =
        new ExtendedKey(
            null,
            masterKey.publicKey(),
            masterKey.chainCode(),
            masterKey.depth(),
            masterKey.index());

    int hardenedIndex = 0x80000000;

    try {
      Bip32.deriveChildKey(publicKey, hardenedIndex);
      fail("Expected an Exception to be thrown when deriving a hardened child from a public key");
    } catch (Exception e) {
      assertEquals("Cannot derive hardened key from public key", e.getMessage());
    }
  }

  // Edge cases

  /** Generating a master key with IL larger than curve order throws an exception. */
  @Test
  public void ilMasterLargerThanCurveOrder() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    Exception exception =
        Assertions.assertThrows(
            RuntimeException.class,
            () ->
                Bip32.generateMasterKey(
                    seed,
                    (key, toHash) ->
                        ser256(
                            new BigInteger(
                                "115792089237316195423570985008687907"
                                    + "852837564279074904382605163141518161494337"))));
    assertEquals("Invalid master key generated", exception.getMessage());
  }

  /** Generating a master key equal to 0 throws an exception. */
  @Test
  public void masterPrivateKeyCannotBe0() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    Exception exception =
        Assertions.assertThrows(
            RuntimeException.class,
            () -> Bip32.generateMasterKey(seed, (key, toHash) -> ser256(BigInteger.ZERO)));
    assertEquals("Invalid master key generated", exception.getMessage());
  }

  /** Deriving a child with IL larger than curve order throws exception. */
  @Test
  public void ilDerivationLargerThanCurveOrder() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);
    Exception exception =
        Assertions.assertThrows(
            RuntimeException.class,
            () ->
                Bip32.deriveChildKey(
                    masterKey,
                    1,
                    (key, toHash) ->
                        ser256(
                            new BigInteger(
                                "115792089237316195423570985008687907"
                                    + "852837564279074904382605163141518161494337"))));
    assertEquals("Invalid key derived", exception.getMessage());
  }

  /** Child private key cannot be 0. */
  @Test
  public void childPrivateKeyCannotBe0() {
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);
    Exception exception =
        Assertions.assertThrows(
            RuntimeException.class,
            () ->
                Bip32.deriveChildKey(
                    masterKey,
                    1,
                    (key, toHash) ->
                        ser256(
                            masterKey
                                .privateKey()
                                .subtract(
                                    new BigInteger(
                                        "9494040129937650177922866863331973634"
                                            + "4545471688410404926661107621815724840233")))));
    assertEquals("Invalid child private key derived", exception.getMessage());
  }

  /** Child public key cannot be at infinity. */
  @Test
  public void childPublicKeyCannotBeAtInf() {
    X9ECParameters params = SECNamedCurves.getByName("secp256k1");
    ECDomainParameters ecParams =
        new ECDomainParameters(params.getCurve(), params.getG(), params.getN(), params.getH());
    byte[] seed = hexSeedToBytes("000102030405060708090a0b0c0d0e0f");
    ExtendedKey masterKey = Bip32.generateMasterKey(seed);
    ExtendedKey publicKey =
        new ExtendedKey(
            null,
            BlockchainPublicKey.fromEncodedEcPoint(
                new FixedPointCombMultiplier()
                    .multiply(ecParams.getG(), BigInteger.ZERO)
                    .normalize()
                    .getEncoded(true)),
            masterKey.chainCode(),
            masterKey.depth(),
            masterKey.index());
    byte[] zeroes = new byte[64];
    Arrays.fill(zeroes, (byte) 0);
    Exception exception =
        Assertions.assertThrows(
            RuntimeException.class,
            () -> Bip32.deriveChildKey(publicKey, 1, (key, toHash) -> zeroes));
    assertEquals("Invalid child public key derived", exception.getMessage());
  }

  private byte[] hexSeedToBytes(String seed) {
    int len = seed.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
      data[i / 2] =
          (byte)
              ((Character.digit(seed.charAt(i), 16) << 4)
                  + Character.digit(seed.charAt(i + 1), 16));
    }
    return data;
  }
}
