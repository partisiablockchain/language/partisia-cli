package com.partisiablockchain.language.bip39;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Tests for mnemonic phrase generation and checking. */
final class Bip39Test {

  // Feature: generate a mnemonic phrase based on entropy input.

  /**
   * Check mnemonic phrase generation according to test vectors found <a
   * href="https://github.com/trezor/python-mnemonic/blob/master/vectors.json">here</a>.
   */
  @Test
  public void testVector1() {
    String entropyHex = "00000000000000000000000000000000";
    String expectedMnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " about";
    String passphrase = "TREZOR";
    String expectedSeedHex =
        "c55257c360c07c72029aebc1b53c05ed0362ada38ead3e3e9efa3708e"
            + "53495531f09a6987599d18264c1e1c92f2cf141630c7a3c4ab7c81b2f001698e7463b04";

    // Convert entropy hex string to byte array
    byte[] entropy = Hex.decode(entropyHex);

    // Generate mnemonic
    String mnemonic = Bip39.generateMnemonic(entropy);

    // Verify mnemonic
    assertEquals(expectedMnemonic, mnemonic);

    // Derive seed from mnemonic
    byte[] seed = Bip39.mnemonicToSeed(mnemonic, passphrase);

    // Verify seed
    String seedHex = Hex.toHexString(seed);
    assertEquals(expectedSeedHex, seedHex);
  }

  @Test
  public void testVector2() {
    String entropyHex = "f585c11aec520db57dd353c69554b21a89b20fb0650966fa0a9d6f74fd989d8f";
    String expectedMnemonic =
        "void come effort suffer camp survey warrior heavy shoot primary clutch crush open amazing"
            + " screen patrol group space point ten exist slush involve unfold";
    String passphrase = "TREZOR";
    String expectedSeedHex =
        "01f5bced59dec48e362f2c45b5de68b9fd6c92c6634f44d6d40aab69056506f0e"
            + "35524a518034ddc1192e1dacd32c1ed3eaa3c3b131c88ed8e7e54c49a5d0998";

    // Convert entropy hex string to byte array
    byte[] entropy = Hex.decode(entropyHex);

    // Generate mnemonic
    String mnemonic = Bip39.generateMnemonic(entropy);

    // Verify mnemonic
    assertEquals(expectedMnemonic, mnemonic);

    // Derive seed from mnemonic
    byte[] seed = Bip39.mnemonicToSeed(mnemonic, passphrase);

    // Verify seed
    String seedHex = Hex.toHexString(seed);
    assertEquals(expectedSeedHex, seedHex);
  }

  @Test
  public void testVector3() {
    String entropyHex = "80808080808080808080808080808080";
    String expectedMnemonic =
        "letter advice cage absurd amount doctor acoustic avoid letter advice cage above";
    String passphrase = "TREZOR";
    String expectedSeedHex =
        "d71de856f81a8acc65e6fc851a38d4d7ec216fd0796d0a6827a3ad6ed5511a30fa"
            + "280f12eb2e47ed2ac03b5c462a0358d18d69fe4f985ec81778c1b370b652a8";

    // Convert entropy hex string to byte array
    byte[] entropy = Hex.decode(entropyHex);

    // Generate mnemonic
    String mnemonic = Bip39.generateMnemonic(entropy);

    // Verify mnemonic
    assertEquals(expectedMnemonic, mnemonic);

    // Derive seed from mnemonic
    byte[] seed = Bip39.mnemonicToSeed(mnemonic, passphrase);

    // Verify seed
    String seedHex = Hex.toHexString(seed);
    assertEquals(expectedSeedHex, seedHex);
  }

  // Feature: Check validity of entropy

  /** Entropy not a multiple of 32 throws. */
  @Test
  public void entropyNotMultipleOf32Throws() {
    byte[] entropy = new byte[200 / 8];
    assertThrows(
        RuntimeException.class,
        () -> Bip39.generateMnemonic(entropy),
        "Invalid entropy size, it should be a multiple of 32 and in the range [128;256]");
  }

  /** Entropy of bit size less than 128 throws. */
  @Test
  public void entropyLessThan128BitsThrows() {
    byte[] entropy = new byte[96 / 8];
    assertThrows(
        RuntimeException.class,
        () -> Bip39.generateMnemonic(entropy),
        "Invalid entropy size, it should be a multiple of 32 and in the range [128;256]");
  }

  /** Entropy of bit size greater than 256 throws. */
  @Test
  public void entropyGreaterThan256BitsThrows() {
    byte[] entropy = new byte[288 / 8];
    assertThrows(
        RuntimeException.class,
        () -> Bip39.generateMnemonic(entropy),
        "Invalid entropy size, it should be a multiple of 32 and in the range [128;256]");
  }

  // Feature: Check validity of mnemonic phrase.

  /** A valid 12 word mnemonic phrase is valid. */
  @Test
  public void testValid12WordMnemonic() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " about";
    assertDoesNotThrow(() -> Bip39.validateMnemonic(mnemonic));
  }

  /** A valid 18 word mnemonic phrase is valid. */
  @Test
  public void testValid18WordMnemonic() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon abandon abandon agent";
    assertDoesNotThrow(() -> Bip39.validateMnemonic(mnemonic));
  }

  /** A valid 24 word mnemonic phrase is valid. */
  @Test
  public void testValid24WordMnemonic() {
    String mnemonic =
        "letter advice cage absurd amount doctor acoustic avoid letter advice cage absurd amount"
            + " doctor acoustic avoid letter advice cage absurd amount doctor acoustic bless";
    assertDoesNotThrow(() -> Bip39.validateMnemonic(mnemonic));
  }

  /** A mnemonic phrase containing extra spaces between words is still valid. */
  @Test
  public void testValidMnemonicWithExtraSpaces() {
    String mnemonic =
        "  abandon  abandon  abandon abandon abandon  abandon abandon abandon abandon abandon"
            + " abandon about  ";
    assertDoesNotThrow(() -> Bip39.validateMnemonic(mnemonic));
  }

  /** A mnemonic phrase containing an invalid word is not valid. */
  @Test
  public void testInvalidMnemonicWrongWord() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " foobar";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word found in mnemonic: foobar", exception.getMessage());
  }

  /** A mnemonic phrase leading to an invalid checksum is not valid. */
  @Test
  public void testInvalidMnemonicIncorrectChecksum() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid checksum", exception.getMessage());
  }

  /** A mnemonic phrase containing only 2 words is not valid. */
  @Test
  public void testInvalidMnemonicTooLowWordCount() {
    String mnemonic = "abandon abandon";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word count: 2", exception.getMessage());
  }

  /** A mnemonic phrase containing 25 words is not valid. */
  @Test
  public void testInvalidMnemonicLargeWordCount() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon abandon abandon abandon abandon";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word count: 25", exception.getMessage());
  }

  /** A mnemonic phrase containing 27 words is not valid. */
  @Test
  public void testInvalidMnemonicEvenLargerWordCount() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word count: 27", exception.getMessage());
  }

  /** A mnemonic phrase containing 13 words is not valid. */
  @Test
  public void testInvalidMnemonicWordCountNotDivisibleBy3() {
    String mnemonic =
        "abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " abandon abandon abandon abandon";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word count: 13", exception.getMessage());
  }

  /** A mnemonic phrase containing only the empty string is not valid. */
  @Test
  public void testInvalidMnemonicEmptyString() {
    String mnemonic = "";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word count: 0", exception.getMessage());
  }

  /** A mnemonic phrase that is null is not valid. */
  @Test
  public void testInvalidMnemonicNull() {
    Exception exception =
        assertThrows(NullPointerException.class, () -> Bip39.validateMnemonic(null));
    assertEquals("Mnemonic is null", exception.getMessage());
  }

  /** A mnemonic phrase containing capitalized words is not valid. */
  @Test
  public void testInvalidMnemonicCaseSensitivity() {
    String mnemonic =
        "Abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon"
            + " about";
    Exception exception =
        assertThrows(RuntimeException.class, () -> Bip39.validateMnemonic(mnemonic));
    assertEquals("Invalid word found in mnemonic: Abandon", exception.getMessage());
  }

  /**
   * When getting the amount of bytes needed to contain a certain amount of bits it should be \lceil
   * bits/8 \rceil.
   */
  @Test
  public void byteToBitConversionRoundsUp() {
    assertEquals(1, Bip39.getByteAmountFromBits(7));
    assertEquals(1, Bip39.getByteAmountFromBits(8));
    assertEquals(2, Bip39.getByteAmountFromBits(9));
  }
}
