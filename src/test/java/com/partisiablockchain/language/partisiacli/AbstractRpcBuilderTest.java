package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.rpc.FnRpcBuilder;
import com.partisiablockchain.language.abimodel.model.FileAbi;
import com.partisiablockchain.language.abimodel.model.SetTypeSpec;
import com.partisiablockchain.language.abimodel.model.SimpleTypeSpec;
import com.partisiablockchain.language.abimodel.model.TypeSpec;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AbstractRpcBuilderTest {

  @Test
  void illegalArgument() {
    FileAbi fileAbi = new FileAbi(null, null, null, -1, null);
    FnRpcBuilder fnRpcBuilder = new FnRpcBuilder(new byte[0]);
    Assertions.assertThatThrownBy(
            () ->
                new ActionRpcBuilder(fileAbi, null)
                    .addArgumentToBuilder(
                        fnRpcBuilder,
                        new PeekingIterator<>(Stream.of("undefined").iterator()),
                        new SetTypeSpec(new SimpleTypeSpec(TypeSpec.TypeIndex.i32))))
        .hasMessage("Type 'Set' is not supported.");
  }
}
