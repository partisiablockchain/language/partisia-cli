package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

/** Tests for the BlockIterator. */
final class BlockIteratorTest {

  BlockchainClient client;

  boolean recording = false;

  @BeforeEach
  void setup(TestInfo info) {
    String packageName = "src/test/resources/blockIterator-test/" + info.getDisplayName();
    Path path = Path.of(packageName);
    if (!path.toFile().exists()) {
      path.toFile().mkdirs();
    }
    client =
        new BlockchainRecordReplay(recording, packageName, BlockchainUrls.TESTNET_REST_URL, 3)
            .getBlockchainClient();
  }

  // Feature: Get the latest blocks.

  /** The iterator begins at the most recent block. */
  @Test
  void iteratorGetsLatestBlock() {
    BlockIterator blockIterator = new BlockIterator(client);
    BlockIterator.BlockData blockDataFromIterator = blockIterator.next();

    // Check that the block we got is more recent than all the latest blocks on the other shards.
    for (BlockIterator.BlockData blockData : blockIterator.nextShardBlocks) {
      assertThat(blockData.blockState().productionTime())
          .isLessThanOrEqualTo(blockDataFromIterator.blockState().productionTime());
    }
  }

  /** Getting two blocks yields two blocks in chronological order. */
  @Test
  void blockIteratorNextProgressesChronologically() {
    BlockIterator blockIterator = new BlockIterator(client);
    BlockIterator.BlockData blockData0 = blockIterator.next();
    BlockIterator.BlockData blockData1 = blockIterator.next();

    assertThat(blockData0.blockState().productionTime())
        .isGreaterThan(blockData1.blockState().productionTime());
  }

  /** Getting two blocks yields two different blocks. */
  @Test
  void blockIteratorNextProgressesIterator() {
    BlockIterator blockIterator = new BlockIterator(client);
    BlockIterator.BlockData blockData0 = blockIterator.next();
    BlockIterator.BlockData blockData1 = blockIterator.next();

    assertThat(blockData0).isNotEqualTo(blockData1);
  }

  /** Peeking returns the next block. */
  @Test
  void peekReturnsNextBlock() {
    BlockIterator blockIterator = new BlockIterator(client);
    BlockIterator.BlockData peekedBlock = blockIterator.peek();
    BlockIterator.BlockData nextBlock = blockIterator.next();

    assertThat(peekedBlock).isEqualTo(nextBlock);
  }

  /** Peeking twice returns the same block. */
  @Test
  void peekTwiceSameBlock() {
    BlockIterator blockIterator = new BlockIterator(client);
    BlockIterator.BlockData peekedBlock0 = blockIterator.peek();
    BlockIterator.BlockData peekedBlock1 = blockIterator.peek();

    assertThat(peekedBlock0).isEqualTo(peekedBlock1);
  }

  /** Peeking multiple times does not change the next block. */
  @Test
  void peekDoesNotAdvanceIterator() {
    BlockIterator blockIterator = new BlockIterator(client);
    List<BlockIterator.BlockData> peekedBlocks = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      peekedBlocks.add(blockIterator.peek());
    }

    // All the peeked blocks should reference the same block
    assertThat(new HashSet<>(peekedBlocks).size()).isEqualTo(1);
    // next() should give us the same block as we saw in peeks
    assertThat(peekedBlocks.get(0)).isEqualTo(blockIterator.next());
  }

  /** The blocks returned are in chronological order. */
  @Test
  void peekGivesTheNextBlockChronologically() {
    Set<String> shardsSeen = new HashSet<>();
    BlockIterator blockIterator = new BlockIterator(client);
    // Check 20 times that the current peek has a greater timestamp than the next
    for (int i = 0; i < 20; i++) {
      BlockIterator.BlockData blockData0 = blockIterator.peek();
      blockIterator.next();
      BlockIterator.BlockData blockData1 = blockIterator.peek();

      assertThat(blockData0).isNotEqualTo(blockData1);
      assertThat(blockData0.blockState().productionTime())
          .isGreaterThanOrEqualTo(blockData1.blockState().productionTime());

      shardsSeen.add(blockData0.shardId().toString());
      shardsSeen.add(blockData1.shardId().toString());
    }
    List<String> allShards =
        List.of(
            new ShardId("null").toString(),
            new ShardId("Shard0").toString(),
            new ShardId("Shard1").toString(),
            new ShardId("Shard2").toString());

    // Check that all shards have been seen
    assertThat(shardsSeen.containsAll(allShards)).isTrue();
  }

  // Feature: Get latest blocks from specific shard.

  /** The first element for a specific shard, should be from that specific shard. */
  @Test
  void getLatestBlocksFromSpecificShard() {
    ShardId shardId = new ShardId("Shard1");
    BlockIterator blockIterator = new BlockIterator(client, shardId);
    BlockIterator.BlockData blockData = blockIterator.next();

    assertThat(blockData.shardId()).isEqualTo(shardId);
  }

  /**
   * Two consecutive elements for a specific shard should be different, but from the same specific
   * shard.
   */
  @Test
  void getTwoLatestBlocksFromSpecificShard() {
    ShardId shardId = new ShardId("Shard0");
    BlockIterator blockIterator = new BlockIterator(client, shardId);
    BlockIterator.BlockData blockData0 = blockIterator.next();
    BlockIterator.BlockData blockData1 = blockIterator.next();

    assertThat(blockData0.shardId()).isEqualTo(shardId);
    assertThat(blockData1.shardId()).isEqualTo(shardId);
    assertThat(blockData0).isNotEqualTo(blockData1);
  }

  // Feature: Get n blocks from given timestamp and back.

  /**
   * The first block returned from a specific timestamp, has a timestamp earlier than or equal to
   * the specific timestamp.
   */
  @Test
  void blockIteratorFromTimeGetsBlocksFromBeforeTimestamp() {
    long utcTime = 1709640577969L; // Random timestamp after genesis timestamp.
    BlockIterator iterator = new BlockIterator(client, utcTime);
    BlockIterator.BlockData blockData = iterator.peek();

    assertThat(blockData.blockState().productionTime()).isLessThanOrEqualTo(utcTime);
  }

  /** An iterator cannot be created for a timestamp earlier than the genesis block. */
  @Test
  void blockIteratorCannotBeCreatedForGenesisBlockTimestamp() {
    // Find the utcTime for the block just after the genesis block
    long utcTime =
        client
            .getBlockByHash(
                new ShardId("Shard0"),
                Hash.fromString("1c4d8a8a381c1aa50b245707c3331ce72ce3390dc3871c52463be40c0099ea60"))
            .productionTime();

    // Trying to get an iterator for before the block just after the genesis block should fail
    assertThatThrownBy(() -> new BlockIterator(client, utcTime - 1))
        .hasMessageContaining("HTTP 404 Not Found");
  }

  /** There are no elements after the genesis blocks. */
  @Test
  void noMoreElementsAfterGenesisBlock() {
    Hash genesisChild =
        Hash.fromString("1c4d8a8a381c1aa50b245707c3331ce72ce3390dc3871c52463be40c0099ea60");
    BlockIterator iteratorAtGenesisChild =
        new BlockIterator(
            client, client.getBlockByHash(new ShardId("Shard0"), genesisChild).productionTime());
    assertThat(iteratorAtGenesisChild.hasNext()).isTrue();
    // Iterate over the genesis block form each shard
    for (int i = 0; i < 5; i++) {
      iteratorAtGenesisChild.next();
    }

    assertThat(iteratorAtGenesisChild.hasNext()).isFalse();
  }

  /**
   * Genesis blocks are returned in the following order based on shards: Gov -> Shard 2 -> Shard 1
   * -> Shard0.
   */
  @Test
  void genesisBlocksAreReturnedInSpecificOrder() {
    Hash genesisChild =
        Hash.fromString("1c4d8a8a381c1aa50b245707c3331ce72ce3390dc3871c52463be40c0099ea60");
    BlockIterator iteratorAtGenesisChild =
        new BlockIterator(
            client, client.getBlockByHash(new ShardId("Shard0"), genesisChild).productionTime());

    assertThat(iteratorAtGenesisChild.next().shardId().toString())
        .isEqualTo(new ShardId("null").toString());
    assertThat(iteratorAtGenesisChild.next().shardId()).isEqualTo(new ShardId("Shard2"));
    assertThat(iteratorAtGenesisChild.next().shardId()).isEqualTo(new ShardId("Shard1"));
    assertThat(iteratorAtGenesisChild.next().shardId()).isEqualTo(new ShardId("Shard0"));
  }
}
