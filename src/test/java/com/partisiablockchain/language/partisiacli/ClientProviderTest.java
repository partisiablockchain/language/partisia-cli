package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardManager;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.language.partisiacli.executionenvironment.ClientProviderMain;
import com.secata.tools.coverage.ExceptionConverter;
import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;

/** Tests correct initialization of the corresponding clients and using reflection. */
final class ClientProviderTest {

  @Test
  void createBlockchainClient() {
    String baseUrl = "https://node1.testnet.partisiablockchain.com";
    int numberOfShards = 3;
    ClientProviderMain clientProvider = new ClientProviderMain();

    assertBlockchainClient(
        clientProvider.blockchainClient(baseUrl, numberOfShards), baseUrl, numberOfShards);
  }

  /** Can get api client with the specified baseurl. */
  @Test
  void createApiClient() {
    String baseUrl = "https://node1.testnet.partisiablockchain.com";
    ClientProviderMain clientProvider = new ClientProviderMain();
    ApiClient apiClient = clientProvider.apiClient(baseUrl);
    assertThat(apiClient.getBasePath()).isEqualTo(baseUrl);
  }

  @Test
  void createWebClient() {
    ClientProviderMain clientProvider = new ClientProviderMain();
    assertThat(clientProvider.webClient()).isNotNull();
    assertThat(clientProvider.webClient()).isInstanceOf(WebClient.class);
  }

  @Test
  void createZkRealClient() {
    String baseUrl = "https://node1.testnet.partisiablockchain.com";
    int numberOfShards = 3;
    ClientProviderMain clientProvider = new ClientProviderMain();
    assertThat(
            clientProvider.getZkClient(
                BlockchainAddress.fromString("033ba835f2348da55de96b41925523c2bfc77262aa"),
                clientProvider.blockchainClient(baseUrl, numberOfShards)))
        .isNotNull();
  }

  @Test
  void createBlockchainTransactionClient() {
    String baseUrl = "https://node1.testnet.partisiablockchain.com";
    int numberOfShards = 3;
    String privateKey = "aa";
    SenderAuthentication sender = SenderAuthenticationKeyPair.fromString(privateKey);

    ClientProviderMain clientProvider = new ClientProviderMain();
    BlockchainTransactionClient txClient =
        clientProvider.blockchainTransactionClient(baseUrl, numberOfShards, sender);

    assertThat(txClient).hasNoNullFieldsOrProperties();

    Field field =
        ExceptionConverter.call(() -> txClient.getClass().getDeclaredField("blockchainClient"));
    field.setAccessible(true);
    BlockchainClient blockchainClient =
        (BlockchainClient) ExceptionConverter.call(() -> field.get(txClient));

    assertBlockchainClient(blockchainClient, baseUrl, numberOfShards);
  }

  @Test
  void createBlockchainTransactionClientWithSpecifiedTransactionValidity() {
    String baseUrl = "https://node1.testnet.partisiablockchain.com";
    int numberOfShards = 3;
    String privateKey = "aa";
    SenderAuthentication sender = SenderAuthenticationKeyPair.fromString(privateKey);

    ClientProviderMain clientProvider = new ClientProviderMain();
    BlockchainTransactionClient txClient =
        clientProvider.blockchainTransactionClient(baseUrl, numberOfShards, sender, 191919);

    assertThat(txClient).hasNoNullFieldsOrProperties();

    Field field =
        ExceptionConverter.call(() -> txClient.getClass().getDeclaredField("blockchainClient"));
    field.setAccessible(true);
    BlockchainClient blockchainClient =
        (BlockchainClient) ExceptionConverter.call(() -> field.get(txClient));

    assertBlockchainClient(blockchainClient, baseUrl, numberOfShards);
  }

  private static void assertBlockchainClient(
      BlockchainClient blockchainClient, String baseUrl, int numberOfShards) {
    assertThat(blockchainClient).hasNoNullFieldsOrProperties();
    assertThat(blockchainClient).hasFieldOrPropertyWithValue("baseUrl", baseUrl);

    // Uses reflection to retrieve the Shardmanager of the client, to confirm that it uses 3 shards.
    Field field =
        ExceptionConverter.call(() -> blockchainClient.getClass().getDeclaredField("shardManager"));
    field.setAccessible(true);
    ShardManager shardManager =
        (ShardManager) ExceptionConverter.call(() -> field.get(blockchainClient));

    // Governance shard is added implicitly, so we expect 3+1 shards.
    assertThat(shardManager.getShards().size()).isEqualTo(numberOfShards + 1);
  }
}
