package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.OutputStreamAppender;
import com.partisiablockchain.client.transaction.ClientProviderRecordReplay;
import com.partisiablockchain.language.partisiacli.executionenvironment.ClientProvider;
import com.partisiablockchain.language.partisiacli.executionenvironment.ExecutionEnvironment;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import com.partisiablockchain.language.partisiacli.parser.Blockchain;
import com.partisiablockchain.language.recordreplay.BlockchainRecordReplay;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

/**
 * Testing framework responsible for ensuring the expected behaviour of running some command-line.
 * It achieves this by recording the output of a command-line to the resource folder of the project,
 * while checking for successful termination, as dictated by the given test's associated
 * exitcode.txt file. The recorded output is then matched against freshly generated output on
 * consecutive runs of the same command-line when {@link ExecutionReferenceTester#recording} is set
 * to false. Only the files kept in the given test's associated 'output' folder is asserted on by
 * this framework.
 */
public final class ExecutionReferenceTester {

  private static final String INPUT = "input";
  private static final String BLOCKCHAIN = "blockchain";
  private static final String PBC = "pbc";
  private static final String ID_PBC = "id_pbc";
  private static final String CONFIG_JSON = "config.json";
  private static final String STD_OUT = "std.out";
  private static final String STD_ERR = "std.err";
  private static final String OUTPUT = "output";
  private static final String COMMANDLINE = "commandline";
  private static final String SETUP = "setup";
  private static final String EXITCODE = "exitcode.txt";
  private static final String STREAM_APPENDER = "STREAM";
  private static final String CONSOLE_INPUT = "consoleinput.txt";

  /** Whether the resource tester is in recording mode. */
  private final boolean recording;

  /** The target folder to write output to. */
  private final Path targetTestFolder;

  /** The resource folder to store recorded output in. */
  private final Path resourceTestFolder;

  /**
   * Runs a command-line and records the output to the project resource folder to be checked for
   * equality in a consecutive run.
   *
   * @param testFolder The name of the folder matching the calling test class.
   * @param recording Whether to record or compare output.
   */
  public ExecutionReferenceTester(String testFolder, boolean recording) {
    this.recording = recording;
    this.resourceTestFolder = Paths.get(testFolder);
    String testParent = resourceTestFolder.getParent().getFileName().toString();
    this.targetTestFolder =
        Paths.get("target/%s/%s".formatted(testParent, resourceTestFolder.getFileName()));
  }

  /**
   * Runs a command-line given as an .sh-file stored in {@link
   * ExecutionReferenceTester#resourceTestFolder}. A run always prepares all folders involved in the
   * testing process except for the {@link ExecutionReferenceTester#resourceTestFolder}, as this is
   * where the command-line is parsed from. It then runs the given command-line and either copies or
   * compares the output, dictated by {@link ExecutionReferenceTester#recording}.
   */
  public void runReferenceTest() {
    Path tempFolder = prepareFolders(targetTestFolder);
    run(tempFolder);
    cleanTempFolder(tempFolder);
    if (recording) {
      cleanResourceFolder();
      copy(tempFolder);
    } else {
      compare(tempFolder);
    }
  }

  private Path prepareFolders(Path targetTestFolder) {
    // Prepare resource folder
    createDirectories(resourceTestFolder.resolve(INPUT));
    createDirectories(resourceTestFolder.resolve(OUTPUT));
    createDirectories(resourceTestFolder.resolve(BLOCKCHAIN));
    createDirectories(resourceTestFolder.resolve(PBC));

    // prepare target folder
    Path target = createDirectories(targetTestFolder);
    Path tempFolder =
        ExceptionConverter.call(
            () -> Files.createTempDirectory(target, ""), "Failed to create temp-directory");
    createDirectories(tempFolder.resolve(BLOCKCHAIN));
    createDirectories(tempFolder.resolve(INPUT));
    createDirectories(tempFolder.resolve(OUTPUT));
    createDirectories(tempFolder.resolve(PBC));

    prepareTempFolder(tempFolder);

    return tempFolder;
  }

  /**
   * Runs the command-line with a test-oriented {@link ExecutionEnvironment}. Loads the exitcode
   * associated with this test to determine expected execution behaviour.
   *
   * @param tempFolder The (temporary) working directory to output files to.
   */
  public void run(Path tempFolder) {
    Random random = new Random(1234L);
    ClientProvider clientProvider =
        new ClientProviderRecordReplay(
            new BlockchainRecordReplay(
                recording, resourceTestFolder.resolve(BLOCKCHAIN).toString(), "", 0),
            random);

    runPotentialSetup(clientProvider, tempFolder);

    String[] args = parseCommandLine(resourceTestFolder.resolve(COMMANDLINE));

    TestingTerminationStrategy terminationStrategy = new TestingTerminationStrategy();
    Path tempOutput = tempFolder.resolve(OUTPUT);

    Path stdOutPath = tempOutput.resolve(STD_OUT);
    Path stdErrPath = tempOutput.resolve(STD_ERR);

    CommandLine commandLine = new CommandLine(new Blockchain());
    PrintStream stdOutPrintStream = getPrintStream(stdOutPath);
    redirectLoggingToFile(stdOutPrintStream);
    commandLine.setColorScheme(CommandLine.Help.defaultColorScheme(CommandLine.Help.Ansi.OFF));
    FileManager fileManager = new FileManager(tempFolder);
    Main.runCommandLine(
        args,
        commandLine,
        new ExecutionEnvironment(
            clientProvider,
            stdOutPrintStream,
            getOverwriteInputStream(),
            getPrintStream(stdErrPath),
            terminationStrategy,
            fileManager,
            random,
            Path.of(tempFolder.resolve(PBC).toAbsolutePath() + "/config.json"),
            Path.of(tempFolder.resolve(PBC).toAbsolutePath() + "/id_pbc")));

    writeExitcode(terminationStrategy, tempOutput);
  }

  private static void writeExitcode(
      TestingTerminationStrategy terminationStrategy, Path tempOutput) {
    ExceptionConverter.run(
        () ->
            Files.writeString(
                tempOutput.resolve(EXITCODE), String.valueOf(terminationStrategy.getExitCode())),
        "Failed to write exit code %d to %s"
            .formatted(terminationStrategy.getExitCode(), tempOutput.resolve(EXITCODE)));
  }

  private static void redirectLoggingToFile(PrintStream printStream) {
    final Logger fileLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
    PatternLayoutEncoder ple = new PatternLayoutEncoder();

    ple.setPattern("%msg%n");
    ple.setContext(lc);
    ple.start();
    OutputStreamAppender<ILoggingEvent> streamAppender = new OutputStreamAppender<>();

    streamAppender.setName(STREAM_APPENDER);
    streamAppender.setContext(lc);
    streamAppender.setEncoder(ple);
    streamAppender.setOutputStream(printStream);
    streamAppender.start();
    fileLogger.addAppender(streamAppender);
  }

  private void cleanTempFolder(Path tempFolder) {
    List<Path> tempFiles = gatherFilesInFolder(tempFolder);
    removeSetupFiles(tempFiles);
    moveUnlabeledFilesToOutput(tempFiles, tempFolder);
    copyPbcFiles(tempFolder);
  }

  private void copyPbcFiles(Path tempFolder) {
    Path configPath = tempFolder.resolve(PBC);
    if (Files.exists(configPath)) {
      copyFilesToFolder(gatherFilesInFolder(configPath), tempFolder.resolve(OUTPUT), configPath);
    }
  }

  private void cleanResourceFolder() {
    deleteExistingBlockchainInteractions();
  }

  private void copy(Path tempFolder) {
    List<Path> filesToCopy = gatherFilesInFolder(tempFolder.resolve(OUTPUT));
    List<Path> blockchainRecordings = gatherFilesInFolder(tempFolder.resolve("blockchain"));
    copyFilesToFolder(filesToCopy, resourceTestFolder, tempFolder);
    copyFilesToFolder(blockchainRecordings, resourceTestFolder, tempFolder.resolve("blockchain"));
  }

  private void compare(Path tempFolder) {
    List<Path> expectedFiles = getPathsInFolder(resourceTestFolder.resolve(OUTPUT));
    List<Path> actualFiles = getPathsInFolder(tempFolder.resolve(OUTPUT));

    assertThat(expectedFiles.size())
        .withFailMessage(
            ("Mismatch in output folder sizes for test '%s'. "
                    + "There were %d recorded files but %d are being replayed.")
                .formatted(
                    resourceTestFolder.getFileName(), expectedFiles.size(), actualFiles.size()))
        .isEqualTo(actualFiles.size());

    // Helps to identify mismatch causes faster
    makeErrFirstFile(expectedFiles, actualFiles);

    for (int i = 0; i < expectedFiles.size(); i++) {
      final Path actualPath = actualFiles.get(i);
      final Path expectedPath = expectedFiles.get(i);
      final String fileName = actualPath.getFileName().toString();
      if (isTextualFile(fileName)) {
        assertThat(loadString(actualPath))
            .as("Comparing " + actualPath + " with " + expectedPath)
            .isEqualToNormalizingNewlines(loadString(expectedPath));
      } else {
        assertThat(loadBytes(actualPath))
            .as("Comparing " + actualPath + " with " + expectedPath)
            .isEqualTo(loadBytes(expectedPath));
      }
    }
  }

  /**
   * Runs a potential setup for a command-line, e.g. for sending a signed transaction the setup to
   * run will be the generation of the signed transaction to send. A setup command-line is
   * identified by a 'setup.sh' file's presence in the {@link
   * ExecutionReferenceTester#resourceTestFolder}. A setup command-line will output a corresponding
   * 'setup.bin' file, which is NOT checked for correctness.
   *
   * @param clientProvider A provider of clients.
   * @param tempFolder The path to the temp folder generated for this run.
   */
  private void runPotentialSetup(ClientProvider clientProvider, Path tempFolder) {
    List<Path> files = getPathsInFolder(resourceTestFolder);
    Optional<Path> setup =
        files.stream().filter(file -> file.getFileName().toString().contains(SETUP)).findFirst();
    if (setup.isPresent()) {
      String[] args = parseCommandLine(setup.get());
      TestingTerminationStrategy terminationStrategy = new TestingTerminationStrategy();
      CommandLine commandLine = new CommandLine(new Blockchain());
      commandLine.setColorScheme(CommandLine.Help.defaultColorScheme(CommandLine.Help.Ansi.OFF));
      FileManager fileManager = new FileManager(tempFolder);
      Main.runCommandLine(
          args,
          commandLine,
          new ExecutionEnvironment(
              clientProvider,
              System.out,
              getOverwriteInputStream(),
              System.err,
              terminationStrategy,
              fileManager,
              new Random(1234L),
              Path.of(tempFolder.resolve(PBC).toAbsolutePath() + "/config.json"),
              Path.of(tempFolder.resolve(PBC).toAbsolutePath() + "/id_pbc")));
      assertThat(terminationStrategy.terminationWasSuccessful()).isTrue();
    }
  }

  /**
   * Moves any generated file that sits at the root level of the temporary folder to the output
   * folder.
   */
  private void moveUnlabeledFilesToOutput(List<Path> tempFiles, Path tempFolder) {
    List<Path> rootLevelFiles =
        tempFiles.stream().filter(file -> tempFolder.relativize(file).getNameCount() == 1).toList();
    moveFilesToFolder(rootLevelFiles, tempFolder.resolve(OUTPUT), tempFolder);
  }

  /** Creates the given directory and any non-existent parent-directories. */
  private static Path createDirectories(Path directory) {
    return ExceptionConverter.call(
        () -> Files.createDirectories(directory),
        "Failed to create target-directory: " + directory);
  }

  private static PrintStream getPrintStream(Path path) {
    return ExceptionConverter.call(
        () -> new PrintStream(new FileOutputStream(path.toFile()), true),
        "Failed to create PrintStream with: " + path);
  }

  private InputStream getOverwriteInputStream() {
    Path inputePath = resourceTestFolder.resolve(INPUT + "/" + CONSOLE_INPUT);
    byte[] inputBytes;
    if (!Files.exists(inputePath)) {
      inputBytes = new byte[0];
    } else {
      inputBytes = loadBytes(inputePath);
    }
    return new ByteArrayInputStream(inputBytes);
  }

  private void deleteExistingBlockchainInteractions() {
    // Assumes no sub-directories inside blockchain folder
    Path blockchainFolder = resourceTestFolder.resolve(BLOCKCHAIN);
    File[] interactions = blockchainFolder.toFile().listFiles();
    for (File interaction : Objects.requireNonNull(interactions)) {
      boolean deleted = interaction.delete();
      if (!deleted) {
        throw new RuntimeException("Failed to delete %s".formatted(interaction));
      }
    }
  }

  /** Retrieves a list of all files in the provided folder. */
  private static List<Path> gatherFilesInFolder(Path folder) {
    List<Path> inputFiles;
    try (Stream<Path> path = Files.walk(folder).filter(Files::isRegularFile)) {
      inputFiles = path.toList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return new ArrayList<>(inputFiles);
  }

  /** Provides the temp folder with all the files it needs for executing the command. */
  private void prepareTempFolder(Path tempFolder) {
    List<Path> inputFiles = getPathsInFolder(resourceTestFolder.resolve(INPUT));
    inputFiles.addAll(getPathsInFolder(resourceTestFolder.resolve(PBC)));
    copyFilesToFolder(inputFiles, tempFolder, resourceTestFolder);
  }

  private boolean isTextualFile(String fileName) {
    return fileName.equals(STD_ERR)
        || fileName.equals(STD_OUT)
        || fileName.equals(EXITCODE)
        || fileName.equals(CONFIG_JSON)
        || fileName.endsWith(".ts")
        || fileName.endsWith(".java")
        || fileName.equals(ID_PBC)
        || fileName.endsWith(".bak");
  }

  private static void makeErrFirstFile(List<Path> expectedFiles, List<Path> actualFiles) {
    for (int i = 0; i < expectedFiles.size(); i++) {
      final Path file = actualFiles.get(i);
      if (file.getFileName().toString().equals(STD_ERR)) {
        Path errActual = actualFiles.remove(i);
        actualFiles.add(0, errActual);

        Path errExpected = expectedFiles.remove(i);
        expectedFiles.add(0, errExpected);
      }
    }
  }

  private void moveFilesToFolder(List<Path> files, Path target, Path relativeRoot) {
    for (Path file : files) {
      Path destination = resolveNesting(file, target, relativeRoot);
      moveFile(file, destination);
    }
  }

  private void moveFile(Path file, Path destination) {
    ExceptionConverter.run(
        () ->
            Files.move(
                file,
                destination,
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.ATOMIC_MOVE),
        "Failed to move %s to %s".formatted(file, destination));
  }

  private void removeSetupFiles(List<Path> files) {
    files.removeIf(file -> file.getFileName().toString().contains("setup"));
  }

  /**
   * Copies files to the target folder while respecting any potential nesting relative to the
   * provided root-folder, i.e. a file 'rootFolder/innerFolder/myFile.txt' will be copied to
   * 'target/innerFolder/myFile.txt'.
   */
  private void copyFilesToFolder(List<Path> tempFiles, Path target, Path relativeRoot) {
    for (Path file : tempFiles) {
      Path destination = resolveNesting(file, target, relativeRoot);
      copyFile(file, destination);
    }
  }

  private static void copyFile(Path file, Path destination) {
    ExceptionConverter.run(
        () ->
            Files.copy(
                file,
                destination,
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES),
        "Failed to copy %s to %s".formatted(file, destination));
  }

  private Path resolveNesting(Path file, Path target, Path relativeRoot) {
    Path relativePath = relativeRoot.relativize(file);
    return target.resolve(relativePath);
  }

  private List<Path> getPathsInFolder(Path folder) {
    List<Path> inputFiles;
    try (Stream<Path> path = Files.list(folder)) {
      inputFiles = new ArrayList<>(path.toList());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return inputFiles;
  }

  private String[] parseCommandLine(Path commandLine) {
    String cmdString = loadString(commandLine);
    String args = cmdString.substring("cargo pbc".length()).trim();

    /*
    [^"]     - token starting with something other than "
    \S*       - followed by zero or more non-space character
    or
    ".+?"   - a "-symbol followed by whatever, until another ".
     */
    List<String> argList = new ArrayList<>();
    Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(args);
    while (m.find()) {
      argList.add(m.group(1).replace("\"", ""));
    }

    return argList.toArray(new String[0]);
  }

  private static String loadString(Path filePath) {
    return ExceptionConverter.call(
        () -> Files.readString(filePath), "Failed to read file at: " + filePath);
  }

  private static byte[] loadBytes(Path filePath) {
    return ExceptionConverter.call(
        () -> Files.readAllBytes(filePath), "Failed to read bytes at: " + filePath);
  }
}
