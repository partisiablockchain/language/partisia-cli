package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

final class FileManagerTest {

  @Test
  void readPrivateKeyWithoutWhitespace() {
    FileManager fileManager = new FileManager(Paths.get(""));
    Path privateKeyPath = Paths.get("src/test/resources/myPrivateKey.txt");
    String privateKey =
        fileManager.readString(
            privateKeyPath, "Failed to read private key from: %s".formatted(privateKeyPath));
    assertThat(privateKey).isEqualTo("aa");
  }

  @Test
  void readPrivateKeyWithWhitespace() {
    FileManager fileManager = new FileManager(Paths.get(""));
    Path privateKeyPath = Paths.get("src/test/resources/myPrivateKeyWithWhitespace.txt");
    String privateKey =
        fileManager.readString(
            privateKeyPath, "Failed to read private key from: %s".formatted(privateKeyPath));
    assertThat(privateKey).isEqualTo("aa");
  }
}
