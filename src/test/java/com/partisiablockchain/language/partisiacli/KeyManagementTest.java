package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SenderAuthentication;
import com.partisiablockchain.language.partisiacli.account.create.Create;
import com.partisiablockchain.language.partisiacli.config.UserConfig;
import com.partisiablockchain.language.partisiacli.executionenvironment.FileManager;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class KeyManagementTest {

  private FileManager fileManager;

  @BeforeEach
  public void setUp() {
    fileManager = new FileManager(Paths.get(""));
  }

  // Feature: Get authentication from private key

  /**
   * The address for the authentication from a private key is the address for the account with that
   * private key.
   */
  @Test
  public void getAuthenticationWithPrivateKey() throws IOException {
    Path tempPrivateKeyFile = Files.createTempFile("privateKey", ".txt");
    String privateKeyHex = "bb";
    Files.writeString(tempPrivateKeyFile, privateKeyHex);
    UserConfig userConfig =
        new UserConfig(
            Paths.get("src/test/resources/myPrivateKey.txt"), "testnet", new HashMap<>());

    KeyManagement keyManagement =
        new KeyManagement(tempPrivateKeyFile, null, fileManager, userConfig);

    SenderAuthentication auth = keyManagement.getAuthentication();

    BlockchainAddress bbAddress =
        BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5");
    assertEquals(bbAddress, auth.getAddress());

    Files.deleteIfExists(tempPrivateKeyFile);
  }

  /**
   * When no wallet or private key override path is provided, KeyManagement uses the one from the
   * config.
   */
  @Test
  public void getAuthenticationNoOverrideProvided() {
    UserConfig userConfig =
        new UserConfig(
            Paths.get("src/test/resources/myPrivateKey.txt"), "mainnet", new HashMap<>());

    KeyManagement keyManagement = new KeyManagement(null, null, fileManager, userConfig);

    SenderAuthentication auth = keyManagement.getAuthentication();
    String aaAddress = "00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed";
    BlockchainAddress expectedAddress = BlockchainAddress.fromString(aaAddress);

    assertEquals(expectedAddress, auth.getAddress());
  }

  // Feature: Get authentication from wallet

  /**
   * The authentication gotten from a wallet has an address that matches the one for the account
   * derived from the wallet.
   */
  @Test
  public void getAuthenticationWithMnemonicPhrase() throws IOException {
    Path tempWalletFile = Files.createTempFile("wallet", "");
    String mnemonicPhrase =
        "position during practice pioneer job pause hamster ranch vibrant shrug velvet kangaroo";
    Files.writeString(tempWalletFile, mnemonicPhrase);
    UserConfig userConfig =
        new UserConfig(
            Paths.get("src/test/resources/myPrivateKey.txt"), "testnet", new HashMap<>());

    KeyManagement keyManagement = new KeyManagement(null, tempWalletFile, fileManager, userConfig);

    SenderAuthentication auth = keyManagement.getAuthentication();

    String defaultWalletAddress = "001453e847d35fea3649d0035489127ca494daffbc";
    BlockchainAddress expectedAddress = BlockchainAddress.fromString(defaultWalletAddress);
    assertEquals(expectedAddress, auth.getAddress());

    Files.deleteIfExists(tempWalletFile);
  }

  /**
   * If both wallet and private key overrides are supplied, the wallet is used for the
   * authentication.
   */
  @Test
  public void getAuthenticationBothOverridesProvided() throws IOException {
    Path tempPrivateKeyFile = Files.createTempFile("privateKey", ".txt");
    String privateKeyHex = "bb";
    Files.writeString(tempPrivateKeyFile, privateKeyHex);

    Path tempWalletFile = Files.createTempFile("wallet", "");
    String mnemonicPhrase =
        "position during practice pioneer job pause hamster ranch vibrant shrug velvet kangaroo";
    Files.writeString(tempWalletFile, mnemonicPhrase);

    UserConfig userConfig =
        new UserConfig(
            Paths.get("src/test/resources/myPrivateKey.txt"), "testnet", new HashMap<>());

    KeyManagement keyManagement =
        new KeyManagement(tempPrivateKeyFile, tempWalletFile, fileManager, userConfig);

    SenderAuthentication auth = keyManagement.getAuthentication();

    String defaultWalletAddress = "001453e847d35fea3649d0035489127ca494daffbc";
    BlockchainAddress expectedAddress = BlockchainAddress.fromString(defaultWalletAddress);
    assertEquals(expectedAddress, auth.getAddress());

    Files.deleteIfExists(tempWalletFile);
    Files.deleteIfExists(tempPrivateKeyFile);
  }

  // Feature: Output private key

  /** The default output destination for a private key is "account-address.pk". */
  @Test
  public void outputAccountPrivateKeyWithDefaultDestination() throws IOException {
    BigInteger privateKey =
        new BigInteger("0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef", 16);
    Create create = new Create();
    create.privateKeyFile = null;

    BlockchainAddress accountAddress =
        BlockchainAddress.fromString("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed");
    Path expectedFile = Paths.get(accountAddress.writeAsString() + ".pk");

    Files.deleteIfExists(expectedFile);

    KeyManagement.outputAccountPrivateKey(privateKey, create, accountAddress, fileManager);

    assertTrue(Files.exists(expectedFile));
    String writtenPrivateKeyHex = Files.readString(expectedFile);
    assertEquals(privateKey.toString(16), writtenPrivateKeyHex);

    Files.deleteIfExists(expectedFile);
  }

  /** If the user specifies an output destination, the private key is output at that destination. */
  @Test
  public void outputAccountPrivateKeyWithUserSpecifiedDestination() throws IOException {
    BigInteger privateKey =
        new BigInteger("fedcba9876543210fedcba9876543210fedcba9876543210fedcba9876543210", 16);
    Create create = new Create();
    Path userSpecifiedPath = Files.createTempFile("userPrivateKey", ".pk");
    create.privateKeyFile = userSpecifiedPath;

    BlockchainAddress accountAddress =
        BlockchainAddress.fromString("033ba835f2348da55de96b41925523c2bfc77262aa");
    Files.deleteIfExists(userSpecifiedPath);

    KeyManagement.outputAccountPrivateKey(privateKey, create, accountAddress, fileManager);

    assertTrue(Files.exists(userSpecifiedPath));
    String writtenPrivateKeyHex = Files.readString(userSpecifiedPath);
    assertEquals(privateKey.toString(16), writtenPrivateKeyHex);

    Files.deleteIfExists(userSpecifiedPath);
  }
}
