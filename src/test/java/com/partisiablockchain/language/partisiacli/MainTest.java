package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.client.transaction.Transaction;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;

final class MainTest {

  /**
   * Builds a deploy transaction for a token contract and compares it against a prebuilt equivalent
   * transaction.
   */
  @Test
  void testMain() {
    Path targetFolder = Paths.get("target/main-test");

    String pbcPath = "src/test/resources/main-test/input/token_contract_SDK_13.0.0.pbc";

    // prepare working directory
    Path directory =
        ExceptionConverter.call(
            () -> Files.createDirectories(targetFolder),
            "Failed to create target folder: " + targetFolder);

    Path tempDirectory =
        ExceptionConverter.call(
            () -> Files.createTempDirectory(directory, ""), "Failed to create temp-directory");

    Path output = tempDirectory.resolve("tx.bin");
    String[] args =
        new String[] {
          "transaction", "deploy", "--build", output.toString(), pbcPath, "hej", "42", "2", "4660"
        };
    Main.main(args);

    Path prebuiltTxPath = Paths.get("src/test/resources/main-test/input/tx.bin");

    Transaction expected = loadTransaction(prebuiltTxPath);
    Transaction actual = loadTransaction(output);
    assertThat(actual).isEqualTo(expected);
  }

  private static Transaction loadTransaction(Path txPath) {
    byte[] txBytes =
        ExceptionConverter.call(
            () -> Files.readAllBytes(txPath), "Failed to read transaction at: " + txPath);
    return Transaction.read(SafeDataInputStream.createFromBytes(txBytes));
  }
}
