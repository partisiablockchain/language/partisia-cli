package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

/**
 * Ensures the requirements of the peeking and iteration functionality of the iterator. Removal is
 * not supported.
 */
final class PeekingIteratorTest {

  private static final List<Integer> threeElementList = List.of(1, 2, 3);
  private static final List<Integer> singleElementList = List.of(1);
  private static final List<Integer> emptyList = List.of();

  /** A finished iterator has no more elements. */
  @Test
  void finishedIteratorHasNoMoreElements() {
    PeekingIterator<Integer> iter = createIteratorFromList(emptyList);
    assertThat(iter.hasNext()).isFalse();
  }

  /** An unfinished iterator has more elements. */
  @Test
  void unfinishedIteratorHasMoreElements() {
    PeekingIterator<Integer> iter = createIteratorFromList(singleElementList);
    assertThat(iter.hasNext()).isTrue();
  }

  /** Retrieving the next element follows the given sequence. */
  @Test
  void elementsAreRetrievedInOrder() {
    PeekingIterator<Integer> sequentialIter = createIteratorFromList(threeElementList);

    assertThat(sequentialIter.next()).isEqualTo(1);
    assertThat(sequentialIter.next()).isEqualTo(2);
    assertThat(sequentialIter.next()).isEqualTo(3);
  }

  /** Iteration should process all elements. */
  @Test
  void iterationEncountersAllElements() {
    PeekingIterator<Integer> threeElementIter = createIteratorFromList(threeElementList);
    assertThat(threeElementList.size()).isEqualTo(3);
    for (int i = 0; i < threeElementList.size(); i++) {
      threeElementIter.next();
    }
    assertThat(threeElementIter.hasNext()).isFalse();
  }

  /**
   * Getting the next element when iteration is finished throws a {@link NoSuchElementException}.
   */
  @Test
  void advancingFinishedIterationThrowsError() {
    PeekingIterator<Integer> iter = createIteratorFromList(emptyList);
    assertThatThrownBy(iter::next).isInstanceOf(NoSuchElementException.class);
  }

  /** Peeking does not advance the iteration. */
  @Test
  void peekingRetainsIterationState() {
    int item = 1;
    PeekingIterator<Integer> singleElementIter = createIteratorFromList(singleElementList);
    Integer peekedElement = singleElementIter.peek();
    assertThat(singleElementIter.hasNext()).isTrue();
    assertThat(peekedElement).isEqualTo(item);

    Integer nextItem = singleElementIter.next();
    assertThat(singleElementIter.hasNext()).isFalse();

    assertThat(peekedElement).isEqualTo(nextItem);
  }

  /** Peeking on a finished iterator throws a {@link NoSuchElementException}. */
  @Test
  void peekingWhenFinishedThrowsError() {
    PeekingIterator<Integer> iter = createIteratorFromList(emptyList);
    assertThat(iter.hasNext()).isFalse();
    assertThatThrownBy(iter::peek).isInstanceOf(NoSuchElementException.class);
  }

  /** Null-elements are valid elements. As such they do not halt iteration. */
  @Test
  void nullElementsAreValid() {
    PeekingIterator<Object> iter = new PeekingIterator<>(Stream.of((Object) null).iterator());
    assertThat(iter.hasNext()).isTrue();
    assertThat(iter.peek()).isNull();
    assertThat(iter.next()).isNull();
    assertThat(iter.hasNext()).isFalse();
  }

  /** Provided iterator has to be an implementation of {@link java.util.Iterator}. */
  @Test
  void providingNullIteratorIsNotAllowed() {
    assertThatThrownBy(() -> new PeekingIterator<>(null)).isInstanceOf(NullPointerException.class);
  }

  /** Removal is not a supported feature. */
  @Test
  void removeIsUnsupported() {
    PeekingIterator<Integer> singleElementIter = createIteratorFromList(singleElementList);
    assertThatThrownBy(singleElementIter::remove).isInstanceOf(UnsupportedOperationException.class);

    singleElementIter.next();
    assertThatThrownBy(singleElementIter::remove).isInstanceOf(UnsupportedOperationException.class);
  }

  /** Iteration via forEachRemaining is not a supported feature. */
  @Test
  void forEachRemainingIsUnsupported() {
    PeekingIterator<Integer> iter = createIteratorFromList(threeElementList);
    assertThatThrownBy(() -> iter.forEachRemaining((element) -> {}))
        .isInstanceOf(UnsupportedOperationException.class);
  }

  private PeekingIterator<Integer> createIteratorFromList(List<Integer> list) {
    return new PeekingIterator<>(list.listIterator());
  }
}
