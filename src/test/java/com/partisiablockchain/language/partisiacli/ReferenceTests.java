package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.util.List;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Uses the {@link ExecutionReferenceTester} framework to run a series of command-lines to check
 * that they behave as expected.
 */
final class ReferenceTests {

  /** Set to true to record interaction with testnet. */
  private static final boolean TEST_REF_REGEN = false;

  @Test
  void regenIsFalse() {
    Assertions.assertThat(TEST_REF_REGEN).isFalse();
  }

  @ParameterizedTest
  @MethodSource("helpTestFolders")
  void runHelpTests(String helpTestPath) {
    runReferenceTest(helpTestPath);
  }

  private static List<String> helpTestFolders() {
    return getTestFolders("help-messages");
  }

  @ParameterizedTest
  @MethodSource("getReferenceTestFolders")
  void runReferenceTests(String testPath) {
    runReferenceTest(testPath);
  }

  private static List<String> getReferenceTestFolders() {
    return getTestFolders("cli-execution-reference-tests");
  }

  private static void runReferenceTest(String testFolder) {
    try {
      ExecutionReferenceTester executionReferenceTester =
          new ExecutionReferenceTester(testFolder, false);
      executionReferenceTester.runReferenceTest();
    } catch (Throwable e) {
      if (TEST_REF_REGEN) {
        ExecutionReferenceTester executionReferenceTester =
            new ExecutionReferenceTester(testFolder, TEST_REF_REGEN);
        executionReferenceTester.runReferenceTest();
        System.out.printf(
            "Re-recorded test: %s. Remember to validate new recording.%n", testFolder);
      } else {
        throw new RuntimeException(String.format("Test %s failed.", testFolder), e);
      }
    }
  }

  private static List<String> getTestFolders(String testName) {
    Path resourceTests = Paths.get("src/test/resources/reference-test/%s".formatted(testName));
    // Delete any empty folders
    ExceptionConverter.run(
        () ->
            Files.walkFileTree(
                resourceTests,
                new SimpleFileVisitor<>() {
                  @Override
                  public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                    dir.toFile().delete();
                    return FileVisitResult.CONTINUE;
                  }
                }));

    List<String> inputFiles;
    try (Stream<Path> path = Files.list(resourceTests)) {
      Stream<String> directories = path.map(file -> file.toString());
      inputFiles = directories.toList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return inputFiles;
  }
}
