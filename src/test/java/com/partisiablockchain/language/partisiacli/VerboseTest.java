package com.partisiablockchain.language.partisiacli;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.partisiablockchain.language.partisiacli.parser.Blockchain;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

/** Circumvent pipeline issues caused by using Picocli annotations regarding the verbose-option. */
final class VerboseTest {

  /**
   * The ROOT logger is OFF by default. Providing the verbose flag on the command-line enables it.
   * The boolean is parsed and passed to the function at run time.
   */
  @Test
  void providingVerboseFlagEnablesLogger() {
    Logger rootLogger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    rootLogger.setLevel(Level.INFO);
    new Blockchain().setVerbose();
    assertThat(rootLogger.getLevel()).isEqualTo(Level.OFF);
  }
}
