package com.partisiablockchain.language.partisiacli.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.partisiacli.BlockchainUrls;
import com.partisiablockchain.language.partisiacli.RestAndWebUrls;
import org.junit.jupiter.api.Test;

final class RestAndWebUrlsConverterTest {

  @Test
  void singleArg() throws Exception {
    String url = "http://localhost:8000";
    RestAndWebUrls urls = new RestAndWebUrlsConverter().convert(url);
    assertThat(urls.restBaseUrl()).isEqualTo(url);
    assertThat(urls.webBaseUrl()).isEqualTo(BlockchainUrls.NO_URL);
  }

  @Test
  void twoUrls() throws Exception {
    String url1 = "http://localhost:8000";
    String url2 = "http://localhost:8300";
    RestAndWebUrls urls = new RestAndWebUrlsConverter().convert("%s,%s".formatted(url1, url2));
    assertThat(urls.restBaseUrl()).isEqualTo(url1);
    assertThat(urls.webBaseUrl()).isEqualTo(url2);
  }

  @Test
  void notAnUrl() {
    assertThatThrownBy(() -> new RestAndWebUrlsConverter().convert("not-an-url123"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Argument is not an URL.");
  }

  @Test
  void threeArgs() {
    String url = "http://localhost:8000";
    assertThatThrownBy(
            () -> new RestAndWebUrlsConverter().convert("%s,%s,%s".formatted(url, url, url)))
        .isInstanceOf(Exception.class)
        .hasMessage("More than 2 arguments provided");
  }
}
