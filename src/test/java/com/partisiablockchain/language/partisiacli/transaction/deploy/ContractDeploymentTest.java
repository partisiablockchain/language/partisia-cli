package com.partisiablockchain.language.partisiacli.transaction.deploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.abimodel.model.AbiVersion;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Map;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

/**
 * Testing contract deployment command determining which binder id to use for deployment
 * transaction.
 */
public final class ContractDeploymentTest {

  private static final Map<Integer, ContractDeployment.VersionInterval> binderMap =
      Map.of(
          1,
          new ContractDeployment.VersionInterval(new AbiVersion(2, 0, 0), new AbiVersion(2, 0, 0)),
          2,
          new ContractDeployment.VersionInterval(new AbiVersion(3, 1, 0), new AbiVersion(3, 4, 0)));

  /**
   * Returns the binder id of a binder if its version interval includes the given binder version.
   */
  @Test
  void findBinderId() {
    int binderId = ContractDeployment.determineBinderId(binderMap, new AbiVersion(2, 0, 0));
    assertThat(binderId).isEqualTo(1);
  }

  /** Different binder versions can give same binder id if it supports a range of versions. */
  @Test
  void multipleBinderVersionsGiveSameId() {
    int binderId1 = ContractDeployment.determineBinderId(binderMap, new AbiVersion(3, 1, 0));
    int binderId2 = ContractDeployment.determineBinderId(binderMap, new AbiVersion(3, 3, 0));
    assertThat(binderId1).isEqualTo(2);
    assertThat(binderId2).isEqualTo(2);
  }

  /** If the binder version is not supported by any binders, it returns -1. */
  @Test
  void cannotFindBinderId() {
    int binderId = ContractDeployment.determineBinderId(binderMap, new AbiVersion(1, 0, 0));
    assertThat(binderId).isEqualTo(-1);
    binderId = ContractDeployment.determineBinderId(binderMap, new AbiVersion(2, 1, 0));
    assertThat(binderId).isEqualTo(-1);
    binderId = ContractDeployment.determineBinderId(binderMap, new AbiVersion(3, 0, 0));
    assertThat(binderId).isEqualTo(-1);
    binderId = ContractDeployment.determineBinderId(binderMap, new AbiVersion(4, 0, 0));
    assertThat(binderId).isEqualTo(-1);
  }

  /** Can find the supported binder version from the public deploy-contract state and abi. */
  @Test
  void findPubVersions() {
    byte[] state = readStateFile("pubdeploy-state.txt");
    ContractAbi abi = readAbiFile("pubdeploy-abi.abi");
    Map<Integer, ContractDeployment.VersionInterval> supportedBinderVersions =
        ContractDeployment.getSupportedBinderVersions(state, abi);
    Map<Integer, ContractDeployment.VersionInterval> expected =
        Map.of(
            1,
            new ContractDeployment.VersionInterval(
                new AbiVersion(7, 0, 0), new AbiVersion(9, 4, 0)),
            2,
            intervalFromVersion(new AbiVersion(9, 5, 0)),
            3,
            intervalFromVersion(new AbiVersion(10, 1, 0)),
            4,
            intervalFromVersion(new AbiVersion(10, 2, 0)));
    assertThat(supportedBinderVersions).isEqualTo(expected);
  }

  /** Can find the supported binder version from the REAL deploy-contract state and abi. */
  @Test
  void findRealVersions() {
    byte[] state = readStateFile("realdeploy-state.txt");
    ContractAbi abi = readAbiFile("realdeploy-abi.abi");
    Map<Integer, ContractDeployment.VersionInterval> supportedBinderVersions =
        ContractDeployment.getSupportedBinderVersions(state, abi);
    Map<Integer, ContractDeployment.VersionInterval> expected =
        Map.of(
            1,
            new ContractDeployment.VersionInterval(
                new AbiVersion(7, 0, 0), new AbiVersion(9, 4, 0)),
            2,
            intervalFromVersion(new AbiVersion(10, 0, 0)),
            3,
            intervalFromVersion(new AbiVersion(10, 1, 0)),
            4,
            intervalFromVersion(new AbiVersion(10, 3, 0)),
            5,
            intervalFromVersion(new AbiVersion(11, 0, 0)),
            6,
            intervalFromVersion(new AbiVersion(11, 1, 0)),
            7,
            intervalFromVersion(new AbiVersion(11, 2, 0)),
            8,
            intervalFromVersion(new AbiVersion(11, 3, 0)),
            9,
            intervalFromVersion(new AbiVersion(11, 4, 0)));
    assertThat(supportedBinderVersions).isEqualTo(expected);
  }

  private ContractDeployment.VersionInterval intervalFromVersion(AbiVersion version) {
    return new ContractDeployment.VersionInterval(version, version);
  }

  private ContractAbi readAbiFile(String fileName) {
    return new AbiParser(
            ExceptionConverter.call(
                () -> ContractDeploymentTest.class.getResourceAsStream(fileName).readAllBytes()))
        .parseAbi()
        .contract();
  }

  private byte[] readStateFile(String fileName) {
    return ExceptionConverter.call(
        () ->
            Base64.decode(
                ContractDeploymentTest.class.getResourceAsStream(fileName).readAllBytes()));
  }
}
