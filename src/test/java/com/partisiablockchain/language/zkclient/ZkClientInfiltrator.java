package com.partisiablockchain.language.zkclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.web.WebClient;
import java.util.Random;

/** Access the package private ZkClient constructor to inject randomness. */
public final class ZkClientInfiltrator {

  /**
   * Create a new RealZkClient with a specified randomness.
   *
   * @param contract the contract to connect to.
   * @param webClient the webclient to communicate with the Real nodes.
   * @param blockchainClient the blockchain client to send transactions with.
   * @param rng the randomness generator.
   * @return the RealZkClient with specified randomness.
   */
  public static RealZkClient createForTest(
      BlockchainAddress contract,
      WebClient webClient,
      BlockchainClient blockchainClient,
      Random rng) {
    return RealZkClient.createForTest(contract, webClient, blockchainClient, rng);
  }
}
