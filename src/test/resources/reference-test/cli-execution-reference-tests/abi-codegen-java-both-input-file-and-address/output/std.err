You must provide one of the following: an abi file, a pbc file or a contract address.Additionally, you must provide a path to the output file.
Usage: cargo pbc abi codegen [-hv] [--deserialize-rpc] [--dont-annotate]
                             [--dont-serialize-actions]
                             [--dont-serialize-state] [--lenient]
                             [--serialize-callbacks]
                             [--contract=<contract-address>] [--net=<netname>]
                             [--package=<package>]
                             [--deserialize=<deserializeNamedTypes>]... (--java
                             | --ts) <abi-or-pbc-path,output-path> or
                             <output-path>...
Generate code to interact with a contract based on an abi.
      <abi-or-pbc-path,output-path> or <output-path>...
                            Path to ABI or PBC file and path to output the
                              generated code, or just output path if contract
                              address is provided.
      --contract=<contract-address>
                            Generate code for the contract deployed at the
                              given contract address.
      --deserialize=<deserializeNamedTypes>
                            Generate code for deserializing the given named
                              types.
      --deserialize-rpc     Generate code for deserializing rpc.
      --dont-annotate       Do not generate the '@AbiGenerated' annotation.
      --dont-serialize-actions
                            Do not generate code for serializing actions.
      --dont-serialize-state
                            Do not generate code for deserializing contract
                              state.
  -h, --help                Print usage description of the command.
      --java                Generate java code.
      --lenient             Allow invalid Java identifiers.
      --net=<netname>       The blockchain net to target. To see all named
                              nets, run "cargo pbc config net -l".
                            "mainnet"  Target the mainnet
                            "testnet"  Target the testnet
                            <reader-url>  Target a custom net (with no browser)
                            <reader-url>,<browser-url>  Target a custom net
                              with a custom browser.
      --package=<package>   Generate the code in the given package.
      --serialize-callbacks Generate code for serializing callbacks.
      --ts                  Generate typescript code.
  -v, --verbose             Print all available information. Default is to
                              print minimum information.
