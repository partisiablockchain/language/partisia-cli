// This file is auto-generated from an abi-file using AbiCodegen.
package null;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class voting {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public voting(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private U8Struct deserializeU8Struct(AbiInput _input) {
    byte u8Value = _input.readU8();
    var u8Vector_vecLength = _input.readI32();
    byte[] u8Vector = _input.readBytes(u8Vector_vecLength);
    var u8Struct_vecLength = _input.readI32();
    List<U8Struct> u8Struct = new ArrayList<>();
    for (int u8Struct_i = 0; u8Struct_i < u8Struct_vecLength; u8Struct_i++) {
      U8Struct u8Struct_elem = deserializeU8Struct(_input);
      u8Struct.add(u8Struct_elem);
    }
    return new U8Struct(u8Value, u8Vector, u8Struct);
  }
  private State deserializeState(AbiInput _input) {
    long someU64 = _input.readU64();
    String someString = _input.readString();
    boolean someBool = _input.readBoolean();
    U8Struct someU8Struct = deserializeU8Struct(_input);
    return new State(someU64, someString, someBool, someU8Struct);
  }
  public State getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeState(input);
  }


  @AbiGenerated
  public record U8Struct(byte u8Value, byte[] u8Vector, List<U8Struct> u8Struct) {
    public void serialize(AbiOutput _out) {
      _out.writeU8(u8Value);
      _out.writeI32(u8Vector.length);
      _out.writeBytes(u8Vector);
      _out.writeI32(u8Struct.size());
      for (U8Struct u8Struct_vec : u8Struct) {
        u8Struct_vec.serialize(_out);
      }
    }
  }

  @AbiGenerated
  public record State(long someU64, String someString, boolean someBool, U8Struct someU8Struct) {
    public static State deserialize(byte[] bytes) {
      return voting.deserializeState(bytes);
    }
  }

  public static byte[] initialize() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
    });
  }

  public static byte[] updateState(long someU64, String someString, boolean someBool, U8Struct someU8Struct) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("01"));
      _out.writeU64(someU64);
      _out.writeString(someString);
      _out.writeBoolean(someBool);
      someU8Struct.serialize(_out);
    });
  }

  public static byte[] provideU64() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] provideString() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] provideBool() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] provideU8Struct() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] provideU64WithCourier(BlockchainAddress address) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("06"));
      _out.writeAddress(address);
    });
  }

  public static State deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new voting(client, address).deserializeState(input);
  }
  
  public static State deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static State deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

}
