// This file is auto-generated from an abi-file using AbiCodegen.
package null;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class zk_statistics {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public zk_statistics(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private StatisticsContractState deserializeStatisticsContractState(AbiInput _input) {
    long deadline = _input.readI64();
    StatisticsResult result = null;
    var result_isSome = _input.readBoolean();
    if (result_isSome) {
      StatisticsResult result_option = deserializeStatisticsResult(_input);
      result = result_option;
    }
    return new StatisticsContractState(deadline, result);
  }
  private StatisticsResult deserializeStatisticsResult(AbiInput _input) {
    AgeCounts ageCounts = deserializeAgeCounts(_input);
    GenderCounts genderCounts = deserializeGenderCounts(_input);
    ColorCounts colorCounts = deserializeColorCounts(_input);
    return new StatisticsResult(ageCounts, genderCounts, colorCounts);
  }
  private AgeCounts deserializeAgeCounts(AbiInput _input) {
    int age0to19 = _input.readI32();
    int age20to39 = _input.readI32();
    int age40to59 = _input.readI32();
    int age60plus = _input.readI32();
    return new AgeCounts(age0to19, age20to39, age40to59, age60plus);
  }
  private GenderCounts deserializeGenderCounts(AbiInput _input) {
    int male = _input.readI32();
    int female = _input.readI32();
    int other = _input.readI32();
    return new GenderCounts(male, female, other);
  }
  private ColorCounts deserializeColorCounts(AbiInput _input) {
    int red = _input.readI32();
    int blue = _input.readI32();
    int green = _input.readI32();
    int yellow = _input.readI32();
    return new ColorCounts(red, blue, green, yellow);
  }
  public StatisticsContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeStatisticsContractState(input);
  }


  @AbiGenerated
  public record StatisticsContractState(long deadline, StatisticsResult result) {
    public static StatisticsContractState deserialize(byte[] bytes) {
      return zk_statistics.deserializeState(bytes);
    }
  }

  @AbiGenerated
  public record StatisticsResult(AgeCounts ageCounts, GenderCounts genderCounts, ColorCounts colorCounts) {
  }

  @AbiGenerated
  public record AgeCounts(int age0to19, int age20to39, int age40to59, int age60plus) {
  }

  @AbiGenerated
  public record GenderCounts(int male, int female, int other) {
  }

  @AbiGenerated
  public record ColorCounts(int red, int blue, int green, int yellow) {
  }

  @AbiGenerated
  public record EntryPoint(byte ageChoice, byte genderChoice, byte colorChoice) {
    public void serialize(AbiOutput _out) {
      _out.writeI8(ageChoice);
      _out.writeI8(genderChoice);
      _out.writeI8(colorChoice);
    }
  }

  public static byte[] initialize(long millisUntilDeadline) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU64(millisUntilDeadline);
    });
  }

  public static byte[] computeStatistics() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static SecretInputBuilder<EntryPoint> addData() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
    Function<EntryPoint, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static StatisticsContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new zk_statistics(client, address).deserializeStatisticsContractState(input);
  }
  
  public static StatisticsContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static StatisticsContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

}
