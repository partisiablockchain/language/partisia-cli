// This file is auto-generated from an abi-file using AbiCodegen.
package null;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.language.abistreams.AbiBitOutput;
import com.partisiablockchain.language.abistreams.AbiByteInput;
import com.partisiablockchain.language.abistreams.AbiByteOutput;
import com.partisiablockchain.language.abistreams.AbiInput;
import com.partisiablockchain.language.abistreams.AbiOutput;
import com.partisiablockchain.language.codegenlib.AbiGenerated;
import com.partisiablockchain.language.codegenlib.AvlTreeMap;
import com.partisiablockchain.language.codegenlib.BlockchainStateClient;
import com.partisiablockchain.language.codegenlib.SecretInputBuilder;
import com.partisiablockchain.language.codegenlib.StateWithClient;
import com.secata.stream.CompactBitArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
@AbiGenerated
public final class zk_actions {
  private final BlockchainStateClient _client;
  private final BlockchainAddress _address;
  
  public zk_actions(
      BlockchainStateClient client,
      BlockchainAddress address) {
    this._address = address;
    this._client = client;
  }
  private ContractState deserializeContractState(AbiInput _input) {
    long ownVariableId = _input.readU64();
    return new ContractState(ownVariableId);
  }
  public ContractState getState() {
    byte[] bytes = _client.getContractStateBinary(_address);
    var input = AbiByteInput.createLittleEndian(bytes);
    return deserializeContractState(input);
  }


  @AbiGenerated
  public record LotsOfSecretData(BigInteger v1, BigInteger v2, BigInteger v3, BigInteger v4, BigInteger v5, BigInteger v6, BigInteger v7, BigInteger v8) {
    public void serialize(AbiOutput _out) {
      _out.writeSignedBigInteger(v1, 16);
      _out.writeSignedBigInteger(v2, 16);
      _out.writeSignedBigInteger(v3, 16);
      _out.writeSignedBigInteger(v4, 16);
      _out.writeSignedBigInteger(v5, 16);
      _out.writeSignedBigInteger(v6, 16);
      _out.writeSignedBigInteger(v7, 16);
      _out.writeSignedBigInteger(v8, 16);
    }
  }

  @AbiGenerated
  public record IntPair(int v1, int v2) {
    public void serialize(AbiOutput _out) {
      _out.writeI32(v1);
      _out.writeI32(v2);
    }
  }

  @AbiGenerated
  public record ContractState(long ownVariableId) {
    public static ContractState deserialize(byte[] bytes) {
      return zk_actions.deserializeState(bytes);
    }
  }

  public static byte[] initialize(byte mode) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("ffffffff0f"));
      _out.writeU8(mode);
    });
  }

  public static byte[] someAction() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("00"));
    });
  }

  public static byte[] toContractDone() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("01"));
    });
  }

  public static byte[] startComputationOneOutput() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("02"));
    });
  }

  public static byte[] startComputationNoOutputs() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0b"));
    });
  }

  public static byte[] startComputationTwoOutputs() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0c"));
    });
  }

  public static byte[] newComputation() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("03"));
    });
  }

  public static byte[] outputCompleteDeleteVariable() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("04"));
    });
  }

  public static byte[] contractDone() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("05"));
    });
  }

  public static byte[] transferVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("06"));
    });
  }

  public static byte[] deleteVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("07"));
    });
  }

  public static byte[] deletePendingInputOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0f"));
    });
  }

  public static byte[] openVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("08"));
    });
  }

  public static byte[] deleteVariableThree() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("09"));
    });
  }

  public static byte[] verifyOnePendingInput() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0a"));
    });
  }

  public static byte[] verifyOneVariable() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("10"));
    });
  }

  public static byte[] updateState(long currentState, long nextState) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("0d"));
      _out.writeU64(currentState);
      _out.writeU64(nextState);
    });
  }

  public static byte[] attestData(byte[] dataToAttest) {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("11"));
      _out.writeI32(dataToAttest.length);
      _out.writeBytes(dataToAttest);
    });
  }

  public static byte[] startComputationDeleteVariableOne() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("12"));
    });
  }

  public static byte[] openVariableTwo() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("13"));
    });
  }

  public static byte[] openVariableOneAndTwo() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("14"));
    });
  }

  public static byte[] deleteVariableOneAndTwo() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeU8(0x09);
      _out.writeBytes(HexFormat.of().parseHex("15"));
    });
  }

  public static SecretInputBuilder<Short> secretInputBitLength10() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("60"));
    });
    Function<Short, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeI16(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Boolean> secretInputBitLength1() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("62"));
    });
    Function<Boolean, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeBoolean(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<LotsOfSecretData> secretInputBitLength1001x1() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("63"));
    });
    Function<LotsOfSecretData, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<IntPair> secretInputBitLength3232() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("65"));
    });
    Function<IntPair, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      secret_input_lambda.serialize(_out);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static SecretInputBuilder<Boolean> secretInputSealedSingleBitWithInfo() {
    byte[] _publicRpc = AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("69"));
    });
    Function<Boolean, CompactBitArray> _secretInput = (secret_input_lambda) -> AbiBitOutput.serialize(_out -> {
      _out.writeBoolean(secret_input_lambda);
    });
    return new SecretInputBuilder<>(_publicRpc, _secretInput);
  }

  public static byte[] onCallback() {
    return AbiByteOutput.serializeBigEndian(_out -> {
      _out.writeBytes(HexFormat.of().parseHex("40"));
    });
  }

  public static ContractState deserializeState(
      byte[] bytes,
      BlockchainStateClient client,
      BlockchainAddress address) {
    var input = AbiByteInput.createLittleEndian(bytes);
    return new zk_actions(client, address).deserializeContractState(input);
  }
  
  public static ContractState deserializeState(byte[] bytes) {
    return deserializeState(bytes, null, null);
  }
  
  public static ContractState deserializeState(StateWithClient state) {
    return deserializeState(state.bytes(), state.client(), state.address());
  }

}
