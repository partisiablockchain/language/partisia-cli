// This file is auto-generated from an abi-file using AbiCodegen.
/* eslint-disable */
// @ts-nocheck
// noinspection ES6UnusedImports
import {
  AbiBitInput,
  AbiBitOutput,
  AbiByteInput,
  AbiByteOutput,
  AbiInput,
  AbiOutput,
  AvlTreeMap,
  BlockchainAddress,
  BlockchainPublicKey,
  BlockchainStateClient,
  BlsPublicKey,
  BlsSignature,
  BN,
  Hash,
  Signature,
  StateWithClient,
  SecretInputBuilder,
} from "@partisiablockchain/abi-client";

type Option<K> = K | undefined;
export class voting {
  private readonly _client: BlockchainStateClient | undefined;
  private readonly _address: BlockchainAddress | undefined;
  
  public constructor(
    client: BlockchainStateClient | undefined,
    address: BlockchainAddress | undefined
  ) {
    this._address = address;
    this._client = client;
  }
  public deserializeVoteState(_input: AbiInput): VoteState {
    const proposalId: BN = _input.readU64();
    const voters_setLength = _input.readI32();
    const voters: BlockchainAddress[] = [];
    for (let voters_i = 0; voters_i < voters_setLength; voters_i++) {
      const voters_elem: BlockchainAddress = _input.readAddress();
      voters.push(voters_elem);
    }
    const deadlineUtcMillis: BN = _input.readI64();
    const votes_mapLength = _input.readI32();
    const votes: Map<BlockchainAddress, boolean> = new Map();
    for (let votes_i = 0; votes_i < votes_mapLength; votes_i++) {
      const votes_key: BlockchainAddress = _input.readAddress();
      const votes_value: boolean = _input.readBoolean();
      votes.set(votes_key, votes_value);
    }
    let result: Option<boolean> = undefined;
    const result_isSome = _input.readBoolean();
    if (result_isSome) {
      const result_option: boolean = _input.readBoolean();
      result = result_option;
    }
    return { proposalId, voters, deadlineUtcMillis, votes, result };
  }
  public async getState(): Promise<VoteState> {
    const bytes = await this._client?.getContractStateBinary(this._address!);
    if (bytes === undefined) {
      throw new Error("Unable to get state bytes");
    }
    const input = AbiByteInput.createLittleEndian(bytes);
    return this.deserializeVoteState(input);
  }

}
export interface VoteState {
  proposalId: BN;
  voters: BlockchainAddress[];
  deadlineUtcMillis: BN;
  votes: Map<BlockchainAddress, boolean>;
  result: Option<boolean>;
}

export function initialize(proposalId: BN, voters: BlockchainAddress[], deadlineUtcMillis: BN): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("ffffffff0f", "hex"));
    _out.writeU64(proposalId);
    _out.writeI32(voters.length);
    for (const voters_vec of voters) {
      _out.writeAddress(voters_vec);
    }
    _out.writeI64(deadlineUtcMillis);
  });
}

export function vote(vote: boolean): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("01", "hex"));
    _out.writeBoolean(vote);
  });
}

export function count(): Buffer {
  return AbiByteOutput.serializeBigEndian((_out) => {
    _out.writeBytes(Buffer.from("02", "hex"));
  });
}

export function deserializeState(state: StateWithClient): VoteState;
export function deserializeState(bytes: Buffer): VoteState;
export function deserializeState(
  bytes: Buffer,
  client: BlockchainStateClient,
  address: BlockchainAddress
): VoteState;
export function deserializeState(
  state: Buffer | StateWithClient,
  client?: BlockchainStateClient,
  address?: BlockchainAddress
): VoteState {
  if (Buffer.isBuffer(state)) {
    const input = AbiByteInput.createLittleEndian(state);
    return new voting(client, address).deserializeVoteState(input);
  } else {
    const input = AbiByteInput.createLittleEndian(state.bytes);
    return new voting(
      state.client,
      state.address
    ).deserializeVoteState(input);
  }
}

