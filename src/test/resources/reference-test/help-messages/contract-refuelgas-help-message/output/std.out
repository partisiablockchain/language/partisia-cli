Usage: cargo pbc contract refuelgas [-hv] [--gas=<amount>] [--net=<netname>]
                                    [--pk=<file>] [--show=<status>]
                                    [--validityDuration=<ms>] [--wallet=<file>]
                                    [--sign=<out-file> | --build=<out-file>]
                                    <contract-address>
Build, sign and send a transaction that adds more gas to a contract.
      <contract-address>   The address of the contract to add gas to.
      --build=<out-file>   Stop after building the transaction saving it to a
                             binary file instead of signing and sending it.
      --gas=<amount>       The amount of gas to send with the transaction. By
                             default 5000 is sent.
  -h, --help               Print usage description of the command.
      --net=<netname>      The blockchain net to target. To see all named nets,
                             run "cargo pbc config net -l".
                           "mainnet"  Target the mainnet
                           "testnet"  Target the testnet
                           <reader-url>  Target a custom net (with no browser)
                           <reader-url>,<browser-url>  Target a custom net with
                             a custom browser.
      --pk, --privatekey=<file>
                           File containing the private key to use for signing.
      --show=<status>      The status to show for the execution of a sent
                             transaction.
                           Waits until the specified status is available.
                           "tx"   The sent transaction.
                           "block"   Inclusion in a block.
                           "all"   All (recursive) spawned events. (DEFAULT)
      --sign=<out-file>    Stop after signing the transaction saving it to a
                             binary file instead of sending it.
  -v, --verbose            Print all available information. Default is to print
                             minimum information.
      --validityDuration=<ms>
                           How long the signature should be valid (in ms). By
                             default it is valid for 180000 ms.
      --wallet=<file>      File containing the wallet to use for signing.
