#!/usr/bin/env bash
# Runs main with the given arguments. (Instead of compiling a jar to execute through)
mvn compile exec:java -Dexec.mainClass="com.partisiablockchain.language.partisiacli.Main" -Dexec.args="$*"
